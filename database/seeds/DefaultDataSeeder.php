<?php

use Illuminate\Database\Seeder;

class DefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$dataRole = [
        	['name' => 'Administrador',
        	'created_at' => now(),
        	'updated_at' => now()]
            ,
        	['name' => 'Prueba',
        	'created_at' => now(),
        	'updated_at' => now()]
		];
		$dataUser = [
        	[
        		'username' => "admin",
                'role_id'   => 1,
        		'email' =>  "admin@gmail.com",
        		'password' => bcrypt("123456"),
        		'baja' => "NO",
        		'disponible' => "NO",
                'nombre' =>  "Admin-Nombre",
                'apellido' =>  "Admin-Apellido",
                'ci' =>  "7894561",
                'direccion' =>  "Gabriel Rene Moreno",
                'telefono' =>  "+59170048511",
                'ciudad_id' => 4,

        		'created_at' => now(),
        		'updated_at' => now()
        	]
		];
		$dataPermissions = [
        	[
        		'role_id'	=> 1,
        		'add'		=> 1,
        		'edit' 		=> 1,
        		'remove' 	=> 1,
        		'created_at' => now(),
        		'updated_at' => now()
        	],
            [
                'role_id'   => 2,
                'add'       => 1,
                'edit'      => 1,
                'remove'    => 1,
                'created_at' => now(),
                'updated_at' => now()
            ]
		];
		$dataCiudad = [
        	['nombre'=>'CHUQUISACA','abreviacion'=>'CHQ','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'LA PAZ','abreviacion'=>'LPZ','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'COCHABAMBA','abreviacion'=>'CBB','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'ORURO','abreviacion'=>'ORU','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'POTOSI','abreviacion'=>'PTS','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'TARIJA','abreviacion'=>'TAR','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'SANTA CRUZ','abreviacion'=>'SCZ','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'BENI','abreviacion'=>'BEN','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'PANDO','abreviacion'=>'PAN','created_at'=>now(),'updated_at'=>now()],
        	['nombre'=>'OTROS','abreviacion'=>'OTR','created_at'=>now(),'updated_at'=>now()],
		];

        $dataCategoriaProyecto = [
            ['nombre'=>'Practicas','created_at'=>now(),'updated_at'=>now()]
            ['nombre'=>'Examenes','created_at'=>now(),'updated_at'=>now()]
            ['nombre'=>'Otros','created_at'=>now(),'updated_at'=>now()]
        ];

        DB::table('ciudades')->insert($dataCiudad);
        DB::table('roles')->insert($dataRole);
        DB::table('users')->insert($dataUser);
        DB::table('permissions')->insert($dataPermissions);
        DB::table('categoria_proyecto')->insert($dataCategoriaProyecto);
    }
}
