<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{

    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-create',
           'user-edit',
           'user-delete',
           'usuariobloqueado-list',
           'usuariobloqueado-create',
           'usuariobloqueado-edit',
           'usuariobloqueado-delete',
           'GestorArchivo-list',
           'GestorArchivo-create',
           'GestorArchivo-edit',
           'GestorArchivo-delete',
           'AlmacenarArchivo-list',
           'AlmacenarArchivo-create',
           'AlmacenarArchivo-edit',
           'AlmacenarArchivo-delete',
           'CompararArchivo-list',
           'CompararArchivo-create',
           'CompararArchivo-edit',
           'CompararArchivo-delete',
           'CompararOcr-list',
           'CompararOcr-create',
           'CompararOcr-edit',
           'CompararOcr-delete',
           'ResultadosArchivo-list',
           'ResultadosArchivo-create',
           'ResultadosArchivo-edit',
           'ResultadosArchivo-delete',
           'CategoriaProyecto-list',
           'CategoriaProyecto-create',
           'CategoriaProyecto-edit',
           'GestorProyecto-list',
           'GestorProyecto-create',
           'GestorProyecto-edit',
           'GestorProyecto-delete',
           'AsignarUsuario-list',
           'AsignarUsuario-create',
           'AsignarUsuario-edit',
           'AsignarUsuario-delete',
           'GestorArchivoProyecto-list',
           'GestorArchivoProyecto-create',
           'GestorArchivoProyecto-edit',
           'GestorArchivoProyecto-delete',
           'GestionVersiones-list',
           'GestionVersiones-create',
           'GestionVersiones-edit',
           'GestionVersiones-delete',
           'Redaccion-list',
           'Redaccion-create',
           'Redaccion-edit',
           'Redaccion-delete',
        ];
       
         
         $Pdescripcion = [
           'Puede ver la lista de roles',
           'Puede crear a un rol',
           'Puede editar a un rol',
           'Puede borrar un rol',
           'Puede ver la lista de usuarios',
           'Puede crear un usuario',
           'Puede Actualizar  a un usuario',
           'Puede dar de baja a un usuario',
           'Puede ver la lista de usuarios bloqueados',
           'Puede agregar a un usuario para bloquearlo',
           'Puede editar a usuario bloqueado',
           'Puede borrar a usuario bloqueado',
           'Puede ver la lista de Gestor Archivo',
           'Puede crear un  Archivo',
           'Puede editar un Archivo',
           'Puede borrar un Archivo',
           'Puede ver la lista almacenar archivo',
           'Puede crear en almacenar archivo',
           'Puede editar almacenar archivo',
           'Puede borrar en almacenar archivo',
           'Puede ver  la lista Comparar Archivo',
           'Puede crear en Comparar Archivo',
           'Puede editar en Comparar Archivo',
           'Puede borrar en Comparar Archivo',
           'Puede ver Comparar Ocr',
           'Puede crear en Comparar Ocr',
           'Puede editar en Comparar Ocr',
           'Puede borrar en Comparar Ocr',
           'Puede ver la lista de  Resultados de Archivo',
           'Puede crear en Resultados de Archivo',
           'Puede editar en Resultados de Archivo',
           'Puede borrar en  Resultados de Archivo',
           'Puede ver la lista en Categoria Proyecto',
           'Puede crear en Categoria de Proyecto',
           'Puede editar en Categoria de  Proyecto',
           'Puede ver la lista de Gestor Proyecto',
           'Puede crear un Proyecto',
           'Puede editar un Proyecto',
           'Puede borrar un Proyecto',
           'Puede ver la lista de Asignar Usuario',
           'Puede crear en Asignar Usuario',
           'Puede editar en  Asignar Usuario',
           'Puede borrar en Asignar Usuario',
           'Puede ver la lista de Gestor Archivo',
           'Puede crear en Gestor Archivo',
           'Puede editar en Gestor Archivo',
           'Puede borrar en  Gestor Archivo',
           'Puede ver la lista de Gestor de Versiones',
           'Puede crear en Gestor de Versiones',
           'Puede editar en Gestor de Versiones',
           'Puede borrar en  Gestor de Versiones',
           'Puede ver la lista en Gestor Redaccion',
           'Puede crear en Gestor Redaccion',
           'Puede editar en Gestor Redaccion',
           'Puede borrar en Gestor Redaccion',
        ];
        $i = 0;
        foreach ($permissions as $permission) {
            //dd($Pdescripcion[$i]);
             Permission::create(['name' => $permission,'descripcion' => $Pdescripcion[$i] ]);
             $i++;
        }

        $dataCiudad = [
          ['nombre'=>'CHUQUISACA','abreviacion'=>'CHQ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'LA PAZ','abreviacion'=>'LPZ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'COCHABAMBA','abreviacion'=>'CBB','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'ORURO','abreviacion'=>'ORU','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'POTOSI','abreviacion'=>'PTS','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'TARIJA','abreviacion'=>'TAR','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'SANTA CRUZ','abreviacion'=>'SCZ','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'BENI','abreviacion'=>'BEN','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'PANDO','abreviacion'=>'PAN','created_at'=>now(),'updated_at'=>now()],
          ['nombre'=>'OTROS','abreviacion'=>'OTR','created_at'=>now(),'updated_at'=>now()],
        ];

        DB::table('ciudades')->insert($dataCiudad);
    }
}
