<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablePersonalizar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personalizados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pcoded_navbar')->nullable();
            $table->string('navbar_logo')->nullable();
            $table->string('pcoded_header')->nullable();
            $table->string('active_item_theme')->nullable();
            $table->string('pcoded_navigatio_lavel')->nullable();
            $table->string('nav_type')->nullable();
            $table->string('vertical_effect')->nullable();
            $table->string('item_border_style')->nullable();
            $table->string('dropdown_icon')->nullable();
            $table->string('subitem_icon')->nullable();
            $table->string('imagen')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personalizados');
    }
}
