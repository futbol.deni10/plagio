<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaBloqueousuario extends Migration
{

    public function up()
    {
        Schema::create('bloqueos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('razon')->nullable();
            $table->date('tiempo')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('creadopor');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bloqueos');
    }
}
