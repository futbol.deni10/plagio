FROM php:7.3-apache


#RUN docker-php-ext-install pdo_pgsql
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

RUN apt-get update \
 && apt-get install -y \
      ca-certificates \
      unzip

# Install Postgre PDO
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql
    
RUN a2enmod rewrite


ADD . /var/www
ADD ./public /var/www/html
RUN chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache 