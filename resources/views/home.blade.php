@extends('template.app')

@section('main-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if(session('nopermiso'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('nopermiso')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    jQuery(document).ready(function($) {
        cambio();
    });
    function cambio (){
    var route = "{{url('getPersonalizar')}}"
    var token = $("input[name=_token]").val();
    $.ajax({
        url : route,
        headers : {'X-CSRF-TOKEN':token},
        method : 'get',
        //dataType : 'json',
        //data : estiloGlobal,
        success : function (data)
        {
            for (var i = 0; i < data.length; i++) {
            if(data[i].nav_type != null)
                $(".pcoded").attr("nav-type", data[i].nav_type);
            if(data[i].navbar_logo != null) 
                $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
            if(data[i].pcoded_navigatio_lavel != null)
                $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
            if(data[i].pcoded_header != null)
                $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
            if(data[i].pcoded_navbar != null)                        
                $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
            if(data[i].active_item_them != null)
                $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
            if(data[i].sub_item_theme != null)
                $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
            if(data[i].themebg_pattern != null)
                $("body").attr("themebg-pattern", data[i].themebg_pattern)
            if(data[i].vertical_effect != null)
                $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
            if(data[i].item_border_style != null)
                $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
            if(data[i].dropdown_icon != null)
                $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
            if(data[i].subitem_icon != null)
                $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
            var usuario = '{{Auth::user()->id}}';
            if(data[i].imagen != null)
            {
                var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                
            }
            }
            
        },
        error : function (data){
            toastr.error("ERROR");
        }
    })
};

</script>
@endsection
