@component('mail::message')
# Hola

{{ $request['user']->username }}, bienvenido a {{ config('app.name') }} !

{!! $request['mensaje'] !!}

@component('mail::button', ['url' => $request['url']])
	Clic para ver resultados
@endcomponent

Saludos, y que estés bien ! <br>
{{ config('app.name') }}
@endcomponent
