@extends('template.app')

@section('title','Busqueda Plagio OnLinea')
@section('main-content')
@csrf
<div class="row">
	<div class="col-md-12">
		<label for="exampleFormControlTextarea1">BUSCAR EL SIGUIENTE TEXTO EN LINEA</label>
    	<textarea class="form-control" id="busqueda" rows="3">{!! $criterio !!}</textarea>
    	<br>
    	<button type="button" class="btn btn-info" id="btnBusqueda">
		    Buscar
		</button>
	</div>
</div>		
<hr>
<div class="table-responsive">
    <table id="Tabla-Resultados" class="table table-bordered table-striped" width="100%">
        <thead> 
            <tr>
                <th >#</th> 
                <th >ENLACE</th>
                <th >PARRAFO</th>
                <th >TITULO PAGINA</th>
                <th >ACCION</th>
            </tr>
        </thead>
        <tbody style="text-align: center;">

        </tbody>
    </table>
</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		cambio();
	});

	$("#btnBusqueda").click(function(event) {
		var contenido = $("#busqueda").val();
		if(contenido != '')
		{	
			$('#Tabla-Resultados').DataTable().destroy();
			var route = "{{url('gerResultados')}}/"+contenido; 
			var t = $('#Tabla-Resultados').DataTable({
		        "processing"  : true,
		        "serverSide"  : true,
		        "lengthMenu": [5,10, 25, 50, 75 ],
		        "language": {
		        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
		        },
		        "ajax": route,
		        columns: [
		            {data: null},
		            {data: 'link'},
		            {data: 'snippet'},
		            {data: 'title'},
		            {
		            	"defaultContent" : " ",
			            "searchable" : false,
			            "orderable" : false,
			            "render" : function(data, type, row, meta) {
			                var html='<div class="form-group">';

			                    html+='<a class="btn btn-info btn-xs text-white" onclick="verEnlace(`'+row.link+'`);"><i class="fas fa-wrench"></i> Ver Enlace</a>';
			                html +="</div>";
			                return html;
						} 
					}		            
		        ],
		        columnDefs: [
	                {
	                    render: function (data, type, full, meta) {
	                        return "<div class='text-wrap width-200'>" + data + "</div>";
	                    },
	                    targets: [0,1,2,3]
	                }
             	]
		    });

	    	t.on( 'draw.dt', function () {
	        var PageInfo = $('#Tabla-Resultados').DataTable().page.info();
	             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
	                cell.innerHTML = i + 1 + PageInfo.start;
	            } );
	        } );
		}else
			toastr.error('Debe llenar/escribir para poder buscar')
	});

    function verEnlace(url){
        //var url = "{{url('getDocumento')}}/"+criterio+"/"+name;
        window.open(url, '_blank');
    }


	function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

</script>
<style type="text/css">
.text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}

/*	.ord{
white-space:normal;
width:200px;
    }*/
/*    table {
  table-layout:fixed;
}
table td {
  word-wrap: break-word;
  max-width: 20%;
}
#Tabla-Resultados td {
  white-space:inherit;
}*/

/*   table.table {
    clear: both;
    margin-bottom: 6px !important;
    max-width: none !important;
    table-layout: fixed;
    Word-break: break-all;
   } */
</style>
@endsection