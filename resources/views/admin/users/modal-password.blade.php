<div class="modal fade" id="modal-EPassword" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    {!! Form::open([ 'id'=> 'EPform','autocomplete'=>'off']) !!}
    <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title" id="EPaTitle-Label" style="margin-left: auto;"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" id="EPid">
          <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                  {!! Form::label('password','Contraseña') !!}
                  {!! Form::password(
                    'password',
                    ['class'=>'form-control',
                    'placeholder' =>'*********',
                    'id'=>'EPpassword']) !!}
                </div>
                <div class="col-md-6">
                  {!! Form::label('password','Verificar Contraseña') !!}
                  {!! Form::password(
                    'password',
                    ['class'=>'form-control',
                    'placeholder' =>'*********',
                    'id'=>'EVpassword']) !!}
                </div>
                </div>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'btnEPassword', 'class'=>'btn btn-primary'])!!}  
        </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>

