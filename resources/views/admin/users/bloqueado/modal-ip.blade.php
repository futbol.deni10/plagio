<div class="modal fade" id="modal-RBloqueoip" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNbloqueoip','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="staticBackdropLabel" style="margin-left: auto;">Bloquear Ip</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Direccion Ip') !!}
							{!! Form::text('dir_ip',null, 
						  		['class'=>'form-control' ,'id'=>'Rdir_ip']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Razon de Bloqueo') !!}
							{!! Form::textarea('razon',null, 
						  		['class'=>'form-control' ,'id'=>'RIrazon']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('fecha','Fecha de expiración') !!}
						  	{!! Form::text('tiempo',null, 
						  		['class'=>'form-control' ,'id'=>'Ifecha']) !!}
							
						</div>
					</div>
				</div>
				<input type="hidden" name="estado" value="Bloqueado">	
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Bloquear Usuario', 
				$attributes = ['id'=>'btnRbloqueoip', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>