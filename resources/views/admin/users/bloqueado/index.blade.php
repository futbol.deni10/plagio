@extends('template.app')

@section('title','Bloquear Usuario / IP')
@section('main-content')
<link rel="stylesheet" href="{{asset('plugins/flatpickr/css/flatpickr.min.css')}}">
<link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet" />
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Usuarios Bloqueados</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Ip Bloqueadas</a>
        </div>
    </nav>

    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <br>
            @if(Auth::user()->hasPermissionTo('user-create'))
                <div align="left" style="border:auto;">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-RBloqueo">
                        Bloquear Usuario
                    </button>
                </div>
            @endif
            <hr>
            <div class="table-responsive table-striped">
                <table id="Tabla-UsuarioBloqueado" class="table table-bordered table-striped" width="100%">
                    <thead > 
                        <tr>
                            <th style="text-align: center;" >#</th>
                            <th style="text-align: center;" >Usuario</th>
                            <th style="text-align: center;" >Fecha Habilitado</th>
                            <th style="text-align: center;" >Bloqueado Por</th>
                            <th style="text-align: center;" >IP</th>
                            <th style="text-align: center;" >Navegador</th>
                            <th style="text-align: center;" >Estado</th>
                            <th style="text-align: center;" >Accion</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center;">

                    </tbody> 
                </table>
            </div>  
        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <br>
            @if(Auth::user()->hasPermissionTo('user-create'))
                <div align="left" style="border:auto;">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-RBloqueoip">
                        Bloquear Ip
                    </button>
                </div>
            @endif
            <br>
            <div class="table-responsive table-striped">
                <table id="Tabla-IpBloqueado" class="table table-bordered table-striped" width="100%">
                    <thead > 
                        <tr>
                            <th style="text-align: center;" >#</th>
                            <th style="text-align: center;" >Bloqueado Por</th>
                            <th style="text-align: center;" >Fecha Habilitado</th>
                            <th style="text-align: center;" >IP</th>
                            <th style="text-align: center;" >Estado</th>
                            <th style="text-align: center;" >Accion</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center;">

                    </tbody> 
                </table>
            </div> 
        </div>
    </div>
    <hr>

    @include('admin.users.bloqueado.modal-crear')
    @include('admin.users.bloqueado.modal-ip')

@endsection
@section('js') 
<script src="{{asset('plugins/flatpickr/js/flatpickr.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script> 
<script src="{{asset('plugins/select2/maximize-select2-height.js')}}"></script> 

<script type="text/javascript">
    $( document ).ready(function() {
        activar_tabla_users();
        activar_tabla_ip();
        setTimeout(function(){ 
            $(".fa-minus").click(); 
        }, 1000);

        cambio();
    });

    $( document ).ajaxStop(function() {
        //preloader();
    });

    //Llena la tabla de los usuarios bloqueados
    function activar_tabla_users() {
        var t = $('#Tabla-UsuarioBloqueado').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [5,10, 25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":'{!! url('list_userbloqueado') !!}',
            columns: [
                {data: 'id' },
                {data: 'username' },
                {data: 'tiempo' },
                {data: 'creadopor'},            
                {data: 'ip' },
                {data: 'navegador' },
                {data: 'estado'},            
                {
                "defaultContent" : " ",
                "searchable" : false,
                "orderable" : false,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                        if(row.estado == 'Bloqueado')
                            html+='<a style="width:100px; margin:5px;" class="btn btn-success btn-xs text-white" onclick="usuarioActivar('+row.id+');"><i class="fas fa-wrench"></i> Activar </a> </br>';

                    html +="</div>";
                    return html;
                },
            }]
        });
        t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-UsuarioBloqueado').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }
    
    function activar_tabla_ip() {
        var t = $('#Tabla-IpBloqueado').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [5,10, 25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":'{!! url('list_ipbloqueado') !!}',
            columns: [
                {data: 'id' },
                {data: 'username' },
                {data: 'tiempo' },
                {data: 'ip' },
                {data: 'estado'},            
                {
                "defaultContent" : " ",
                "searchable" : false,
                "orderable" : false,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                        if(row.estado == 'Bloqueado')
                            html+='<a style="width:100px; margin:5px;" class="btn btn-success btn-xs text-white" onclick="ipActivar('+row.id+');"><i class="fas fa-wrench"></i> Activar </a> </br>';

                    html +="</div>";
                    return html;
                },
            }]
        });
        t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-IpBloqueado').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }

        // Funcion para Bloquear un Usuario
    $("#btnRbloqueo").click(function(){
        var registerForm = $("#FRNbloqueo");
        var formData = registerForm.serialize();
        var route = "{{route('usuariobloqueado.store')}}";
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
        type: 'POST',
        dataType: 'json',
        data:formData,
        success: function(data){
            $("#modal-RBloqueo").modal('toggle');
            toastr.success("El Usuario fue bloqueado");
            $("#Tabla-UsuarioBloqueado").DataTable().ajax.reload();
        }, 
        error:function(data)
        {
            console.log(data);
            toastr.error("ERROR");   
        }  
        });
    });

    $("#btnRbloqueoip").click(function(){
        var registerForm = $("#FRNbloqueoip");
        var formData = registerForm.serialize();
        var route = "{{route('blockip.store')}}";
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
        type: 'POST',
        dataType: 'json',
        data:formData,
        success: function(data){
            $("#modal-RBloqueoip").modal('toggle');
            toastr.success("La ip Fue Bloqueada");
            $("#Tabla-IpBloqueado").DataTable().ajax.reload();
        }, 
        error:function(data)
        {
            console.log(data);
            toastr.error('ERROR');   
        }  
        });
    });

    function usuarioActivar(criterio){
         swal({
                title: "CONFIRMAR",
                text: "Esta seguro Activar al Usuario",
                buttons: ["Cancelar", "Confirmar"],
            })
            .then((willDelete) => {
              if (willDelete) {
                var route = "{{url('activarUsuarioBloqueado')}}";
                $.ajax({
                        url: route,
                        method: 'get',
                        data:{criterio},
                        success: function(data) {
                            toastr.success("El Usuario Fue Activado");  
                            $('#Tabla-UsuarioBloqueado').DataTable().ajax.reload();
                           
                        }
                });
              } 
            }); 
    };

    function ipActivar(criterio){
         swal({
                title: "CONFIRMAR",
                text: "Esta seguro Activar la IP",
                buttons: ["Cancelar", "Confirmar"],
            })
            .then((willDelete) => {
              if (willDelete) {
                var route = "{{url('activarIpBloqueado')}}";
                $.ajax({
                        url: route,
                        method: 'get',
                        data:{criterio},
                        success: function(data) {
                            toastr.success("La Ip Fue Activada");  
                            $('#Tabla-IpBloqueado').DataTable().ajax.reload();
                           
                        }
                });
              } 
            }); 
    };

    $("#fecha").flatpickr({
        minDate: new Date(),
        //defaultDate: new Date(),
        dateFormat: "Y-m-d ",
        disableMobile: "true"
    });

    $("#Ifecha").flatpickr({
        minDate: new Date(),
        //defaultDate: new Date(),
        dateFormat: "Y-m-d ",
        disableMobile: "true"
    });

    $('#Ruser').select2({
        placeholder: "Seleccione un Usuario",
        maximumSelectionSize: 1,
        allowClear: true,
        width: '100%',
        theme: "bootstrap4",
        dropdownParent: $('#modal-RBloqueo'),
        minimumInputLength : 1
    });  

    $('#Rrazon').select2({
        placeholder: "Seleccione una Razon",
        maximumSelectionSize: 1,
        allowClear: true,
        width: '100%',
        theme: "bootstrap4",
        dropdownParent: $('#modal-RBloqueo'),
        //minimumInputLength : 1
    });  

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
    </script>
<style type="text/css">
.flatpickr-calendar.open {
z-index: 99999999999 !important;
}
.flatpickr-calendar.static {
top: -300px;
}
textarea{
    resize: none;
    height: 150px;
}
</style>
@endsection