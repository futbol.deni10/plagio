<!-- Modal Usuario--> 
<div class="modal fade" id="modal-RUsers" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog ">
		{!! Form::open(['id'=> 'FRUsers','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="staticBackdropLabel" style="margin-left: 20%;">Formulario - Nuevo Usuario</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>

		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						{!! Form::label('username','Usuario') !!}
						{!! Form::text( 
							'username',null,
							['class'=>'form-control',
							'placeholder' =>'Nombre del Usuario',
							'id'=>'Rusername']) !!}
						</div>
						<div class="col-md-6" >
						{!! Form::label('email','Correo Electronico') !!}
						{!! Form::email(
							'email',null,
							['class'=>'form-control',
							'placeholder' =>'Example@gmail.com',
							'id'=>'Remail']) !!}
						</div>
					</div> 
				</div>   
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						{!! Form::label('password','Contraseña') !!}
						{!! Form::password(
							'password', 
							['class'=>'form-control',
							'placeholder' =>'*********',
							'id'=>'Rpassword']) !!}
						</div>
						<div class="col-md-6">
						{!! Form::label('type','Tipo Rol') !!}
						{!! Form::select(
							'roles',$roles,
							null,
							['class'=>'form-control',
							'placeholder'=>'Seleccione una Opcion...',
							'id'=>'Rrole_id']) !!}
						</div>						
					</div>
				</div>
				<div class="form-group"> 
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('nombre','Nombres') !!}
						  	{!! Form::text('nombre',null, 
						  		['class'=>'form-control nombre',
								'placeholder' =>'Nombre de la Persona',
								'id'=>'RPnombre']) !!}
						</div>
						<div class="col-md-6">
						  	{!! Form::label('apellido','Apellidos') !!}
						  	{!! Form::text('apellido',null, 
						  		['class'=>'form-control apellido',
								'placeholder' =>'Apellido de la Persona',
								'id'=>'RPapellido']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
						  	{!! Form::label('ci','Carnet de Identidad') !!}
						  	{!! Form::text('ci',null, 
						  		['class'=>'form-control ci',
								'placeholder' =>'Carnet de Identidad',
								'id'=>'RPci','onkeypress'=>"return valida(event,true)"]) !!}
						</div>
						<div class="col-md-6">
						  	{!! Form::label('ciudad_id','Extension') !!}
						  	<select class='form-control select_Pciudad' name="ciudad_id" id="Plist_ciudad">
						  		<option value=""></option>
						  	</select> 
						</div>
					</div>
				</div>			 	
				<div class="form-group">
					<div class="row">		
						<div class="col-md-6">
						{!! Form::label('direccion','Direccion') !!}
						{!! Form::textarea( 
							'direccion',null,
							['class'=>'form-control direccion',
							'placeholder' =>'direccion',
							'id'=>'RPdireccion']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('celular','Nº Telefono') !!}
						  	{!! Form::text('telefono',null, 
						  		['class'=>'form-control telefono',
								'placeholder' =>'+59170045677',
								'id'=>'RPtelefono','onkeypress'=>"return validaTelefono(event,true)"]) !!}
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar', $attributes = ['id'=>'btnRUser', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div> 