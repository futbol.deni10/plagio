@extends('template.app')

@section('title','Usuarios')
@section('main-content')
   <!-- Your Page Content Here -->
    <!-- if(Auth::user()->role->permission->add == 1) -->
    @if(Auth::user()->hasPermissionTo('user-create'))
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-RUsers">
                Nuevo Usuario
            </button>
        </div>
    @endif
    <!-- endif -->
    <hr>
    <div class="table-responsive table-striped">
        <table id="DataTableUser" class="table table-bordered table-striped" width="100%">
            <thead > 
                <tr>
                    <th style="text-align: center;" >#</th>
                    <th style="text-align: center;" >Usuario</th>
                    <th style="text-align: center;" >Correo</th>
                    <th style="text-align: center;" >Roles</th>
                    <th style="text-align: center;" >Estado</th>
                    <th style="text-align: center;" >Sesion</th>
                    <th style="text-align: center;" >Accion</th>
                </tr>
            </thead>
            <tbody style="text-align: center;">

            </tbody> 
        </table>
    </div>  
 
    @include('admin.users.create-modal')
    @include('admin.users.modal-edit')
    @include('admin.users.modal-edit-permissions')
    @include('admin.users.modal-confirm-delete')
    @include('admin.users.modal-password')
    @include('admin.users.modal-role')

@endsection
@section('js') 
<script type="text/javascript">
    $( document ).ready(function() {
        activar_tabla_users();
        llenarCiudades();
        setTimeout(function(){ 
            $(".fa-minus").click(); 
        }, 1000);

        cambio();
    });

    $( document ).ajaxStop(function() {
        //preloader();
    });

    // Funcion para Registrar un Usuario
    $("#btnRUser").click(function(){
        var registerForm = $("#FRUsers");
        var formData = registerForm.serialize();
        var route = "{{route('users.store')}}";
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
        type: 'POST',
        dataType: 'json', 
        data:formData,
        success: function(data){
            $("#modal-RUsers").modal('toggle');
            toastr.success("El Usuario: "+ data.username+ " Fue Registrado");
            $("#DataTableUser").DataTable().ajax.reload();
        }, 
        error:function(data)
        {
            console.log(data);
            var message="";
            if(data.responseJSON.errors.username != undefined){ 
                $('#Rusername').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else{
                $("#Rusername").removeAttr( 'style' );}           
            if(data.responseJSON.errors.email != undefined){ 
                $('#Remail').css('border', 'solid 1px red'); 
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>"; 
            }else {
                $('#Remail').removeAttr( 'style' );}
            if(data.responseJSON.errors.password != undefined){ 
                $('#Rpassword').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else {
                $('#Rpassword').removeAttr( 'style' );}
            if(data.responseJSON.errors.role_id != undefined){ 
                $('#Rrole_id').css('border', 'solid 1px red');
                message="Los campos Resaltados de color rojo son obligatorios. "+"<br/>";  
            }else {
                $('#Rrole_id').removeAttr( 'style' );}
            
            if($("#Rusername").val().length != 0 )
            {
                if(data.responseJSON.errors.username!=undefined){
                message = message +  data.responseJSON.errors.username[0]+ "<br/>";}
            }
            if($("#Remail").val().length != 0 )
            {
                if(data.responseJSON.errors.email!=undefined){
                message =  message + data.responseJSON.errors.email[0];}
            }

            if($("#Rpassword").val().length > 0)
            {
                message += data.responseJSON.errors.password[0] + "<br>";
            }
            toastr.error(message);   
        }  
        });
    });


    
    //Edita los permisos 
    $("#btnEPermissions").click(function()
    {
        var id = $("#id").val();
        var add = $(".Eadd").prop('checked');
        var edit = $(".Eedit").prop('checked');
        var remove = $(".Eremove").prop('checked');
        console.log(remove);
        var route = "{{url('permissions')}}/"+id+"";
        var token = $("input[name=_token]").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {add,edit,remove},
            success: function(data){
                $("#MeditPermissions").modal('hide');
                toastr.warning('Se Actualizaron Los Permisos Del Usuario: '+data.username);
                $("#DataTableUser").DataTable().ajax.reload();
            },
            error:function(data)
            {
                toastr.error('ERROR');
                console.log(data);
            }  
        });
    }); 

    //Dar de baja a un usuario
    function bajaUsuario(id)
    {
        var route = "{{url('users/')}}/"+id;
        $.get(route, function(data){
            swal({
              title: "CONFIRMAR",
              text: "Esta seguro dar de baja al Usuario: "+"\n"+ data.username,
              buttons: ["Cancelar", "Confirmar"],
            })
            .then((willDelete) => {
              if (willDelete) {
                var route = "{{url('bajaUsuario')}}";
                $.ajax({
                        url: route,
                        method: 'get',
                        data:{id:data.id},
                        success: function(data) {
                            toastr.success("El Usuario: "+data[0].username+" Fue Dado de Baja");  
                            $('#DataTableUser').DataTable().ajax.reload();
                           
                        }
                });
              }  
            });            
        });     
    }    

    //Activar a un usuario dado de baja
    function activarUsuario(id)
    {
        var route = "{{url('users/')}}/"+id;
        $.get(route, function(data){
            swal({
                title: "CONFIRMAR",
                text: "Esta seguro Activar al Usuario: "+"\n"+ data.username,
                buttons: ["Cancelar", "Confirmar"],
            })
            .then((willDelete) => {
              if (willDelete) {
                var route = "{{url('activarUsuario')}}";
                $.ajax({
                        url: route,
                        method: 'get',
                        data:{id:data.id},
                        success: function(data) {
                            toastr.success("El Usuario: "+data[0].username+" Fue Activado");  
                            $('#DataTableUser').DataTable().ajax.reload();
                           
                        }
                });
              } 
            });            
        });    
    }

    //Llena los datos de un usuario en el formulario para editar
    var Mostrar = function(id,criterio)
    {


        var route = "{{url('users')}}/"+id+"/edit";
        $.get(route, function(data){
            //console.log(data);
            //console.log(data.roles[0].name);
            $("#Eid").val(data.id);
            $("#Eusername").val(data.username);
            $("#Eemail").val(data.email);
            //$("#Epassword").val(data.password);
            $("#Erole_id").val(data.roles[0].name);
            $("#Eemail").val(data.email);
            $("#EPnombre") .val(data.nombre)
            $("#EPapellido").val(data.apellido)
            $("#EPci").val(data.ci)
            $("#Elist_ciudad").val(data.ciudad_id)
            $("#EPdireccion").val(data.direccion)
            $("#EPtelefono").val(data.telefono)

            if(criterio==2)
            {
                $("#ETitle-Label").html("Vista Previa: "+data.username);
                $("#Eusername").prop('disabled',true);
                $("#Eemail").prop('disabled',true);
                //$("#Epassword").prop('disabled',true);
                $("#Erole_id").prop('disabled',true);
                $("#Eemail").prop('disabled',true);
                $("#EPnombre") .prop('disabled',true);
                $("#EPapellido").prop('disabled',true);
                $("#EPci").prop('disabled',true);
                $("#Elist_ciudad").prop('disabled',true);
                $("#EPdireccion").prop('disabled',true);
                $("#EPtelefono").prop('disabled',true);
                $("#btnEUsers").hide();
            }
            else
            {
                $("#Eemail").prop('disabled',false);
                $("#Eusername").prop('disabled',false);
                //$("#Epassword").prop('disabled',false);
                $("#Erole_id").prop('disabled',false);
                $("#Eemail").prop('disabled',false);
                $("#EPnombre") .prop('disabled',false);
                $("#EPapellido").prop('disabled',false);
                $("#EPci").prop('disabled',false);
                $("#Elist_ciudad").prop('disabled',false);
                $("#EPdireccion").prop('disabled',false);
                $("#EPtelefono").prop('disabled',false);
                $("#ETitle-Label").html("Editando al Usuario: "+data.username);
                $("#btnEUsers").show();
            }


        });
    }

    //llena los datos en el modal para Resetear Contraseña
    var Mostrar2 = function(id)
    {
        var route = "{{url('passwords')}}/"+id+"/edit";
        $.get(route, function(data){ 
            /*console.log(data);*/
            $("#EPid").val(data.id);
            $("#EPaTitle-Label").html("Reiniciando Contraseña de: "+data.username);
        });
    }
 
    //Edita los datos de un usuario
    $("#btnEUsers").click(function()
    {
        var id = $("#Eid").val();
        var username = $("#Eusername").val();
        var email = $("#Eemail").val();
        //var password = $("#Epassword").val();
        var roles = $("#Erole_id").val();
        var nombre = $("#EPnombre") .val()
        var apellido = $("#EPapellido").val()
        var ci = $("#EPci").val()
        var ciudad_id = $("#Elist_ciudad").val()
        var direccion = $("#EPdireccion").val()
        var telefono = $("#EPtelefono").val()
        var route = "{{url('users/')}}/"+id+"";
        var token = $("input[name=_token]").val();
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {username,email,roles,nombre,apellido,ci,ciudad_id,direccion,telefono},
        success: function(data){
            if (data.success == 'true')
            {
                $("#modal-EUsers").modal('toggle');
                toastr.warning(data.mensaje);
                $("#DataTableUser").DataTable().ajax.reload();
            }else {
                if(data.error == 'true')
                {
                    limpiarStyle();
                    toastr.error(data.mensaje);
                    if(data.cont == 1)
                    {
                        $('#Eusername').css('border', 'solid 1px red'); 
                    }else {
                        if(data.cont == 2)
                        {
                            $('#Eemail').css('border', 'solid 1px red'); 
                        }else {
                            $('#username').css('border', 'solid 1px red'); 
                            $('#Eemail').css('border', 'solid 1px red'); 
                        }

                    }
                }
            }
        }, 
        error:function(data)
        {
            var message="Los campos Resaltados de color rojo son obligatorios";
            //vaidacion de los inputs vacios
            if(data.responseJSON.errors.username){
                $('#Eusername').css('border', 'solid 1px red'); 
            }else{
                $("#Eusername").removeAttr( 'style' );}
            if(data.responseJSON.errors.name){ 
                $('#Ename').css('border', 'solid 1px red'); 
            }else {
                $('#Ename').removeAttr( 'style' );}
            if(data.responseJSON.errors.lastname){ 
                $('#Elastname').css('border', 'solid 1px red'); 
            }else {
                $('#Elastname').removeAttr( 'style' );}
            if(data.responseJSON.errors.email){ 
                $('#Eemail').css('border', 'solid 1px red'); 
            }else {
                $('#Eemail').removeAttr( 'style' );}
            if(data.responseJSON.errors.rol_id){ 
                $('#Erole_id').css('border', 'solid 1px red'); 
            }else {
                $('#Erole_id').removeAttr( 'style' );}
            toastr.error(message); 
        }  
      });
    });

    //Edita la contraseña de un usuario
    $("#btnEPassword").click(function()
    {
        var id = $("#EPid").val();
        var password = $("#EPpassword").val();
        var password2 = $("#EVpassword").val();
        var route = "{{url('passwords/')}}/"+id;
        var token = $("input[name=_token]").val();
        if(ValidarPassword(password,password2))
        {
            $.ajax({ 
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data: {password:password},
                success: function(data){
                    if (data.success == 'true')
                    {
                        $("#modal-EPassword").modal('toggle');
                        window.location = '{{ route('users.index')}}' ;
                    }
                },
                error:function(data)
                {
                    console.log(data.responseJSON.errors);
                    var message="Los campos Resaltados de color rojo son obligatorios";
                    if(data.responseJSON.errors.password){ 
                        $('#EPpassword').css('border', 'solid 1px red'); 
                        $('#EVpassword').css('border', 'solid 1px red'); 
                    }else {
                        $('#EPpassword').removeAttr( 'style' );
                        $('#EVpassword').removeAttr( 'style' );
                    }

                    if($("#EPpassword").val().length > 0)
                    {
                        message += data.responseJSON.errors.password[0] + "<br>";
                    }

                    toastr.error(message);
                }  
            });        
        }else {
            $('#EPpassword').css('border', 'solid 1px red'); 
            $('#EVpassword').css('border', 'solid 1px red');
            toastr.error("Las contraseñas son diferentes");
        }

    });

    //Llena la tabla de los usuarios
    function activar_tabla_users() {
    var t = $('#DataTableUser').DataTable({
        responsive: true,
        "processing" : true,
        "serverSide" : true,
        "searchDelay" : 500,
        "responsive": true,
        "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}'
        } , 
        "lengthMenu": [5,10, 25, 50, 75 ],
        "ajax":'{!! url('list_user') !!}',
        "columnDefs":  [
            { width: "10%", targets: [0,5]},
        ],
        columns: [
            {data: 'id' },
            {data: 'username'},
            {data: 'email'},
            {data: 'roles',
                render: function(data, type, row, meta) {
                var html = ''
                if(row.roles.length>0)
                {                
                    for(var i=0; i<row.roles.length; i++)
                    {                        
                        var role = row.roles[i].name;
                        if ( role == 'Administrador' )
                        {
                            html += '<span class="badge badge-danger" style="width:100px;"> '+role+' </span> <br>';
                                    
                        }else {
                            html += '<span class="badge badge-primary" style="width:100px;"> '+role+' </label></span> <br>';
                        }
                    }
                }
                return html;
                }
            },
            {data: 'baja', 
                render: function(data, type, row, meta) {
                var html = ''
                if ( row.baja == 'Activo' )
                {
                    html = '<span class="badge badge-success" style="width:80px;"> Activo </span>';
                            
                }else {
                    html = '<span class="badge badge-danger" style="width:80px;"> Inactivo </span>';
                }
                return html;
                }
            },
            {data: 'remember_token', 
                render: function(data, type, row, meta) {
                var html = ''
                if ( row.remember_token == 'ONLINE' )
                {
                    html = '<span class="badge badge-success" style="width:80px;"> ONLINE </span>';
                            
                }else {
                    html = '<span class="badge badge-danger" style="width:80px;"> OFFLINE </span>';
                }
                return html;
                }
            },
            { 
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "className": "test",
            "render" : function(data, type, row) {
                    var html='<div class="form-group">';
                    var editar = "{{Auth::user()->hasPermissionTo('user-edit') }}";
                    var eliminar = "{{Auth::user()->hasPermissionTo('user-delete') }}";
                    var crear = "{{Auth::user()->hasPermissionTo('user-create') }}";

                    if(eliminar != ''){
                        if (row.baja == 'Activo')
                        {
                        html += '<a style="width:135px;" class="btn btn-danger btn-xs text-white" onclick="bajaUsuario('+row.id+')" ><i class="fas fa-user-slash"></i> Dar De Baja</a> <br> ';
                        }else{
                            html += '<a style="width:135px;" class="btn btn-success btn-xs text-white" onclick="activarUsuario('+row.id+')"><i class="fas fa-check"></i></span> Activar </a> <br>';
                        }
                    }
                    if(editar != ''){
                        html+='<a style="width:135px; margin:5px;" class="btn btn-warning btn-xs text-white" onclick="Mostrar('+row.id+',1);" data-toggle = "modal" data-target="#modal-EUsers"  data-tippy-content="Editar"><i class="fas fa-wrench"></i> Editar </a> <br> ';

                        html+='<a  style="width:135px; margin:5px;" class="btn btn-success btn-xs text-white" onclick="Mostrar2('+row.id+');"  data-toggle="modal" data-target="#modal-EPassword"><i class="fas fa-key"></i> Cambiar Contraseña </a> <br> ';
                    }
                    if(crear != '')
                    {
                        html+='<a  style="width:135px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="addRole('+row.id+');"  data-toggle="modal" data-target="#modal-Rrole"><i class="fas fa-key"></i> Agregar Role </a> <br> ';
                    }

                    html+='<a style="width:135px; margin:5px;" class="btn btn-primary btn-xs text-white" onclick="Mostrar('+row.id+',2);" data-toggle = "modal" data-target="#modal-EUsers"  data-tippy-content="Editar"><i class="fas fa-wrench"></i> Vista Previa </a> <br> ';

                    html += '</div>'
                    return html;
            },
            }]
    });
        t.on( 'draw.dt', function () {
        var PageInfo = $('#DataTableUser').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }
    
    function addRole(criterio) {
        $(".inputrole").val(criterio);
    }
    
    $("#btnRURole").click(function(event) {
        var role = $("#Rrole").val();
        var criterio = $(".inputrole").val();
        var route = "{{url('addRole')}}/"+criterio+'/'+role+"";
        var token = $("input[name=_token]").val();
        $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        //data: {},
        success: function(data){
            $("#modal-Rrole").modal('toggle');
            toastr.success("ROL AGREGADO");
            $("#DataTableUser").DataTable().ajax.reload();
        }, 
        error:function(data)
        {
            toastr.error("ERROR");
        }  
        });
    });

    //Quita bordes
    function limpiarStyle()
    {        
        $("#Eusername").removeAttr( 'style' );
        $("#Eemail").removeAttr( 'style' );
        $("#Epassword").removeAttr( 'style' );
        $("#Erole").removeAttr( 'style' );
    }
    
    //Validar Contraseña
    function ValidarPassword(p1,p2)
    {
        if(p1 != p2)
            return false;
        else
            return true;
    }

    function llenarCiudades()
    {
        var route = "{{url('get_allciudades')}}";
        $.get(route,function(res, sta){
            $("#Plist_ciudad").empty();
            $("#Plist_ciudad").append(`<option value=""> </option>`);
            res.forEach(element => {
                $("#Plist_ciudad").append(`<option value=${element.id}> ${element.nombre} </option>`);
            });
            $("#Plist_ciudad").trigger("chosen:updated");
        });
    }
 

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
    </script>

<style type="text/css">
    textarea {
        resize: none;
        height: 150px;
    }
</style>
@endsection