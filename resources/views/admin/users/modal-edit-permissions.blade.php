<div class="modal fade" id="MeditPermissions" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open([ 'id'=> 'form','autocomplete'=>'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="EPTitle-Label" style="margin-left: auto;"></h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" id="id">
		  		<div class="form-group" align="center"> 
					<div class="form-check form-check-inline">
						<div class="center">
							<input type="checkbox" class="Eadd" id="cbx" style="display:none"/>
							<label for="cbx" class="toggle"><span></span></label>Agregar
						</div>
					</div>
					<div class="form-check form-check-inline">
						<div class="center">
							<input type="checkbox" class="Eedit" id="cbx2" style="display:none"/>
							<label for="cbx2" class="toggle"><span></span></label>Editar
						</div>
					</div>
					<div class="form-check form-check-inline">
						<div class="center">
							<input type="checkbox" class="Eremove" id="cbx3" style="display:none"/>
							<label for="cbx3" class="toggle"><span></span></label>Eliminar
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'btnEPermissions', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
