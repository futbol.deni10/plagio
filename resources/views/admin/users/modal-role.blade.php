<div class="modal fade" id="modal-Rrole" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog ">
		{!! Form::open(['id'=> 'FRRole','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" id="staticBackdropLabel" style="margin-left: auto;">Agregar Rol</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="inputrole" value="">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('type','Roles') !!}
						{!! Form::select(
							'roles',$roles,
							null,
							['class'=>'form-control',
							'placeholder'=>'Seleccione una Opcion...',
							'id'=>'Rrole']) !!}
						</div>						
					</div>
				</div>
				
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Agregar Rol', $attributes = ['id'=>'btnRURole', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>