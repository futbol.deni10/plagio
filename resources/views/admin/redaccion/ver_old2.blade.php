@extends('template.app')

@section('title','Redaccion TEXTO - '.$who)
@section('main-content')

<h1>
    hola
</h1>


<div>
    <textarea rows="30" cols="50" id="editor" style="background-color:rgb(34, 29, 29);color:white;padding:10px" placeholder="Type Your Text..."></textarea>
</div>

@include('admin.redaccion.modal-crear')
@endsection

@section('js')
<script src="{{asset('assets/socketioclient/dist/socket.io.js')}}"></script>
<script>
    var socket = io('http://localhost:3000');
    const log = console.log;

    const getEl = id => document.getElementById(id);

    const editor = getEl("editor");

    editor.addEventListener("keyup", evt => {
        const text = editor.value;
        socket.send(text);
    });

    socket.on('message', data => {
        editor.value = data;
    });

</script>
@endsection
