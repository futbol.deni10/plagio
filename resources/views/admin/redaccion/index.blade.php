@extends('template.app')

@section('title','Redaccion')
@section('main-content')
<link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet" />
<div class="row">
	<div class="col-md-12">
		<!-- if(Auth::user()->role->permission->add == 1) -->
        @if(Auth::user()->hasPermissionTo('Redaccion-create'))
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rredaccion">
		        Nueva Redaccion
		    </button>
		</div>
        @endif
		<br>
		<!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-Redaccion" class="table table-bordered table-striped" width="100%">
		        <thead>
		            <tr>
		                <th>#</th>
		                <th>CREADO POR</th>
		                <th>NOMBRE REDACCION</th>
		                <th>FECHA</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">

		        </tbody>
		    </table>
		</div>
	</div>
</div>
@include('admin.redaccion.modal-crear')


@endsection

@section('js')
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('plugins/select2/maximize-select2-height.js')}}"></script>
<script type="text/javascript">

	jQuery(document).ready(function($) {
		cambio();
	});

	function activar_tabla_redaccion() {
    	var t = $('#Tabla-Redaccion').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_redacciones') !!}',
        columns: [
            {data: 'id' },
            {data: 'username' },
            {data: 'nombre' },
            {data: 'created_at' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';
                    var edit = "{{Auth::user()->hasPermissionTo('Redaccion-edit') }}";
                    var remove = "{{Auth::user()->hasPermissionTo('Redaccion-delete') }}";
                    if(edit !='')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="abrirRedaccion('+row.id+');"><i class="fas fa-wrench"></i> Ver Redaccion </a> </br>';
                    if(remove !='')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="eliminarRedaccion('+row.id+');"><i class="fas fa-wrench"></i> Eliminar </a> </br>';

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Redaccion').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }
    activar_tabla_redaccion();

    $("#btnRredaccion").click(function(){
        var registerForm = $("#FRNredaccion");
        var formData = registerForm.serialize();
        var route = "{{route('redaccion.store')}}";
        var token = $('input[name="_token"]').val();
        $.ajax({
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data:formData,
            success : function(data){
                $("#modal-Rredaccion").modal('toggle');
                toastr.success("Archivo Almacenado");
                $('#Tabla-Redaccion').DataTable().ajax.reload();
                abrirRedaccion(data.id);
            },
            error : function(data){
            	toastr.error("ERROR");

            }
        });
    })


    function abrirRedaccion(criterio)
    {
        var url = "{{url('veredaccion')}}/"+criterio;
        window.open(url, '_blank');
    }

    function eliminarRedaccion(criterio)
    {
        Swal.fire({
            title: 'Esta seguro?',
            text: "De eliminar el Registro Seleccionado!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
        }).then((result) => {
            console.log(result);
            if (result.value) {
                var route = "{{url('eliminarRedaccion')}}/"+criterio;
                var token = $("input[name=_token]").val();
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    success : function(data)
                    {
                        if(data.mensaje == '')
                        {
                            toastr.success('Registro Eliminado');
                            $('#Tabla-Redaccion').DataTable().ajax.reload();
                        }else
                            toastr.error(data.mensaje);
                    }
                });
            }
        })
    }

	function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null)
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');

                }
                }

            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

    $('#Rusers').select2({
        placeholder: "Seleccione un Usuario",
        //maximumSelectionSize: 1,
        allowClear: true,
        width: '100%',
        theme: "bootstrap4",
        dropdownParent: $('#modal-Rredaccion'),
        minimumInputLength : 1
    });
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
</style>
@endsection
