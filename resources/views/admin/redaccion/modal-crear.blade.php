<div class="modal fade" id="modal-Rredaccion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNredaccion','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title title-redaccion" >NUEVA REDACCION</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('nombre','NOMBRE REDACCION') !!}
						  	{!! Form::text('nombre',null,
						  		['class'=>'form-control' ,'id'=>'Rnombre']) !!}

						</div>
					</div>
				</div>
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Usuarios') !!}
							{!! Form::select('user_id[]', $users, null,
								['class'=>'form-control',
								'id' => 'Rusers',
								'multiple' => 'multiple']) !!}
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Redaccion',
				$attributes = ['id'=>'btnRredaccion', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
