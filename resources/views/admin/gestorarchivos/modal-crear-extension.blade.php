<div class="modal fade" id="modal-RExtension" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-ms">
		{!! Form::open(['id'=> 'FRNextension','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="text-align: center;">Formulario - Extension</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="form-control idextension">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						  	{!! Form::label('tparchivo','Seleccione Tipo de Archivo') !!}
						  	{!! Form::select('tipoarchivo_id',[], null,
                        		['class' => 'form-control','id' => 'select_Rtparchivo']) !!}
						</div>
					</div> 
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						  	{!! Form::label('extension','Ingrese Extension del archivo') !!}
						  	{!! Form::text('extension',null, 
						  		['class'=>'form-control extension',]) !!}
						</div>
					</div>
				</div> 
				
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Extension', 
				$attributes = ['id'=>'btnRExtension', 'class'=>'btn btn-primary'])!!}

				{!!link_to('#', $title='Actualizar Extension', 
				$attributes = ['id'=>'btnEExtension', 'class'=>'btn btn-primary', 'style' => 'display:none;'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>