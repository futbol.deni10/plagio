<div class="modal fade" id="modal-Rtparchivo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-ms">
		{!! Form::open(['id'=> 'FRNtparchivo','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="text-align: center;">Formulario - Tipo Archivo</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="form-control idtparchivo">
		  		<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						  	{!! Form::label('apellido','Ingrese El Tipo de Archivo') !!}
						  	{!! Form::text('tipoarchivo',null, 
						  		['class'=>'form-control tipoarchivo',]) !!}
						</div>
					</div>
				</div>
				
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Tipo de Archivo', 
				$attributes = ['id'=>'btnRtparchivo', 'class'=>'btn btn-primary'])!!}

				{!!link_to('#', $title='Actualizar Tipo de Archivo', 
				$attributes = ['id'=>'btnEtparchivo', 'class'=>'btn btn-primary', 'style' => 'display:none;'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>