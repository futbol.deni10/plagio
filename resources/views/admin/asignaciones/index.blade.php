        @extends('template.app')

@section('title','Lista Asignar Usuarios')
@section('main-content')
<link rel="stylesheet" href="{{asset('plugins/flatpickr/css/flatpickr.min.css')}}">
<link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet" />

<div class="row"> 
	<div class="col-md-12">
        <!-- if(Auth::user()->role->permission->add == 1) -->
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rasignacion">
		        Nueva Asignacion
		    </button>
		</div>
        <!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-Asignacion" class="table table-bordered table-striped" width="100%">
		        <thead> 
		            <tr>
		                <th>#</th>
		                <th>NOMBRE DEL PROYECTO</th>
		                <th>USUARIO ASIGNADO</th>
                        <th>ASIGNADO POR</th>
                        <th>FECHA CREADO</th>
                        <th>FECHA EXPIRACION</th>
                        <th>ESTADO</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">
 
		        </tbody>
		    </table>
		</div>
	</div> 
</div>


@include('admin.asignaciones.modal-crear')
@include('admin.asignaciones.modal-editar')
 
@endsection

@section('js')
<script src="{{asset('plugins/flatpickr/js/flatpickr.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script> 
<script src="{{asset('plugins/select2/maximize-select2-height.js')}}"></script> 
<script type="text/javascript">
        // $( document ).ajaxStart(function() {
        // $('.theme-loader').css('opacity','none');
        // $('.theme-loader').show();
        // }); 

        // $( document ).ajaxStop(function() {
        // $('.theme-loader').hide();
        // });

    jQuery(document).ready(function($) {
        cambio();    
    });

	function activar_tabla_asignacion() {
    	var t = $('#Tabla-Asignacion').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_asignacion') !!}',
        columns: [
            {data: 'id' },
            {data: 'nombre' },
            {data: 'username' },
            {data: 'creadopor'},
            {data: 'created_at' },
            {data: 'tiempo' },
            {data: 'estado',
                render: function(data, type, row, meta) {
                    var html = ''
                    if ( row.estado == 'activo' )
                    {
                        html = '<span class="badge badge-success" style="width:80px;"> Activo </span>';
                                
                    }else {
                        html = '<span class="badge badge-danger" style="width:80px;"> Inactivo </span>';
                    }
                    return html;
                }
            },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';

                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Easignacion" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';

                html +="</div>";
                return html;
            },
        }]
    });
	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Asignacion').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    activar_tabla_asignacion();

    $("#btnRasignacion").click(function(){
        var proyecto_id = $("#Rproyecto").val();
        var user_id = $("#Ruser").val();
        var tiempo = $("#fecha").val();
        var roles = $("#Rrole").val();
        var route = "{{route('asignacion.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url:route,
            method:'POST',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            data: {proyecto_id,user_id,tiempo,roles},
            success : function(data){
                if(data.mensaje == '')
                {   
                    $("#modal-Rasignacion").modal('toggle');
                    toastr.success('Asignacion Registrado');
                    $("#Tabla-Asignacion").DataTable().ajax.reload();
                }else 
                {
                    toastr.error(data.mensaje);
                }
                
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    });

    $("#btnEasignacion").click(function(event) {
        var criterio = $("#criterioAsignacion").val()
        var proyecto_id = $("#Eproyecto").val(); 
        var user_id = $("#Euser").val();
        var tiempo = $("#Efecha").val();
        var roleproyecto_id = $("#Erole").val();
        var route = "{{url('updateAsignacion')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url:route,
            method:'PUT',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            data: {proyecto_id,user_id,tiempo,roleproyecto_id},
            success : function(data){
                    $("#modal-Easignacion").modal('toggle');
                    toastr.success('Asignacion Actualizada');
                    $("#Tabla-Asignacion").DataTable().ajax.reload();               
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    });

	function cargarextension()
	{	
		var route = "{{url('getExtensiones')}}";
		$.ajax({
			url:route,
			method : 'get',
			success: function(data){
				var extension = [];
				for(var i=0; i<data.length; i++)
				{
					extension.push(data[i].extension.toLowerCase());
				}
				$('#RFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});

               	$('#EFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});
			}
		})
	}

    function llenarDatosEdit(criterio)
    {
        var route = "{{url('asignacion')}}/"+criterio+"/edit";
        $.get(route, function(data){
            console.log(data);
            if(data.asignacion.user_id == data.asignacion.creadopor)
            {
                console.log('ok');
                $('#criterioAsignacion').val(data.asignacion.id);
                //time.setDate(data.tiempo,true);
                $("#Eproyecto").val(data.asignacion.proyecto_id);
                $("#Efecha").prop( "disabled", true );
                $("#Euser").val(data.asignacion.user_id).prop( "disabled", true );
                $("#Erole").val('JefeProyecto').prop( "disabled", true );
            }else{
                $('#criterioAsignacion').val(data.asignacion.id);
                $("#Eproyecto").val(data.asignacion.proyecto_id);
                $("#Efecha").prop( "disabled", false );
                time.setDate(data.tiempo,true);
                $("#Euser").val(data.asignacion.user_id).prop( "disabled", false );
                for(var i=0; i<data.usuario.roles.length; i++)
                {
                    $("#Erole").val(data.usuario.roles[i].name).prop( "disabled", false );
                }
            }
        });
    }

    $("#fecha").flatpickr({
        minDate: new Date(),
        //defaultDate: new Date(),
        dateFormat: "Y-m-d ",
        disableMobile: "true"
    });
//flatpickr.setDate(new Date('2017/12/04'))
    var time = $("#Efecha").flatpickr({
        minDate: new Date(),
        //defaultDate: new Date(),
        dateFormat: "Y-m-d ",
        disableMobile: "true"
    });
	//cargarextension();

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

    $('#Ruser').select2({
        placeholder: "Seleccione un Usuario",
        maximumSelectionSize: 1,
        allowClear: true,
        width: '100%',
        theme: "bootstrap4",
        dropdownParent: $('#modal-Rasignacion'),
        minimumInputLength : 1
    });  
</script>
<style type="text/css">
.flatpickr-calendar.open {
z-index: 99999999999 !important;
}
.flatpickr-calendar.static {
top: -300px;
}
</style>
@endsection