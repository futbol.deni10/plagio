@extends('template.app')

@section('title','Repositorio Archivo')
@section('main-content')

<div class="row">
	<div class="col-md-12">
        <!-- if(Auth::user()->role->permission->add == 1) -->
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rarchivo">
		        Nuevo Registro Archivo
		    </button>
		</div>
        <!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-Repositorio" class="table table-bordered table-striped" width="100%">
		        <thead> 
		            <tr>
		                <th>#</th>
		                <th>NOMBRE ARCHIVO</th>
		                <th>TIPO ARCHIVO</th>
		                <th>EXTENSION</th>
		                <th>CREADO POR</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">
 
		        </tbody>
		    </table>
		</div>
	</div>
</div>
@include('admin.repositorioarchivo.modal-crear')
@include('admin.repositorioarchivo.modal-editar')
@endsection

@section('js')
<script type="text/javascript" src="{{asset('plugins/docx/jszip/dist/jszip.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/docx/DOCX/docx.js')}}"></script>
<script type="text/javascript">
    var textoExtraido;
    var verificar = false;

	function activar_tabla_repositorio() {
    	var t = $('#Tabla-Repositorio').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_repositorio') !!}',
        columns: [
            {data: 'id' },
            {data: 'nombrearchivo' },
            {data: 'tipoarchivo' },
            {data: 'extension' },
            {data: 'nombreusuario' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';

                    // html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Earchivo" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';

                    html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="descargar('+row.id+');"><i class="fas fa-wrench"></i> Descargar </a> </br>'; 

                    html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="eliminarArchivo('+row.id+');"><i class="fas fa-wrench"></i> Eliminar </a> </br>'; 

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Repositorio').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    activar_tabla_repositorio();

	function cargarextension()
	{	
		var route = "{{url('getExtensiones')}}";
		$.ajax({
			url:route,
			method : 'get',
			success: function(data){
				var extension = [];
				for(var i=0; i<data.length; i++)
				{
					extension.push(data[i].extension.toLowerCase());
				}
				$('#RFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});

               	$('#EFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});
			}
		})
	}
	cargarextension();


	// $("#btnRarchivo").click(function(e){
 //        var archivo = $("#RFarchivo").fileinput('getFileStack')[0];
 //        var estado = "archivo";
 //        var formData = new FormData();
 //        formData.append('archivo',archivo);
 //        formData.append('estado',estado);

 //        var route = "{{route('repositorio.store')}}";
 //        var token = $('input[name="_token"]').val();
 //        $.ajax({
 //            url: route,
 //            headers: {'X-CSRF-TOKEN': token},
 //            method: 'POST',
 //            data:formData,
 //            contentType: false,
 //            cache: false,
 //            processData : false,
 //            success : function(data){
 //                $("#modal-Rarchivo").modal('toggle');
 //                toastr.success("Archivo Almacenado");  
 //                $('#Tabla-Repositorio').DataTable().ajax.reload();
 //            },
 //            error : function(data){

 //            }
 //        });
	// })	
 
	$("#btnEarchivo").click(function(e){
        var archivo = $("#EFarchivo").fileinput('getFileStack')[0];
        var extension_id = $(".select_RExtension").val();
        var criterio = $(".idarchivo").val();
        var formData = new FormData();
        formData.append('archivo',archivo);
        formData.append('extension_id',extension_id);
        formData.append('id',criterio);

        var route = "{{url('actualizarrepositorio')}}";
        var token = $('input[name="_token"]').val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            method: 'POST',
            data:formData,
            contentType: false,
            cache: false,
            processData : false,
            success : function(data){
                $("#modal-Earchivo").modal('toggle');
                toastr.success("Archivo Almacenado");  
                $('#Tabla-Repositorio').DataTable().ajax.reload();
            },
            error : function(data){

            }
        });
	})
        

	function llenarDatosEdit(criterio)
	{
		var route = "{{url('repositorio')}}/"+criterio+"/edit";
        $.get(route, function(data){
            $(".idarchivo").val(data.id);
            $(".select_RExtension").val(data.extension_id)
            cargarextension();
        });
	}

	function descargar(criterio)
	{
		var url = "{{url('getArchivo')}}/"+criterio;
        window.open(url, '_blank');
	}

	function eliminarArchivo(criterio)
	{
		Swal.fire({
		  	title: 'Esta seguro?',
		 	text: "De eliminar el Registro Seleccionado!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Si',
		  	cancelButtonText: 'No',
		}).then((result) => {
			console.log(result);
  			if (result.value) {
    			var route = "{{url('repositorio')}}/"+criterio;
    			var token = $("input[name=_token]").val();
    			$.ajax({
    				url: route,
			        headers: {'X-CSRF-TOKEN': token},
			        type: 'DELETE',
			        success : function(data)
			        {
			        	toastr.success('Registro Eliminado');
			        	$('#Tabla-Repositorio').DataTable().ajax.reload();
			        }
    			});
  			}
		})
	}

    function $id(id) {
      return document.getElementById(id);
    }

    function convert() {        
            //var selected_file = $id('file').files[0];
            var selected_file =  $('#RFarchivo').fileinput('getFileStack')[0];
            //var selected_file =  "{{asset('Archivo.docx')}}"
            var reader = new FileReader();
            reader.onload = function(aEvent) {
                textoExtraido = convertToPDF(btoa(aEvent.target.result));
                //console.log(textoExtraido);
                enviarDocumento(textoExtraido);
            }; 
        //reader.readAsArrayBuffer(selected_file);
        reader.readAsBinaryString(selected_file);
    }
 
    function convertToPDF(aDocxContent) {
        try {
            var content = docx(aDocxContent);
            //$id('container').textContent = '';
            //console.log('content length: ' + content.DOM.length);
            var node ="";
            var con = [];
            for(var i=0; i<content.DOM.length; i++) {
                node = content.DOM;
            }
            for(var j=0; j<node.length; j++) {
                con [j]= node[j].outerHTML;
            }
            verificar = true;

          //$id('container').appendChild(node[0]);
            return con;
        }
        catch (e) {
           return null;
        }
        
    }

    window.addEventListener('load', function() {
        document.getElementById('btnRarchivo').onclick = convert;
        cargarextension();
        //console.log('hola');
    });
  
    function enviarDocumento(criterio) {
        var archivo = $("#RFarchivo").fileinput('getFileStack')[0];
        var estado = "archivo";
        var formData = new FormData();
        formData.append('archivo',archivo);
        formData.append('estado',estado);
        formData.append('verificar',verificar);
        formData.append('criterio',criterio);

        var route = "{{route('repositorio.store')}}"; 
        var token = $('input[name="_token"]').val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            method: 'POST',
            data:formData, 
            contentType: false,
            cache: false,
            processData : false,
            success : function(data){
                $("#modal-Rarchivo").modal('toggle');
                toastr.success("Archivo Almacenado");  
                $('#Tabla-Repositorio').DataTable().ajax.reload();
            },
            error : function(data){

            }
        });
    }
    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    }; 

    cambio();  
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
</style>
@endsection