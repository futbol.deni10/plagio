<div class="modal fade" id="modal-Earchivo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FREarchivo','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="margin-left: auto;">Actualizar Archivo</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="form-control idarchivo" value="">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('nombre','Extension') !!}
						{!! Form::select('extension_id',$extension, null,
				            ['class' => 'form-control select_RExtension',
				            'required' => 'required']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('archivo','Seleccione un Archivo') !!}
						{!! Form::file(
							'archivo',
							['class'=>'file','id'=>'EFarchivo','type'=>'file']) !!}
						</div>
					</div>
				</div>		
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar Archivo', 
				$attributes = ['id'=>'btnEarchivo', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>