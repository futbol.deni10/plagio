<div class="modal fade" id="modal-Rarchivo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNarchivo','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title">Registrando Archivo</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('archivo','Seleccione un Archivo') !!}
<!-- 						 {!! Form::file(
							'archivo', 
							['class'=>'file','id'=>'RFarchivo','type'=>'file']) !!} -->
						<!-- <input id="RFarchivo" type="file" class="file" data-theme="fas" > -->
						<input id="RFarchivo" name="file[]" type="file" class="file" data-upload-url="#" data-theme="fas" multiple = 'multiple'>
						</div>
					</div>
				</div>		
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('version','#Version') !!}
						{!! Form::text( 
							'version',null,
							['class'=>'form-control',
							'id'=>'Rversion','disabled']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('archivo','Notas de Version') !!}
						{!! Form::textarea( 
							'nombre',null,
							['class'=>'form-control',
							'id'=>'Rnotas']) !!}
						</div>
					</div>
				</div>
			</div>
		  	<div class="modal-footer Cver">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    <button type="button" class="btn btn-primary btnRA" id="btnRarchivo">Registrar Version</button>
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>