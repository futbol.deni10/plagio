@extends('template.app')

@section('title','Gestion de Versiones')
@section('main-content')
@if(count($datos) > 0)
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
<div class="row"> 
    <div class="col-md-6">
        <div align="left" style="border:auto;">
            {!! Form::label('type','Proyecto Asignado') !!}
            {!! Form::select(
                'proyecto_id',$datos,
                null,
                ['class'=>'form-control',
                'placeholder'=>'Seleccione una Opcion...',
                'id'=>'Select_Pasignado']) !!}           
        </div> 
    </div>
</div>
<hr>
<input type="hidden" class="vers" value="">
<div class="row">
    <div class="col-md-12"> 
        <!-- if(Auth::user()->role->permission->add == 1) -->
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info"  onclick="getVersion();" data-toggle="modal" data-target="#modal-Rarchivo">
                Nueva Version
            </button>
        </div>
        <!-- endif -->
        <hr>
        <div class="table-responsive">
            <table id="Tabla-Versiones" class="table table-bordered table-striped" width="100%">
                <thead style="text-align: center;"> 
                    <tr>
                        <th>#</th>                        
                        <th>VERSION</th>
                        <th>NOTAS VERSION</th>
                        <th>CREADO POR</th>
                        <th>FECHA</th>
                        <th>ACCION</th>
                    </tr>
                </thead>
                <tbody style="text-align: center;">
 
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    No tiene ningun proyecto asignado
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@include('admin.gestionversiones.modal-crear')
@include('admin.gestionversiones.modal-verComparado')
@include('admin.gestionversiones.modal-verVersion')

@endsection

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{asset('plugins/docx/jszip/dist/jszip.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/docx/DOCX/docx.js')}}"></script>
<script type="text/javascript" src="{{ asset('plugins/jqueri-encode/jquery.base64.js') }}"></script>
<script type="text/javascript">
    var textoExtraido;
    var verificar = false;

    jQuery(document).ready(function($) {
        cambio();
    });


    function activar_tabla_versiones(criterio)
    {
        $('#Tabla-Versiones').DataTable().destroy();
        var route = "{{url('listar_Versiones')}}/"+criterio;
        var t = $('#Tabla-Versiones').DataTable({
        "processing"  : true,
        // "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":route,
        columns: [
            {data: 'id' },
            {data: 'version' },
            {data: 'notas' },
            {data: 'username' },
            {data: 'created_at' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group">';

                    html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" data-toggle="modal" data-target="#modal-verVersion" onclick="verVersion(`'+row.version+'`);"><i class="fas fa-wrench"></i> Ver </a> </br>';
 
                    html+='<a style="width:100px; margin:5px;" class="btn btn-warning btn-xs text-white" data-toggle="modal" data-target="#modal-vercomparado" onclick="verCambio('+row.id+');"><i class="fas fa-wrench"></i> Ver Cambios </a> </br>';

                    html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="eliminarVersion('+row.id+');"><i class="fas fa-wrench"></i> Eliminar</a> </br>';

                    html+='<a style="width:100px; margin:5px;" class="btn btn-success btn-xs text-white disabled"><i class="fas fa-wrench"></i> Descargar</a> </br>';

                html +="</div>";
                return html;
            },
        }]
        });

        t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Versiones').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }

    $("#Select_Pasignado").change(function(event) {
        var id = $("#Select_Pasignado").val();
        if(id != '')
            activar_tabla_versiones(id);
        else
            activar_tabla_versiones(0);
    });

    var textoExtraido;
    var verificar = false;
    var pt = 0;
    var ca = 0;
    function convert() {        
        var cant_archivos = $('#RFarchivo').fileinput('getFileStack').length;
        for(var k=0; k<cant_archivos; k++){
            //pt = k;
            //console.log(pt);
            ca = cant_archivos;
            var selected_file =  $('#RFarchivo').fileinput('getFileStack')[k];
            //console.log(selected_file);
            //var selected_file =  "{{asset('Archivo.docx')}}"
            var reader = new FileReader();
            reader.onload = function(aEvent) {
                //console.log(pt, k)
                textoExtraido = convertToPDF(btoa(aEvent.target.result));
                //console.log(textoExtraido);
                if(pt+1 == ca)
                    enviarDocumento(textoExtraido,pt,true)
                else
                    enviarDocumento(textoExtraido,pt,false)

                pt++;
            };             
            reader.readAsBinaryString(selected_file);
            //pt++;
        }

    }
    
    function convertToPDF(aDocxContent) {
        try {
            var content = docx(aDocxContent);
            //$id('container').textContent = '';
            //console.log('content length: ' + content.DOM.length);
            var node ="";
            var con = [];
            for(var i=0; i<content.DOM.length; i++) {
                node = content.DOM;
            }
            for(var j=0; j<node.length; j++) {
                con [j]= node[j].outerHTML;
            }
            verificar = true;

          //$id('container').appendChild(node[0]);
            return con;
        }
        catch (e) {
           return null;
        }
        
    }

    window.addEventListener('load', function() {
        document.getElementById('btnRarchivo').onclick = convert;
        //cargarextension();
        //console.log('hola');
    });

    function enviarDocumento(criterio,pos,sem){
        var archivo = $('#RFarchivo').fileinput('getFileStack')[pos];
       
        var proyecto_id = $("#Select_Pasignado").val();
        var formData = new FormData();
        var notas = $("#Rnotas").val();
        formData.append('archivo',archivo);
        formData.append('proyecto_id',proyecto_id); 
        formData.append('notas',notas); 
        formData.append('verificar',verificar);
        formData.append('criterio',criterio);
        formData.append('vers',$(".vers").val()); 
        
        var route = "{{route('gestionversiones.store')}}";
        var token = $('input[name="_token"]').val();
        if(proyecto_id != ''){
            $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            method: 'POST',
            data:formData,
            contentType: false,
            cache: false,
            processData : false,
            success : function(data){
                verificar = false;
                if(sem){
                    $("#modal-Rarchivo").modal('toggle');
                    toastr.success("Version Almacenada, Puede realizar otras tareas mientras los datos son procesados");  
                    $('#Tabla-Versiones').DataTable().ajax.reload();
                }
            },
            error : function(data){
                toastr.error("ERROR");

            }
            });
        }else {
           toastr.error("Debe seleccionar un proyecto"); 
        }
    }

    function verCambio(criterio) {
        var route = "{{url('getCambios')}}/"+criterio;
        $.get(route, function(data){
            //console.log(data);
            if(data.dato.length>0)
            {            
                if($(".caja table").length>0)
                    $(".caja table").remove();
                if($(".caja span").length>0)
                    $(".caja span").remove();
                var html = '<table class="table table-responsive table-inverse" >'
                    html += '<thead>'
                    html += '<tr>'
                    html += '<th>TIPO</th>'
                    html += '<th>Direccion</th>'
                    html += '<th>Notas</th>'
                    html += '</tr>'
                    html += '</thead>'
                    html += '<tbody>'
                    for(var i=0; i<data.dato.length; i++)
                    {                    
                        html += '<tr>'
                        html += '<td>'+data.dato[i].tipo+'</td>'
                        html += '<td class="ord">'+data.dato[i].comparado+'</td>'
                        html += '<td>'+data.notas+'</td>'
                        html += '</tr>'
                    }
                    html += '</tbody>'
                    html += '</table>'
                $(".caja").append(html);
            }else{
                if($(".caja table").length>0)
                    $(".caja table").remove();
                if($(".caja span").length>0)
                    $(".caja span").remove();
                var html = '<span class="label label-danger">No Existen Datos de comparacion</span>'
                $(".caja").append(html);
            }
        })
    }

    function eliminarVersion(criterio)
    {
        var texto = "Esta seguro dar de Eliminar el registro seleccionado?";
        var token = $("input[name=_token]").val();
        swal({
            title: "CONFIRMAR",
            text: texto,
            buttons: ["Cancelar", "Confirmar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = "{{url('eliminarVersion')}}";
            $.ajax({
                url: route,
                method: 'delete',
                headers : {'X-CSRF-TOKEN':token},
                data:{criterio},
                success: function(data) {
                    toastr.success(data);  
                    $('#Tabla-Versiones').DataTable().ajax.reload();                   
                }
            });
          }  
        });
    }

    $('#RFarchivo').fileinput(
    {
        language: 'es',
        // dropZoneEnabled: false,
        'showUpload' : false,
        //allowedFileExtensions: ['zip'],
        fileActionSettings: {
            showRemove: false,
            showUpload: false,
            showZoom: false,
            showDrag: false
        }, 
        //uploadAsync : true,
        // 'showPreview': true,
        previewFileIconSettings: {
            'docx': '<i class="fas fa-file-word text-primary"></i>',
            'xlsx': '<i class="fas fa-file-excel text-success"></i>',
            'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
            'jpg': '<i class="fas fa-file-image text-warning"></i>',
            'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
            'zip': '<i class="fas fa-file-archive text-muted"></i>',
        }
    });

    function verVersion(criterio)
    {
        //console.log(criterio);
        var directorio = $("#Select_Pasignado option:selected" ).text()+'-'+criterio.replace(/ /g, "") ;
        var criterio =  $("#Select_Pasignado").val();
        var ini = 1;
        var original = directorio;
        activar_tabla_directorio(directorio,criterio,ini,original);
    }

    function getVersion(){
        var criterio = $("#Select_Pasignado option:selected" ).text();
        var route = "{{url('getVersion2')}}/"+criterio;
        $.get(route, function(data){
            $("#Rversion").val('Version '+data);
            $(".vers").val(data);
        })
    }

    function activar_tabla_directorio(directorio,criterio,ini,original)
    {
        console.log(directorio,original,ini,criterio);

        $(".dirHere").val(directorio);
        $(".dirOriginal").val(original);
        $(".criterio").val(criterio);
        $(".ini").val(ini);

        $('#Tabla-ArchivosP').DataTable().destroy();
        var route = "{{url('listadoDirectorio')}}/"+directorio+"/"+criterio+"/"+ini+"/"+original;
        var t = $('#Tabla-ArchivosP').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":route,
            columns: [
                {data: null ,
                  render :  function(data, type, row, meta){
                    //console.log(data)
                    if(data.mensaje != '')
                    {
                      var htmlMensaje = '<div class="alert alert-danger alert-dismissible fade show" role="alert">'+row.mensaje+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                    '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                            '</div>';
                      $(".mensaje div").remove();
                      $(".mensaje").append(htmlMensaje);  
                    }else
                    {
                      $(".mensaje div").remove(); 
                    }
                    return null;
                  }
                },
                {data: null ,
                    "render" : function(data, type, row, meta) {
                        var html = '';
                        if(row.tipo == 'file' )
                        {
                            if(row.dir.split('.').pop() == 'docx' || row.dir.split('.').pop() == 'pdf' || row.dir.split('.').pop() == 'xlsx' || row.dir.split('.').pop() == 'png' ||  row.dir.split('.').pop() == 'jpg' || row.dir.split('.').pop() == 'jpeg'){
                                var ver = $(".dirHere").val();
                                ver = ver.split('-').pop();
                                ver = ver.toLowerCase();
                                var nombre =row.dir+'-'+row.directorio+'-'+ver+'-'+ $( "#Select_Pasignado option:selected" ).text();                          
                                var html='<div class="form-group" >';
                                    html+='<input type="checkbox" autocomplete="off" value="'+nombre+'">';
                                html +="</div>";
                            }
                        }
                    return html;
                    },
                },
                {data: 'dir' ,
                    render :  function(data, type, row, meta){
                        //console.log(data);
                        var html = '';

                        if(row.tipo == 'dir' )
                        {        
                            var direc =  row.directorio.replace(/[/]/g, '-')
                            html += '<i class="fas fa-folder-open"></i> '; 
                            html += '<a href="#" class="stretched-link" onclick="activar_tabla_directorio(`'+direc+'`,`'+row.criterio+'`,2,`version1`)">'+row.dir+'</a>'                            
                        }else{
                            if(row.tipo == 'file' )
                            {

                            var direc =  row.directorio.replace(/[/]/g, '-')
                            html += '<i class="far fa-file"></i> ';
                            html += '<a href="#" class="stretched-link" onclick="descargar(`'+direc+'`,`'+row.dir+'`)">'+row.dir+'</a>' 
                            }

                        }

                        return html;
                    }
                },       
                {data: 'tipoarchivo'},       
                {data: 'fecha'},       
                {data: 'usuario'},       
            ]
        });

        t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-ArchivosP').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    }

    var datosOnline = [];
    function BuscarComparar()
    {
        $('#Tabla-ArchivosP tbody tr').each(function(index, value){
            if( $(this).find('input').prop('checked') ) {
                datosOnline.push($(this).find('input')[0].value);
                //datosOnline.push($(this).find('input')[0].value);
            }            
        });
        if(datosOnline.length > 0){
            var route = "{{url('leerDocumento2')}}"; 
            var token = $('input[name="_token"]').val();
            $.ajax({ 
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{datosOnline},
                success : function(data)
                {
                    datosOnline = [];
                    toastr.success('Estado en espera');
                },
                error : function(data)
                {
                    toastr.error("ERROR");
                    console.log(data);
                }

            });
        }else
            toastr.error('Debe seleccionar almenos un archivo para enviar a deteccion de PLAGIO');

    }

    function descargar(criterio,name){
        criterio = $.base64.encode(criterio);
        name = $.base64.encode(name);
        var url = "{{url('getDocumento')}}/"+criterio+"/"+name;
        window.open(url, '_blank');
    }

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

</script>
<style type="text/css" media="screen">
    textarea{
        resize: none;
        height: 150px;
    }    
    .ord{
    width: 100%;
    display: block;
    
    white-space: pre-line;
    }
</style>
@endsection