<div class="modal fade" id="modal-ERole" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog ">
		{!! Form::open(['id'=> 'FERole','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title titleRole" >Formulario - Nuevo Rol</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
		  		<input type="hidden" class="form-control idRole" value="">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('name','Rol') !!}
						{!! Form::text( 
							'name',null,
							['class'=>'form-control',
							'placeholder' =>'Nombre Rol',
							'id'=>'Erole']) !!}
						</div>
					</div> 
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('name','Descripcion Rol') !!}
						{!! Form::textarea( 
							'descripcion',null,
							['class'=>'form-control',
							'placeholder' =>'Descripcion de Rol',
							'id'=>'Edescripcion']) !!}
						</div>
					</div> 
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
			        <div class="form-group">
			            <strong>Permission:</strong>
			            <br/>
			            @foreach($permission as $value)
			                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name Epermission')) }}
			                {{ $value->name }}</label>
			            <br/>
			            @endforeach 
			        </div>
		    	</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'btnERole', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>