@extends('template.app')

@section('title','Lista de Roles')
@section('main-content')
   <!-- Your Page Content Here -->
    <!-- if(Auth::user()->role->permission->add == 1) -->
    @if(Auth::user()->hasPermissionTo('role-create'))
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-RRole">
                Nuevo Rol
            </button>
        </div>
    @endif
    <!-- endif -->
    <hr>
    <div class="table-responsive table-striped">
        <table id="Tabla-Roles" class="table table-bordered table-striped" width="100%">
            <thead > 
                <tr>
                    <th style="text-align: center;" >#</th>
                    <th style="text-align: center; " width="30%">Nombre</th>
                    <th style="text-align: center; " width="50%">Descripcion</th>
                    <th style="text-align: center; " >Accion</th>
                </tr> 
            </thead>
            <tbody style="text-align: center;">

            </tbody>
        </table>
    </div>   
 
    @include('admin.role.modal-crear')
    @include('admin.role.modal-edit')
    @include('admin.role.modal-permisos')

@endsection
@section('js')
<script type="text/javascript">
    $( document ).ready(function() {
       activar_tabla_roles();
       cambio();
    });

    $( document ).ajaxStop(function() {
        //preloader();
    });


    function activar_tabla_roles() {
        var t = $('#Tabla-Roles').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [5,10, 25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":'{!! url('list_roles') !!}',
            columns: [
                {data: 'id'},
                {data: 'name' },
                {data: 'descripcion' , className :'ord'},
                {
                "defaultContent" : " ",
                "searchable" : false,
                "orderable" : false,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                    var edit = "{{Auth::user()->hasPermissionTo('role-edit') }}";
                    if( edit != '')
                    {

                        html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-ERole" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';
                    }


                    html +="</div>";
                    return html;
                },
            }]
        });
        t.on( 'draw.dt', function () {
            var PageInfo = $('#Tabla-Roles').DataTable().page.info();
                 t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
                } );
            } );
    }

    var rolePermissions;
    function llenarDatosEdit(criterio)
    {
        var route = "{{url('roles')}}/"+criterio+"/edit";
        $.get(route, function(data){ 
            rolePermissions = data.rolePermissions;
            $(".idRole").val(data.role.id);
            $("#Erole").val(data.role.name);
            $("#Edescripcion").val(data.role.descripcion);
            $(".titleRole").html("Actualiando ROL");
            $('.Epermission').each(function() {
                //console.log(this.value,rolePermissions)
                this.checked = false;
                for(var i=0; i<rolePermissions.length; i++)
                {
                    if(this.value == rolePermissions[i].permission_id){
                        this.checked = true;
                    }
                    // else
                    // {
                    //     this.checked = false;
                    // }
                }
            });
        });
    }

    $("#btnRRole").click(function(e){
        var registerForm = $("#FRRole");
        var formData = registerForm.serialize();
        
        var route = "{{route('roles.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data:formData,
            success : function(data){
                $("#modal-RRole").modal("toggle");
                toastr.success("Rol Registrado.");  
                $('#Tabla-Roles').DataTable().ajax.reload();
            }
        });
 
    });

    $("#btnERole").click(function(e){
        var criterio = $(".idRole").val();
        var registerForm = $("#FERole");
        var formData = registerForm.serialize();

        var route = "{{url('actualizarRoles')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            //cache: false,
            data:formData,
            success : function(data){
                $("#modal-ERole").modal("toggle");
                toastr.success("Rol Actualizado.");  
                $('#Tabla-Roles').DataTable().ajax.reload();
            }
        });

    });

    //Abre modal y llena los datos de los permisos a un rol
    function openmodal(id) { 
        var route = "{{url('permissions')}}/"+id+"/edit";
        $.get(route, function(data){
            console.log(data);
            $("#EPTitle-Label").html("Editando Permisos del Rol:  "+data.role_name);
            $("#id").val(data.role_id);
            document.getElementsByClassName("Eedit")[0].checked = false;
            document.getElementsByClassName("Eremove")[0].checked = false;
            document.getElementsByClassName("Eadd")[0].checked = false;
            if(data.add=="1")
                {document.getElementsByClassName("Eadd")[0].checked = true;}
            if(data.edit=="1")
                {document.getElementsByClassName("Eedit")[0].checked = true;}
            if(data.remove=="1")
                {document.getElementsByClassName("Eremove")[0].checked = true;}
        });
    };

    $("#btnEPermissions").click(function()
    {
        var id = $("#id").val();
        var add = $(".Eadd").prop('checked');
        var edit = $(".Eedit").prop('checked');
        var remove = $(".Eremove").prop('checked');
        //console.log(remove);
        var route = "{{url('permissions')}}/"+id+"";
        var token = $("input[name=_token]").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {add,edit,remove},
            success: function(data){
                $("#MeditPermissions").modal('hide');
                toastr.warning('Se Actualizaron Los Permisos Del Rol: '+data.name);
                $("#Tabla-Roles").DataTable().ajax.reload();
            },
            error:function(data)
            {
                toastr.error('ERROR');
                console.log(data);
            }  
        });
    }); 

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
    </script>
<style type="text/css">
    .ord{
    
    
    
    white-space: pre-line;
    }
    textarea{
        resize: none;
        height: 150px;
    }
</style>
@endsection