@extends('template.app')

@section('title','Archivos a Comparar')
@section('main-content')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/jcrop/jquery.Jcrop.min.css')}}">
<input type="hidden" id="ancho" value="0">
<div class="row">
	<div class="col-md-12">
		<!-- if(Auth::user()->role->permission->add == 1) -->
        @if(Auth::user()->hasPermissionTo('CompararOcr-create'))
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rarchivo">
		        Nuevo Registro Archivo OCR
		    </button>
		</div>
        @endif
		<br>
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-warning"  onclick="BuscarComparar();">
		        COMPARAR ARCHIVOS
		    </button>
		</div>
		<!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-OCR" class="table table-bordered table-striped" width="100%">
		        <thead> 
		            <tr>
		                <th>#</th>
		                <th>check</th>
		                <th>NOMBRE ARCHIVO</th>
		                <th>EXTENSION</th>
                        <th>RECORTE</th>
		                <th>CREADO POR</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;"> 

		        </tbody>
		    </table>
		</div>
	</div>
</div>
@include('admin.ocr.modal-crear')
@include('admin.ocr.modal-recorte')

@endsection

@section('js')
<script src="{{asset('plugins/jcrop/jquery.Jcrop.min.js')}}"></script>
<script type="text/javascript">
    var size;
	jQuery(document).ready(function($) {
		cambio();
        
        var ubicacion;
	});

	function activar_tabla_repositorio() {
    	var t = $('#Tabla-OCR').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_ocr') !!}',
        columns: [
            {data: 'id' },
            {data: null ,
            	"render" : function(data, type, row, meta) {
	                var html='<div class="form-group" >';
	                    html+='<input type="checkbox" autocomplete="off" value="'+row.id+'-'+row.extension+'">';
	                html +="</div>";
	                return html;
            	},
            },
            {data: 'nombrearchivo' },
            {data: 'extension' },
            {data: 'recorte' ,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                    if(row.recorte == 'SI'){
                        
                        var url = "{{asset('archivocomparar/OCR/RECORTE')}}/Recorte-"+row.nombrearchivo+'?'+Math.random(); 
                        html += '<img onclick="verImagen(this)" src="'+url+'" class="img-responsive" alt="Image" height="42" width="42" align="center" />';
                    }else{
                        var url = "{{asset('archivocomparar/OCR/RECORTE')}}/"+row.nombrearchivo+'?'+Math.random(); 
                        html += '<img onclick="verImagen(this)" src="{{asset("archivocomparar/OCR")}}/'+row.nombrearchivo+'" class="img-responsive" alt="Image" height="42" width="42" align="center" />';
                    }
                    html +="</div>";
                    return html;
                },
            },
            {data: 'nombreusuario' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';
                    var edit = "{{Auth::user()->hasPermissionTo('CompararOcr-edit') }}";
                    var remove = "{{Auth::user()->hasPermissionTo('CompararOcr-delete') }}";
                    if(edit !=''){
                        html+='<a style="width:100px; margin:5px;" class="btn btn-info btn-xs text-white" onclick="descargar('+row.id+');"><i class="fas fa-wrench"></i> Descargar </a> </br>'; 

                        html+='<a style="width:100px; margin:5px;" class="btn btn-warning btn-xs text-white" onclick="recortar(`'+row.nombrearchivo+'`,`'+row.id+'`);"   data-toggle="modal" data-target="#modal-Rrecorte"><i class="fas fa-wrench"></i> Recorgar Imagen </a> </br>'; 
                    }
                    if(remove !='')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="eliminarArchivo('+row.id+');"><i class="fas fa-wrench"></i> Eliminar </a> </br>'; 

                html +="</div>";
                return html;
            },
        }]
    }); 

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-OCR').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    activar_tabla_repositorio();

    $("#btnRarchivo").click(function(){
        var archivo = $("#RFarchivo").fileinput('getFileStack')[0];
        var formData = new FormData();
        formData.append('archivo',archivo);

        var route = "{{route('ocr.store')}}";
        var token = $('input[name="_token"]').val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            method: 'POST',
            data:formData,
            contentType: false,
            cache: false,
            processData : false,
            success : function(data){
                $("#modal-Rarchivo").modal('toggle');
                toastr.success("Archivo Almacenado");  
                $('#Tabla-OCR').DataTable().ajax.reload();
            },
            error : function(data){
            	toastr.error("ERROR");

            }
        });
    })
    var jp;
    function recortar(criterio,id){
        $("#idRec").val(id);
        ubicacion = criterio;
        $("#RecortarImagen").removeAttr("style");
        var url = "{{asset('archivocomparar/OCR')}}/"+criterio+"?"+Math.random();    
        $("#RecortarImagen").attr("src",url);
        setTimeout(function() {
            var imagen = $('#RecortarImagen');
            $("#ancho").val(imagen.width());
            if(jp!=null){

                   jp.destroy();

                }
            jp = $.Jcrop('#RecortarImagen',{
                shadeOpacity:0.95,
                
                onSelect: function(c){
                size = {x:c.x,y:c.y,w:c.w,h:c.h};
                $("#recortar").css("visibility", "visible");     
                // $("#descargar").css("visibility", "visible");     
                }
            });
            // $('#RecortarImagen').Jcrop({
            //   //aspectRatio: 1,
            //   shadeOpacity:0.95,
            //   //onSelect: showPreview,
            //   onSelect: function(c){
            //    size = {x:c.x,y:c.y,w:c.w,h:c.h};
            //    $("#recortar").css("visibility", "visible");     
            //    // $("#descargar").css("visibility", "visible");     
            //   }
            // });
        }, 200);
    }

    jQuery('#modal-Rrecorte').on('shown.bs.modal',function() {
       // $("#RecortarImagen").attr("src",'#');
       
        var anchoCSS = $("#ancho").val(),
        anchoEfectivo = jQuery(this).find('.modal-content').outerWidth();
        if(anchoEfectivo < anchoCSS)
            margenIzquierdo=(anchoCSS/2-anchoEfectivo);  
        else   
            margenIzquierdo = 0; 
        console.log(anchoCSS/2, anchoCSS,margenIzquierdo,anchoEfectivo);
        jQuery(this).find('.modal-content').css('margin-left',margenIzquierdo);
    
    });
    jQuery('#modal-Rrecorte').on('hidden.bs.modal', function () {
        $("#imgrecortada_img").prop('src','#');
        $(".recortado").hide();

    });

    $("#btnRarchivorecorte").click(function(){
        var img = $("#RecortarImagen").attr('src');
        var idRec = $("#idRec").val();
        var extension = ubicacion.split('.').pop();
        var route = '{{(url("recortarImagen"))}}';
        var token = $("input[name=_token]").val();
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            method: 'get',
            data:{size,ubicacion,extension,idRec},
            success: function(data){
                
                //$("#ancho").val(data.ancho);
                var url = "{{asset('archivocomparar/OCR/RECORTE')}}/"+data.url+"?"+Math.random();
                //$("#imgrecortada_img").attr("src",url);
                document.getElementById('imgrecortada_img').src = '';
                document.getElementById('imgrecortada_img').src = url;
                $(".recortado").show();

                $('#Tabla-OCR').DataTable().ajax.reload();
            },
            error : function(data) {
                toastr.error('ERROR')
            }
        });
        //$("#descargar").show();
        //$("#imgrecortada_img").attr('src','ImagenRecortada.php?x='+size.x+'&y='+size.y+'&w='+size.w+'&h='+size.h+'&img='+img);
    });
	var datosComparar = [];
	function BuscarComparar()
	{
		$('#Tabla-OCR tbody tr').each(function(index, value){
			if( $(this).find('input').prop('checked') ) {
                datosComparar.push($(this).find('input')[0].value);
            }            
        });
		if(datosComparar.length > 0){
			var route = "{{url('leerDocumentoOCR')}}"; 
			var token = $('input[name="_token"]').val();
			$.ajax({ 
		        url: route,
		        headers: {'X-CSRF-TOKEN': token},
		        type: 'get',
		        dataType: 'json',
		        data:{datosComparar},
	            success : function(data)
	            {
	            	datosComparar = [];
	            	toastr.success('Estado en espera');
	            },
	            error : function(data)
	            {
	            	toastr.error("ERROR");
	            	console.log(data);
	            }

			});
		}else
			toastr.error('Debe seleccionar almenos un archivo para enviar a deteccion de PLAGIO');

	}

    function descargar(criterio)
    {
        var url = "{{url('getArchivoOcr')}}/"+criterio;
        window.open(url, '_blank');
    }

    function eliminarArchivo(criterio)
    {
        Swal.fire({
            title: 'Esta seguro?',
            text: "De eliminar el Registro Seleccionado!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
        }).then((result) => {
            console.log(result);
            if (result.value) {
                var route = "{{url('eliminarArchivoOcr')}}/"+criterio;
                var token = $("input[name=_token]").val();
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    success : function(data)
                    {   
                        if(data.mensaje == '')
                        {
                            toastr.success('Registro Eliminado');
                            $('#Tabla-OCR').DataTable().ajax.reload();
                        }else
                            toastr.error(data.mensaje);
                    }
                });
            }
        })
    }
    //cargarextension();
    function cargarextension()
    {   
        var route = "{{url('getExtensiones')}}";
        $.ajax({
            url:route,
            method : 'get',
            success: function(data){
                console.log(data);
                var extension = [];
                for(var i=0; i<data.length; i++)
                {
                    extension.push(data[i].extension.toLowerCase());
                }
                $('#RFarchivo').fileinput('destroy').fileinput(
                {
                    language: 'es',
                    dropZoneEnabled: false,
                    showUpload : false,
                    allowedFileExtensions: extension
                });
            }
        })
    }

     $('#RFarchivo').fileinput('destroy').fileinput(
    {
        language: 'es',
        dropZoneEnabled: false,
        showUpload : false,
        allowedFileExtensions: ['png','jpg','jpeg'],
        maxImageHeight : '800px', 
        maxImageWidth : '800px' 
    });
    function verImagen(e)
    {
        Swal.fire({
          imageUrl: e.src,
          imageWidth: 320,
          imageHeight: 420,
          imageAlt: 'Custom image',
        })
        
    }
	function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };	 
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}

 /*   .modal-content {
  float:left;
}*/
</style>
@endsection