<div class="modal fade" id="modal-Rrecorte" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog  modal-lg">
		{!! Form::open(['id'=> 'FRNrecorte','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="margin-left: auto;">RECORTE DE IMAGEN</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<input type="hidden" id="idRec" value="">
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12" style="text-align: -webkit-center;">
							{!! Form::label('archivo','Seleccione la seccion que quiera recortar') !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-12" style="text-align: -webkit-center;">						
							<img src="#" id="RecortarImagen"   alt="Responsive image">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row recortado" style="display: none;">
						<div class="col-md-12">
						{!! Form::label('archivo','IMAGEN RECORTADA') !!}
						<img src="#" id="imgrecortada_img" class="img-fluid" alt="Responsive image">
						</div>
					</div>
				</div>
				
				<!-- <div class="form-group mb-2" style="padding:20px;"><input class="btn btn-primary" type='button' id="recortar" value='Recortar Imagen'></div> -->		
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Recortar Imagen', 
				$attributes = ['id'=>'btnRarchivorecorte', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>