<div class="modal fade" id="modal-Rproyecto" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNproyecto','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" >Formulario Nuevo Proyecto</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Nombre Proyeto') !!}
							{!! Form::text( 
								'nombre',null,
								['class'=>'form-control',
								'placeholder' =>'Nombre del Proyecto',
								'id'=>'Rnombre']) !!}
						</div>
						<div class="col-md-12">
							{!! Form::label('type','Descripcion') !!}
							{!! Form::text( 
								'descripcion',null,
								['class'=>'form-control',
								'placeholder' =>'Descripcion del Proyecto',
								'id'=>'Rdescripcion']) !!}
						</div>
						<div class="col-md-12">
							{!! Form::label('type','Tipo Proyecto') !!} <br>
<!-- 							{!! Form::text( 
								'tipo_proyecto',null,
								['class'=>'form-control',
								'placeholder' =>'practicas, examenes, imagenes, otros',
								'id'=>'Rtipo_proyecto']) !!} -->
							<label>
							{{ Form::radio('status', 'Publico', null,['class'=>'Rtipo_proyecto','name'=>'option']) }} <span>Publico</span> 
							</label><br>
							<label>
							{{ Form::radio('status', 'Privado', null ,['class'=>'Rtipo_proyecto','name'=>'option']) }} <span>Privado</span>
							</label>
						
						</div>
						<div class="col-md-12">
							{!! Form::label('type','Categoria Proyeto') !!}
							{!! Form::select(
							'categoria_proyecto_id',$CategoriaProyecto,
							null,
							['class'=>'form-control',
							'placeholder'=>'Seleccione una Opcion...',
							'id'=>'Rcategoria_id']) !!} 
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Proyecto', 
				$attributes = ['id'=>'btnRproyecto', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>