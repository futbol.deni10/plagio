<div class="modal fade" id="modal-Eproyecto" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNEproyecto','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" >Formulario Actualizar Proyecto</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<input type="hidden" class="idProye" value="">
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							{!! Form::label('type','Nombre Proyeto') !!}
							{!! Form::text( 
								'nombre',null,
								['class'=>'form-control',
								'placeholder' =>'Nombre del Proyecto',
								'id'=>'Enombre']) !!}
						</div>
						<div class="col-md-12">
							{!! Form::label('type','Descripcion') !!}
							{!! Form::text( 
								'descripcion',null,
								['class'=>'form-control',
								'placeholder' =>'Descripcion del Proyecto',
								'id'=>'Edescripcion']) !!}
						</div>
						<div class="col-md-12">
							{!! Form::label('type','Tipo Proyecto') !!}<br>
								<label>
							{{ Form::radio('status', 'Publico', null,['class'=>'Etipo_proyecto','name'=>'option2']) }} <span>Publico</span> 
							</label><br>
							<label>
							{{ Form::radio('status', 'Privado', null ,['class'=>'Etipo_proyecto','name'=>'option2']) }} <span>Privado</span>
							</label>

						</div>
						<div class="col-md-12">
							{!! Form::label('type','Categoria Proyeto') !!}
							{!! Form::select(
							'categoria_proyecto_id',$CategoriaProyecto,
							null,
							['class'=>'form-control',
							'placeholder'=>'Seleccione una Opcion...',
							'id'=>'Ecategoria_id']) !!} 
						</div>
					</div>
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar Proyecto', 
				$attributes = ['id'=>'btnEproyecto', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>