@extends('template.app')

@section('title','Proyectos')
@section('main-content')

<div class="row">
	<div class="col-md-12">
        <!-- if(Auth::user()->role->permission->add == 1) -->
		<div align="left" style="border:auto;">
		    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-Rproyecto">
		        Nuevo Proyecto
		    </button>
		</div>
        <!-- endif -->
		<hr>
		<div class="table-responsive">
		    <table id="Tabla-Proyecto" class="table table-bordered table-striped" width="100%">
		        <thead>  
		            <tr> 
		                <th>#</th>
		                <th>NOMBRE DEL PROYECTO</th>
                        <th>CATEGORIA PROYECTO</th>
		                <th>CREADO POR</th>
                        <th>ESTADO</th>
                        <th>FECHA</th>
		                <th>ACCION</th>
		            </tr>
		        </thead>
		        <tbody style="text-align: center;">
 
		        </tbody>
		    </table>
		</div>
	</div>
</div>
@include('admin.proyectos.modal-crear')
@include('admin.proyectos.modal-editar')

@endsection

@section('js')
<script type="text/javascript">

	function activar_tabla_proyecto() {
    	var t = $('#Tabla-Proyecto').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "searchDelay" : 500 ,
        "lengthMenu": [5,10, 25, 50, 75 ],
        "responsive": true,
        "language": {
        "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
        },
        "ajax":'{!! url('listar_proyectos') !!}',
        columns: [
            {data: 'id' },
            {data: 'nombre' },
            {data: 'nombre_categoria' },
            {data: 'username' },
            {data: 'baja' ,
                render: function(data, type, row, meta) {
                    var html = ''
                    if ( row.baja == 'NO' )
                    {
                        html = '<span class="badge badge-success" style="width:80px;"> Activo </span>';
                                
                    }else {
                        html = '<span class="badge badge-danger" style="width:80px;"> Inactivo </span>';
                    }
                    return html;
                }
            },
            {data: 'created_at' },
            {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html='<div class="form-group" >';

                    html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-Eproyecto" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';
                    if(row.baja == 'NO')
                        html+='<a style="width:100px; margin:5px;" class="btn btn-danger btn-xs text-white" onclick="estadoProyecto('+row.id+',1);"><i class="fas fa-wrench"></i> Dar de Baja </a> </br>'; 
                    else
                        html+='<a style="width:100px; margin:5px;" class="btn btn-success btn-xs text-white" onclick="estadoProyecto('+row.id+',2);"><i class="fas fa-wrench"></i>Activar</a> </br>';

                html +="</div>";
                return html;
            },
        }]
    });

    	t.on( 'draw.dt', function () {
        var PageInfo = $('#Tabla-Proyecto').DataTable().page.info();
             t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
    } 
    activar_tabla_proyecto();

	function cargarextension()
	{	
		var route = "{{url('getExtensiones')}}";
		$.ajax({
			url:route,
			method : 'get',
			success: function(data){
				var extension = [];
				for(var i=0; i<data.length; i++)
				{
					extension.push(data[i].extension.toLowerCase());
				}
				$('#RFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});

               	$('#EFarchivo').fileinput('destroy').fileinput(
                {
                	language: 'es',
			        dropZoneEnabled: false,
			        showUpload : false,
			        allowedFileExtensions: extension
               	});
			}
		})
	}
	//cargarextension();

    $("#btnRproyecto").click(function(){
        $("#Rnombre").val($("#Rnombre").val().toUpperCase());
        var nombre = $("#Rnombre").val();
        var descripcion = $("#Rdescripcion").val();
        var tipo_proyecto = $('input[name=option]:checked').val();
        var categoria_proyecto_id = $("#Rcategoria_id").val();
        var route = "{{route('proyectos.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({  
            url:route,
            method:'POST',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            data: {nombre,descripcion,tipo_proyecto,categoria_proyecto_id},
            success : function(data){                 
                $("#modal-Rproyecto").modal('toggle');
                toastr.success('Proyecto Registrado');
                $("#Tabla-Proyecto").DataTable().ajax.reload();
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    });

    $("#btnEproyecto").click(function(){
        var criterio = $(".idProye").val();
        $("#Rnombre").val($("#Rnombre").val().toUpperCase());
        var nombre = $("#Enombre").val();
        var descripcion = $("#Edescripcion").val();
        var tipo_proyecto = $('input[name=option2]:checked').val();
        var categoria_proyecto_id = $("#Ecategoria_id").val();
        var route = "{{url('proyectos')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({  
            url:route,
            method:'PUT',
            headers : {'X-CSRF-TOKEN':token},
            dataType:'json',
            data: {nombre,descripcion,tipo_proyecto,categoria_proyecto_id},
            success : function(data){                 
                $("#modal-Eproyecto").modal('toggle');
                toastr.success('Proyecto Actualizado');
                $("#Tabla-Proyecto").DataTable().ajax.reload();
            },
            error : function(data){
                toastr.error('ERROR');
            }
        });
    });

    function llenarDatosEdit(criterio){
        $("#FRNEproyecto").trigger("reset");
        var route = "{{url('proyectos')}}/"+criterio+'/edit';
        $.get(route, function(data){
            $("#Enombre").val(data.nombre)
            $("#Edescripcion").val(data.descripcion)
            var $radios = $('input:radio[name=option2]');
            //if($radios.is(':checked') === false) {
                $radios.filter('[value='+data.tipo_proyecto+']').prop('checked', true);
            //}
            //$("#Etipo_proyecto").val(data.tipo_proyecto)
            $("#Ecategoria_id").val(data.categoria_proyecto_id)
            $(".idProye").val(data.id)
        });
    }

    function estadoProyecto(id,criterio)
    {
        //console.log(criterio);
        if(criterio == 1)
            var texto = "Esta seguro dar de baja el registro seleccionado?"
        else
            var texto = "Esta seguro de Activar el registro seleccionado?"
        swal({
            title: "CONFIRMAR",
            text: texto,
            buttons: ["Cancelar", "Confirmar"],
        })
        .then((willDelete) => {
          if (willDelete) {
            var route = "{{url('estadoProyecto')}}";
            $.ajax({
                url: route,
                method: 'get',
                data:{id,criterio},
                success: function(data) {
                    toastr.success(data);  
                    $('#Tabla-Proyecto').DataTable().ajax.reload();
                   
                }
            });
          }  
        });
    }

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };

    cambio(); 
</script>
<style type="text/css">
	.fileinput-upload-button{
		background-color: red;
		color: white;
	}
	.fileinput-upload-button::hover{
		color:white;
	}
</style>
@endsection