<div class="modal fade" id="modal-Rarchivo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		{!! Form::open(['id'=> 'FRNarchivo','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title" style="margin-left: auto;">Formulario Nuevo Archivo</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('archivo','Seleccione un Archivo') !!}
						{!! Form::file(
							'archivo',
							['class'=>'file','id'=>'RFarchivo','type'=>'file']) !!}
						</div>
					</div>
				</div>		
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Registrar Archivo', 
				$attributes = ['id'=>'btnRarchivo', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>