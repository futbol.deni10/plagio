@extends('template.app')

@section('title','Lista de Categoria Proyecto')
@section('main-content')
   <!-- Your Page Content Here -->
    <!-- if(Auth::user()->role->permission->add == 1) -->
    @if(Auth::user()->hasPermissionTo('CategoriaProyecto-create'))
        <div align="left" style="border:auto;">
            <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#modal-RCategoria">
                Nueva Categoria
            </button>
        </div>
    @endif
    <!-- endif -->
    <hr>
    <div class="table-responsive table-striped">
        <table id="Tabla-Categorias" class="table table-bordered table-striped" width="100%">
            <thead > 
                <tr>
                    <th style="text-align: center;" >#</th>
                    <th style="text-align: center;" >Nombre</th>
                    <th style="text-align: center;" >Accion</th>
                </tr> 
            </thead>
            <tbody style="text-align: center;">

            </tbody>
        </table>
    </div>  
 
    @include('admin.categoriaproyecto.modal-crear')
    @include('admin.categoriaproyecto.modal-editar')

@endsection
@section('js')
<script type="text/javascript">
    $( document ).ready(function() {
        activar_tabla_categorias();
        cambio();
    });

    $( document ).ajaxStop(function() {
        //preloader();
    });


    function activar_tabla_categorias() {
        var t = $('#Tabla-Categorias').DataTable({
            "processing"  : true,
            "serverSide"  : true,
            "searchDelay" : 500 ,
            "lengthMenu": [5,10, 25, 50, 75 ],
            "responsive": true,
            "language": {
            "url": '{!! asset('/plugins/datatables.net/latino.json') !!}',
            },
            "ajax":'{!! url('listar_Categorias') !!}',
            columns: [
                {data: 'id' },
                {data: 'nombre' },
                {
                "defaultContent" : " ",
                "searchable" : false,
                "orderable" : false,
                "render" : function(data, type, row, meta) {
                    var html='<div class="form-group" >';
                    var edit = "{{Auth::user()->hasPermissionTo('CategoriaProyecto-edit') }}";
                    if(edit !='')                   
                        html+='<a style="width:100px; margin:5px;" data-toggle="modal" data-target="#modal-ECategoria" class="btn btn-warning btn-xs text-white" onclick="llenarDatosEdit('+row.id+');"><i class="fas fa-wrench"></i> Editar </a> </br>';
                   
                    html +="</div>";
                    return html;
                },
            }]
        });
        t.on( 'draw.dt', function () {
            var PageInfo = $('#Tabla-Categorias').DataTable().page.info();
                 t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
                } );
            } );
    }

    function llenarDatosEdit(criterio)
    {
        var route = "{{url('categoriaproyectos')}}/"+criterio+"/edit";
        $.get(route, function(data){      
            
            $(".idCategoria").val(data.id);
            $("#Ecategoria").val(data.nombre);
            //$(".titleRole").html("Actualiando ROL");
        });
    }

    $("#btnRCategoria").click(function(e){
        var registerForm = $("#FRCategoria");
        var formData = registerForm.serialize();

        var route = "{{route('categoriaproyectos.store')}}";
        var token = $("input[name=_token]").val();
        $.ajax({ 
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            dataType: 'json',
            data:formData,
            success : function(data){
                $("#modal-RCategoria").modal("toggle");
                toastr.success("Categoria Registrada.");  
                $('#Tabla-Categorias').DataTable().ajax.reload();
            }
        });
 
    });

    $("#btnECategoria").click(function(e){
        var criterio = $(".idCategoria").val();
        var nombre = $("#Ecategoria").val();
        var route = "{{url('categoriaproyectos')}}/"+criterio;
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data:{nombre},
            success : function(data){
                $("#modal-ECategoria").modal("toggle");
                toastr.success("Categoria Actualizada.");  
                $('#Tabla-Categorias').DataTable().ajax.reload();
            }
        });

    });

    function cambio (){
        var route = "{{url('getPersonalizar')}}"
        var token = $("input[name=_token]").val();
        $.ajax({
            url : route,
            headers : {'X-CSRF-TOKEN':token},
            method : 'get',
            //dataType : 'json',
            //data : estiloGlobal,
            success : function (data)
            {
                for (var i = 0; i < data.length; i++) {
                if(data[i].nav_type != null)
                    $(".pcoded").attr("nav-type", data[i].nav_type);
                if(data[i].navbar_logo != null) 
                    $(".navbar-logo").attr("logo-theme", data[i].navbar_logo)
                if(data[i].pcoded_navigatio_lavel != null)
                    $(".pcoded-navigatio-lavel").attr("menu-title-theme", data[i].pcoded_navigatio_lavel)
                if(data[i].pcoded_header != null)
                    $(".pcoded-header").attr("header-theme", data[i].pcoded_header)
                if(data[i].pcoded_navbar != null)                        
                    $(".pcoded-navbar").attr("navbar-theme", data[i].pcoded_navbar)
                if(data[i].active_item_them != null)
                    $(".pcoded-navbar").attr("active-item-theme", data[i].active_item_them)
                if(data[i].sub_item_theme != null)
                    $(".pcoded-navbar").attr("sub-item-theme", data[i].sub_item_theme)
                if(data[i].themebg_pattern != null)
                    $("body").attr("themebg-pattern", data[i].themebg_pattern)
                if(data[i].vertical_effect != null)
                    $(".pcoded").attr("vertical-effect", data[i].vertical_effect)
                if(data[i].item_border_style != null)
                    $(".pcoded-navbar .pcoded-item").attr("item-border-style", data[i].item_border_style)
                if(data[i].dropdown_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("dropdown-icon", data[i].dropdown_icon)
                if(data[i].subitem_icon != null)
                    $(".pcoded-navbar .pcoded-hasmenu").attr("subitem-icon", data[i].subitem_icon)
                var usuario = '{{Auth::user()->id}}';
                if(data[i].imagen != null)
                {
                    var url = 'personalizado/user/'+usuario+'/'+data[i].imagen;
                    $('head').append('<style>.main-menu:before{content: ""; background-image: url('+url+'); background-size: cover; position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.2;}</style>');
                    
                }
                }
                
            },
            error : function (data){
                toastr.error("ERROR");
            }
        })
    };
    </script>

@endsection