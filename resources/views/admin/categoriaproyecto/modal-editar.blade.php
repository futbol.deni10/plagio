<div class="modal fade" id="modal-ECategoria" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog ">
		{!! Form::open(['id'=> 'FRECategoria','autocomplete' => 'off']) !!}
		<div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<h5 class="modal-title titleRole" >Formulario - Actualizar Categoria</h5>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			      	<span aria-hidden="true">&times;</span>
			    </button>
		  	</div>
		  	<input type="hidden" class="idCategoria" value="">
		  	<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
						{!! Form::label('categoria','Categoria') !!}
						{!! Form::text( 
							'nombre',null,
							['class'=>'form-control',
							'placeholder' =>'Nombre Categoria',
							'id'=>'Ecategoria']) !!}
						</div>
					</div> 
				</div>
		  	</div>
		  	<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'btnECategoria', 'class'=>'btn btn-primary'])!!}
		  	</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>