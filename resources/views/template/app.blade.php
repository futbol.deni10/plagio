<!DOCTYPE html>
<html lang="en" id="prt">
@section('htmlheader')
    @include('template.partials.htmlheader')
@show
<body>
	@include('template.partials.preloader')
	<div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
			@include('template.partials.navbar')
			<div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                	@include('template.partials.sidebar')
                	<div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">                                        
                                        <div class="row justify-content-center">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">@yield('title')</div>

                                                    <div class="card-body">
                                                        
                                	                   @yield('main-content')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                	</div>
                                </div>

                                <div id="styleSelector">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('template.partials.scripts')
    @yield('js')
    <script type="text/javascript">
 

        $( document ).ready(function() {        
            
            actualizarAsignacion();
        });

        $(".btnPersonalizar").click(function(e){
            //console.log(estiloGlobal);
            var route = "{{url('storePersonalizar')}}";
            var imagen = $("#customFile").fileinput('getFileStack')[0];
            var token = $("input[name=_token]").val();
            var formData = new FormData();
            formData.append('file',imagen);
            for (var key in estiloGlobal ) {
                formData.append(key, estiloGlobal[key]);
            }
            //formData.append('estiloGlobal',estiloGlobal);
            $.ajax({
                url : route,
                headers : {'X-CSRF-TOKEN':token},
                method : 'POST',
                //dataType : 'json',
                data : formData,
                contentType: false,
                cache: false,
                processData : false,
                success : function (data)
                {
                    location.reload(); 
                    toastr.success('Cambio Realizado');
                    //$( "#pcoded" ).load(window.location.href + " #pcoded" );
                    //location.reload(); 
                },
                error : function (data){

                }
            })
        });

        $("#customFile").fileinput({
            language: 'es',
            dropZoneEnabled: false,
            showUpload : false,
            allowedFileExtensions: ["jpg", "png",'jpeg','webp'],
            maxImageWidth: 500,
            maxImageHeight: 800,
            size: 1000,
        });

        function actualizarAsignacion(){
            var route = "{{url('actualizarAsignacion')}}";
            var token = $("input[name=_token]").val();
            $.ajax({
                url : route,
                headers : {'X-CSRF-TOKEN':token},
                method : 'get',
                success : function (data)
                {
                    if(data)
                        toastr.success('Asignacion Actualizada');
                },
                error : function (data){
                    toastr.error('Error Asignacion');
                }
            })
        }

    </script>
</body>

</html>