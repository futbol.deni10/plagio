<head>
    <title>SISTEMA PLAGIO</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
        content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('assets\files\assets\images\favicon.ico')}}" type="image/x-icon">

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\bower_components\bootstrap\css\bootstrap.min.css')}}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\icon\feather\css\feather.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\css\style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\files\assets\css\jquery.mCustomScrollbar.css')}}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"> 
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

    <!-- File Upload  -->
    <link href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.css') }}" rel="stylesheet"/>
    <!-- <link href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet"/> -->
    <link href="{{ asset('plugins/bootstrap-fileinput/themes/explorer-fas/theme.css') }}" rel="stylesheet"/>

    <!-- select -->
    <!-- <link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" /> -->

    <!-- toastr -->
    <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">

    <link rel="stylesheet" href="{{asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('css/miestilo.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta id="app">
</head>