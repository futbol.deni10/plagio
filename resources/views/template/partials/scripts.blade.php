<!-- <script type="text/javascript" src='../js/app.js'></script> -->
<!-- <script src="{{ asset('js/app.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{asset('plugins/jquery/jquery.min.js')}}"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="{{asset('assets\files\bower_components\jquery-ui\js\jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\files\bower_components\popper.js\js\popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\files\bower_components\bootstrap\js\bootstrap.min.js')}}"></script>
<!-- jquery slimscroll js -->

<script type="text/javascript" src="{{asset('assets\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{asset('assets\files\bower_components\modernizr\js\modernizr.js')}}"></script>
<!-- Chart js -->
<!-- <script type="text/javascript" src="{{asset('assets\files\bower_components\chart.js\js\Chart.js')}}"></script> -->
<!-- amchart js -->
<!-- <script src="{{asset('assets\files\assets\pages\widget\amchart\amcharts.js')}}"></script>
<script src="{{asset('assets\files\assets\pages\widget\amchart\serial.js')}}"></script>
<script src="{{asset('assets\files\assets\pages\widget\amchart\light.js')}}"></script> -->
<script src="{{asset('assets\files\assets\js\jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- <script src="{{asset('assets\files\assets\js\SmoothScroll.js')}}"></script> -->
<script src="{{asset('assets\files\assets\js\pcoded.min.js')}}"></script>
<!-- custom js -->
<script src="{{asset('assets\files\assets\js\vartical-layout.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('assets\files\assets\pages\dashboard\custom-dashboard.js')}}"></script> -->
<script type="text/javascript" src="{{asset('assets\files\assets\js\script.min.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->

<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<!--File Upload -->
<!-- <script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/piexif.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/sortable.js') }}" ></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.js') }}"></script> -->

<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/piexif.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/plugins/sortable.min.js') }}" ></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/themes/fa/theme.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-fileinput/themes/explorer-fas/theme.js') }}"></script>
<!-- select -->
<!-- <script src="{{asset('plugins/select2/select2.min.js')}}"></script>  -->

<!-- alertas -->
<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>
<!-- Alertas -->
<script src="{{asset('plugins/sweetalert2/js/sweetalert2.all.js')}}"></script>
<!-- Alertas -->
<script src="{{ asset('plugins/sweetalert/js/sweetalert.min.js') }}"></script>

{!! Toastr::message() !!}

<script type="text/javascript">
	function personalizador(e){
		console.log(e);
	}
</script>