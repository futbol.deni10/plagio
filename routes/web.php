<?php

Route::get('/', function () {
    return view('auth.login');
});


// Route::get('even', function () {
// 	    event(new \App\Events\NuevaOrdenEvent('MOOOOOOOOOOOOOOOOOOOOOOOY'));
// 	    //return "Event has been sent!";
// 		//dd(event(new \App\Events\NuevaOrdenEvent('hello world')));
// 	});

Route::post('ocr', 'OcrController@readImage');

Route::get('ocr/test', 'OcrController@test');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
// Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::group(['middleware' => 'auth'], function(){

	Route::resource('roles', 'RoleController');
	Route::resource('permissions', 'PermissionsController');
	
	Route::group(['middleware' => ['Administrador']], function () {
		Route::resource('users', 'UsersController');
	});

	Route::resource('passwords', 'ReiniciarPasswordController');
	Route::resource('persona', 'PersonaController');

	Route::resource('archivo','ControllerArchivo');
	Route::resource('repositorio','ControllerRepositorioArchivo');
	Route::resource('comparar','ArchivoCompararController');
	
	Route::resource('categoriaproyectos','CategoriaProyectoController');
	Route::resource('proyectos','ProyectoController');
	Route::resource('asignacion','AsignacionController');
	Route::resource('gestionversiones','GestionVersionController');
	Route::resource('archivoproyecto','GestorArchivoController');
	Route::resource('usuariobloqueado','UserBloqueadoController');
	Route::resource('ocr', 'OcrController');
	Route::resource('redaccion', 'RedaccionController');
	Route::resource('plagionline', 'GoogleSearchController');
	
	

	//Roles
	Route::get('list_roles', 'RoleController@list_roles');
	Route::PUT('actualizarRoles/{criterio}', 'RoleController@actualizarRoles');
 
	//Usuario
	Route::get('/list_user', 'UsersController@list_user');
	Route::get('/bajaUsuario', 'UsersController@bajaUsuario');
	Route::get('/activarUsuario', 'UsersController@activarUsuario');
	
	Route::get('profile', 'UsersController@profile');
	Route::put('actualizardatos/{criterio}', 'UsersController@actualizardatos');

	Route::post('storePersonalizar','UsersController@storePersonalizar')->name('personalizar.store');
	Route::get('getPersonalizar','UsersController@getPersonalizar')->name('personalizar.show');
	
	Route::PUT('addRole/{criterio}/{role}','UsersController@addRole');
	//usuario bloqueado
	Route::get('/list_userbloqueado', 'UserBloqueadoController@list_userbloqueado');
	Route::get('/activarUsuarioBloqueado', 'UserBloqueadoController@activarUsuarioBloqueado');
	
	//ip bloqueo
	Route::get('/list_ipbloqueado', 'UserBloqueadoController@list_ipbloqueado');
	Route::post('/storeIp', 'UserBloqueadoController@storeIp')->name('blockip.store');
	Route::get('/activarIpBloqueado', 'UserBloqueadoController@activarIpBloqueado');
	
	//Persona
	Route::get('/listar_personas', 'PersonaController@listar_personas');
	Route::get('/get_allciudades', 'PersonaController@get_allciudades');
	Route::get('/get_usuariosdisponibles', 'PersonaController@get_usuariosdisponibles');
	
	Route::get('/get_allUsuarios', 'PersonaController@get_allUsuarios');
	Route::post('/actualizarPersonal/{id}', 'PersonaController@actualizarPersonal');


	//tipoarchivo
	Route::get('listar_tparchivos','ControllerArchivo@listar_tparchivos');
	Route::get('listar_extension','ControllerArchivo@listar_extension');
	Route::get('editExtension/{criterio}','ControllerArchivo@editExtension');
	Route::post('storeExtension','ControllerArchivo@storeExtension')->name('extension.store');
	Route::put('updateExtension/{criterio}','ControllerArchivo@updateExtension')->name('extension.update');
	Route::get('bajaExtension','ControllerArchivo@bajaExtension');
	Route::get('activarExtension','ControllerArchivo@activarExtension');
	Route::get('getArchivo','ControllerArchivo@getArchivo');

	//Repositorio
	Route::get('getExtensiones','ControllerRepositorioArchivo@getExtensiones');
	Route::get('getArchivo/{criterio}','ControllerRepositorioArchivo@getArchivo');
	Route::get('listar_repositorio','ControllerRepositorioArchivo@listar_repositorio');
	Route::post('actualizarrepositorio','ControllerRepositorioArchivo@actualizarrepositorio');

	//ArchivoComparar

	Route::get('listar_comparar','ArchivoCompararController@listar_comparar');
	Route::get('leerDocumento','ArchivoCompararController@leerDocumento');
	Route::get('convertirPDF','ArchivoCompararController@convertirPDF');
	Route::get('getArchivo2/{criterio}','ArchivoCompararController@getArchivo2');
	Route::get('getComparacion/{fileres}/{file}','ArchivoCompararController@getComparacion');

	//resultado
	Route::get('listar_resultados','ArchivoCompararController@listar_resultados');
	Route::get('resultadoIndex','ArchivoCompararController@resultadoIndex')->name('resultado.index');
	Route::get('getResultados','ArchivoCompararController@getResultados');

	//proyectos
	Route::get('listar_proyectos','ProyectoController@listar_proyectos');

	Route::get('estadoProyecto','ProyectoController@estadoProyecto');
	Route::get('getArchivoProyecto/{criterio}','ProyectoController@getArchivoProyecto');

	//Asignacion usuario
	Route::get('actualizarAsignacion','AsignacionController@actualizarAsignacion');
	Route::get('listar_asignacion','AsignacionController@listar_asignacion');

	//Gestor Archivos
	Route::get('listar_archivosp/{criterio}','ProyectoController@listar_archivosp');
	Route::get('getDocumento/{criterio}/{name}','GestorArchivoController@getDocumento');
	Route::get('listadoDirectorio/{id}/{criterio}/{ini}/{original}','GestorArchivoController@listadoDirectorio');
	
	Route::get('eliminarArchivo/{dir}/{dirhere}/','GestorArchivoController@eliminarArchivo');
	Route::get('leerDocumento2','GestorArchivoController@leerDocumento2');
	Route::get('listar_resultados2','GestorArchivoController@listar_resultados2');
	Route::get('resultadoproyecto','GestorArchivoController@resultadoproyecto')->name('resultadoproyecto.index');
	Route::get('getResultados2','GestorArchivoController@getResultados2');
	//categoria proyecto
	Route::get('listar_Categorias','CategoriaProyectoController@listar_Categorias');

	//gestionversiones
	Route::get('listar_Versiones/{criterio}','GestionVersionController@listar_Versiones');
	Route::get('getVersion/{criterio}','GestionVersionController@getVersion');
	Route::get('getCambios/{criterio}','GestionVersionController@getCambios');
	Route::delete('eliminarVersion','GestionVersionController@eliminarVersion');
	Route::get('getVersion2/{criterio}','GestionVersionController@getVersion2');


	//OCR
	Route::get('listar_ocr','OcrController@listar_ocr');
	Route::get('leerDocumentoOCR','OcrController@leerDocumentoOCR');
	Route::get('getArchivoOcr/{criterio}','OcrController@getArchivoOcr');
	Route::delete('eliminarArchivoOcr/{criterio}','OcrController@eliminarArchivoOcr');
	
	Route::get('recortarImagen','OcrController@recortarImagen');

	//Redaccion
	Route::get('listar_redacciones','RedaccionController@listar_redacciones');
	Route::get('veredaccion/{criterio}','RedaccionController@indexEditor');
	Route::get('getRedaccion/{criterio}','RedaccionController@getRedaccion');
	Route::post('guardarTexto','RedaccionController@guardarTexto');
	Route::delete('eliminarRedaccion/{criterio}','RedaccionController@eliminarRedaccion');
	Route::post('storeUsuario','RedaccionController@storeUsuario')->name('adduser.store');

	//api google
	Route::get('gerResultados/{contenido}', 'GoogleSearchController@gerResultados');
	Route::get('indexGoogleSearch/{contenido}', 'GoogleSearchController@indexGoogleSearch');
});

Route::get('clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    
    return 'DONE';
}); ;
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
