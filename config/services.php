<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '2910771262514440',
        'client_secret' => 'd2de432bbe857f99f461425146476f21',
        'redirect' => 'http://localhost/auth/facebook/callback'
    ],
        'google' => [
        'client_id' => '593371189651-6q5mgitqdkmhal41ofhm5j6tpjktslm5.apps.googleusercontent.com',
        'client_secret' => 'igQuHmJzf850MndA819JU4ii',
        'redirect' => 'http://localhost/auth/google/callback',
    ]
];
 