<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBloqueado extends Model
{
    protected $table = "bloqueos";

    protected $fillable = [
        'razon','tiempo','user_id','creadopor'
    ];
}
