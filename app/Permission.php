<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $table = "permissions";

    protected $fillable = ['role_id','add','edit','remove'];
    
    public function role()
    {
        return $this->hasOne('App\Role');
    }
}
