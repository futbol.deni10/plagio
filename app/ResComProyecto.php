<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResComProyecto extends Model
{
    protected $table = "res_comp_proyecto";


    protected $fillable = [
        'a_comparados','proyectos_id','extension'
    ];
}
