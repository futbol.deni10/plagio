<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
	protected $table = "personas";

    protected $fillable = ['nombre','apellido','ci','direccion','codigo_postal','telefono','ciudad_id','user_id'];
    
    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad');
    }
}