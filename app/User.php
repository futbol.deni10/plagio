<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;
use Cache;
use App\Notifications\ResetPasswordNotification;
use Spatie\Permission\Traits\HasRoles;
//  use Illuminate\Database\Eloquent\Factories\HasFactory;
class User extends Authenticatable
{
    use Notifiable, HasRoles;
    //use Notifiable;

    protected $table = "users";

    protected $fillable = [
        //'id', 'role_id','username','email', 'password','baja'
        'id', 'username','email', 'password','baja','google_id','facebook_id','nombre','apellido','ci','direccion','codigo_postal','telefono','ciudad_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function persona(){
        return $this->hasMany('App\Persona', 'user_id');
    }

    public function activo(){
        return Cache::has('user-is-online-' . $this->id);
    }
}
