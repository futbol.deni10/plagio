<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaProyecto extends Model
{
    
    protected $table = "categoria_proyecto";

    protected $fillable = ['nombre'];
}
