<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockIp extends Model
{
    protected $table = "blockip";

    protected $fillable = [
        'razon','tiempo','dir_ip','user_id','estado'
    ];
}
