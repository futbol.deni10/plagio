<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
	protected $table = "extension";

    protected $fillable = [
        'extension','tiposarchivo_id','user_id','estado'
    ];
}
