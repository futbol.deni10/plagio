<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProyecto extends Model
{
    protected $table = "roleproyecto";

    protected $fillable = [
        'name','add','remove', 'edit'
    ];
}


