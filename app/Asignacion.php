<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
	protected $table = "asignaciones";

    protected $fillable = [
        'tiempo','proyecto_id','user_id','creadopor','estado'
    ];
}
