<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!.$#%]).*$/',
        ];
    }
    public function messages()
    {
        return [
            'password.required' => 'El campo Usuario es obligatorio',
            'password.min' => 'La contraseña debe tener minimo 8 caracteres',
            'password.regex' => 'La contraseña es debil, debe contener almenos un caracter en Mayuscula, Minuscula y un caracter especial !.$#%',
            //'username.min' => 'El campo Usuario necesita minimo 4 caracteres    ',
        ];
    }
}
