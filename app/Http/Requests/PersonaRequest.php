<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'telefono'  => 'starts_with:+',
            'nombre'    => 'required',
            'apellido'    => 'required',
            'ci'    => 'required',
            'direccion'    => 'required',
            'ciudad_id'    => 'required',
            'user_id'    => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            //'telefono.required' => 'El campo Telefono es obligatorio',
            'telefono.starts_with' => 'El formato del numero es: <br> +(Codigo)(Numero) +59170058654',
            //'username.min' => 'El campo Usuario necesita minimo 4 caracteres    ',
        ];
    }
}
