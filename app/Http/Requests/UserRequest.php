<?php
 
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!.$#%]).*$/',
            //'role_id' => 'required',
            // 'add' => 'required',
            // 'edit' => 'required',
            // 'remove' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'El campo Usuario es obligatorio',
            'username.unique' => 'Ya Existe el Usuario',
            'email.unique' => 'Ya Existe el Email',
            'password.min' => 'La contraseña debe tener minimo 8 caracteres',
            'password.regex' => 'La contraseña es debil, debe contener almenos un caracter en Mayuscula, Minuscula y un caracter especial !.$#%',
            //'username.min' => 'El campo Usuario necesita minimo 4 caracteres    ',
        ];
    }
}
