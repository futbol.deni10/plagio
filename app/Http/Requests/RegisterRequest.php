<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'ci' => 'required',
            'pais_id' => 'required',
            'telefono' => 'required|starts_with:+',
        ];
    }

    public function messages()
    {
        return [
            'telefono.starts_with' => 'El formato del numero es: <br> +(Codigo)(Numero) +59170058654',
            'username.unique' => 'El Usuario esta en uso',
            'email.unique' => 'El Email esta en uso',
        ];
    }
}
