<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UserBloqueado;
use App\BlockIp;


use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class UserBloqueadoController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:usuariobloqueado-list|usuariobloqueado-create|usuariobloqueado-edit|usuariobloqueado-delete', ['only' => ['index','store']]);
        $this->middleware('permission:usuariobloqueado-create', ['only' => ['create','store']]);
        $this->middleware('permission:usuariobloqueado-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:usuariobloqueado-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {
        $users = User::orderBy('id','ASC')->pluck('username','id');
        $razon = [''=>'',
                'Subir contenido indebido'=>'Subir contenido indebido'
                ];
        return view('admin.users.bloqueado.index')
            ->with('users',$users)
            ->with('razon',$razon);
    }

    public function create()
    {
        //
    }

    public function list_userbloqueado()
    {
        $users = User::from('bloqueos as b')
                ->join('users as u','u.id','b.user_id')
                //->join('bloqueados as b','b.bloqueadopor','u.id')
                ->select('u.username','u.navegador','u.dir_ip as ip','u.bloqueo',
                        'b.razon','b.tiempo','b.creadopor','b.id')
                ->orderBy('b.id','DESC')->get();

        // $users->each(function ($users)
        // {
        //     $users->activo();
        //     $users->getRoleNames();
        // });
        if(request()->ajax()){
            return Datatables::of($users)
            ->rawColumns(['id','username','tiempo','creadopor','ip','navegador','estado'])
            ->editColumn('estado', function ($users) {
                if($users->bloqueo == 'SI')
                    return 'Bloqueado';
                else
                    return 'Activo';
            })
             ->make(true);
            }else{
                abort('404');
        }


    }

    public function list_ipbloqueado()
    {
        $ip = User::from('blockip as b')
                ->join('users as u','u.id','b.user_id')
                //->join('bloqueados as b','b.bloqueadopor','u.id')
                ->select('u.username','b.dir_ip as ip',
                        'b.razon','b.tiempo','b.id','b.estado')
                ->orderBy('b.id','DESC')->get();
        //dd($ip);
        // $users->each(function ($users)
        // {
        //     $users->activo();
        //     $users->getRoleNames();
        // });
        if(request()->ajax()){
            return Datatables::of($ip)
            ->rawColumns(['id','username','tiempo','creadopor','ip','navegador','estado'])
             ->make(true);
            }else{
                abort('404');
        }


    }

    public function store(Request $request)
    {
        if($request->ajax())
        {            
            $check = DB::table('bloqueos')->where('user_id',$request->user_id)->exists();
            if(!$check)
            {
                $UserBloqueado = new UserBloqueado($request->all());
                $UserBloqueado->creadopor = Auth::user()->username;
                $UserBloqueado->save();
            }else
            {
                $dato = DB::table('bloqueos')->where('user_id',$request->user_id)->get();
                $UserBloqueado = UserBloqueado::find($dato[0]->id);
                $UserBloqueado->razon = $request->razon;
                $UserBloqueado->tiempo = $request->tiempo;
                $UserBloqueado->save();
            }
            $user = User::find($UserBloqueado->user_id);
            $user->bloqueo = 'SI';
            $user->save();

            return response()->json($UserBloqueado);
        }
    }

    public function storeIp(Request $request){
        if($request->ajax())
        {
            //dd($request->all());
            $check = DB::table('blockip')->where('dir_ip',$request->ip)->exists();
            if(!$check)
            {
                $BlockIp = new BlockIp($request->all());
                $BlockIp->user_id = Auth::user()->id;
                $BlockIp->save();
            }else
            {
                $dato = DB::table('blockip')->where('dir_ip',$request->ip)->get();
                $BlockIp = BlockIp::find($dato[0]->id);
                $BlockIp->razon = $request->razon;
                $BlockIp->tiempo = $request->tiempo;
                $BlockIp->save();
            }

            return response()->json($BlockIp);
        } 
    }

    public function activarUsuarioBloqueado(Request $request){
        $UserBloqueado = UserBloqueado::find($request->criterio);
        $user = User::find($UserBloqueado->user_id);
        $user->bloqueo = 'NO';
        $user->save();

        return $user;
    }

    public function activarIpBloqueado(Request $request){
        $BlockIp = BlockIp::find($request->criterio);
        $BlockIp->estado = 'Activo';
        $BlockIp->save();

        return $BlockIp;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
