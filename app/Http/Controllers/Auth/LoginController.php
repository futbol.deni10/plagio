<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\User;
use App\BlockIp;
use Auth;
use App\UserBloqueado;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    use AuthenticatesUsers;
    
    protected function credentials(Request $request)
    {
        $login = $request->input($this->username());
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        //Toastr::error('DATOS INCORRECTOS');
        return [
         $field => $login,
         'password' => $request->input('password')
         ]; 
    }

    public function login(Request $request)
    {

        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            $myIp = request()->ip();
            $user = $this->checkBloqueo($user,$myIp);
            $check = DB::table('blockip')->where('dir_ip',$myIp)->where('estado','Bloqueado')->exists();
            // Make sure the user is active
            if ($user->baja == 'NO' && $this->attemptLogin($request) && $user->bloqueo == 'NO' && !$check) {
                
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                $navegador = $this->getBrowser($user_agent);
                $myIp = request()->ip();
                if($myIp == '::1')
                    $myIp = 'localhost';
                $user->navegador = $navegador;
                $user->dir_ip = $myIp;
                $user->save();

                return $this->sendLoginResponse($request);

            } else {
                $nopersona = '';
                if($user->baja == 'SI'){ 
                    $nopersona .= "El usuario esta inactivo, contáctese con el administrador";
                }
                if($user->bloqueo == 'SI'){ 
                    $dato = DB::table('bloqueos')->where('user_id',$user->id)->get();
                    $nopersona .= "El usuario esta bloqueado por: ".$dato[0]->razon.", <br> hasta el ".$dato[0]->tiempo." <br> contáctese con el administrador";
                }
                if($check){
                    $dato = DB::table('blockip')->where('dir_ip',$myIp)->get();
                    $nopersona .= "La ip ".$dato[0]->dir_ip." esta bloqueado por: ".$dato[0]->razon.",  hasta el ".$dato[0]->tiempo."  contáctese con el administrador";
                }
                $this->incrementLoginAttempts($request);
                Auth::logout();
                return redirect()
                    ->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->with(compact('nopersona')); 
            }
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    // protected $redirectTo = RouteServiceProvider::HOME;

    public function username()
    {
        return 'login';
    }
    
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getBrowser($user_agent){
        if(strpos($user_agent, 'MSIE') !== FALSE)
            return 'Internet explorer';
        elseif(strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
            return 'Microsoft Edge';
        elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
            return 'Internet explorer';
        elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
            return "Opera Mini";
        elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
            return "Opera";
        elseif(strpos($user_agent, 'Firefox') !== FALSE)
            return 'Mozilla Firefox';
        elseif(strpos($user_agent, 'Chrome') !== FALSE)
            return 'Google Chrome';
        elseif(strpos($user_agent, 'Safari') !== FALSE)
            return "Safari";
        else
            return 'No hemos podido detectar su navegador';
    }

    public function checkBloqueo($User,$ip){
        $dato = DB::table('bloqueos')->where('user_id',$User->id)->get();
        
        $dato2 = DB::table('blockip')->where('dir_ip',$ip)->get();
        $hoy = Carbon::today()->locale('es');
        if(sizeof($dato)>0){
            if($dato[0]->tiempo != null)
            {

                $fecha_final = Carbon::create($dato[0]->tiempo);
                //dd($fecha_final);
                if($hoy > $fecha_final)
                {
                    $User = User::find($dato[0]->user_id);
                    //dd($User);
                    $User->bloqueo = 'NO';
                    $User->save();
                }
            }
        }

        if(sizeof($dato2)>0){
            if($dato2[0]->tiempo != null)
            {

                $fecha_final = Carbon::create($dato2[0]->tiempo);
                //dd($fecha_final);
                if($hoy > $fecha_final)
                {
                    $BlockIp = BlockIp::find($dato2[0]->id);
                    //dd($BlockIp);
                    $BlockIp->estado = 'Activo';
                    $BlockIp->save();
                }
            }
        }
        return $User;
    }
}
