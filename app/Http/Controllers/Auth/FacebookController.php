<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Socialite;
use Auth;
use Exception;
use App\User;
class FacebookController extends Controller
{
	public function redirectToFacebook()
    {

        return Socialite::driver('facebook')->redirect();

    }

    public function handleFacebookCallback()
    { 
        try {
            $user = Socialite::driver('facebook')->user();
            
            $finduser = User::where('facebook_id', $user->id)->first();
            if($finduser){
                Auth::login($finduser);
                return redirect('/home');
            }else{
                $newUser = User::create([
                    'username' => $user->name,
                    'name' => $user->name,
                    'email' => $user->email,
                    'facebook_id'=> $user->id,
                    'password' => encrypt('123456')
                ]);

                Auth::login($newUser);
                return redirect('/home');

            }

        } catch (Exception $e) {
            dd($e->getMessage());

        }

    }
}
