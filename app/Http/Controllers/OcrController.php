<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Alimranahmed\LaraOCR\Services\OcrAbstract;
use OCR;
use App\RepositorioArchivo;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use App\Jobs\ProcessCompararOcr;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class OcrController extends Controller
{
    protected $ocr;

    function __construct()
    {
        $this->middleware('permission:CompararOcr-list|CompararOcr-create|CompararOcr-edit|CompararOcr-delete', ['only' => ['index','store']]);
        $this->middleware('permission:CompararOcr-create', ['only' => ['create','store']]);
        $this->middleware('permission:CompararOcr-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:CompararOcr-delete', ['only' => ['destroy']]);
    }

    public function index(){
        return view('admin.ocr.index');
    }

    public function listar_ocr()
    {
        $check = Auth::user()->hasRole('Administrador');
        if($check)
        {
            $datos = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ra.user_id')
                ->select('u.username as nombreusuario',
                        'ta.nombre as tipoarchivo',
                        'e.extension',
                        'ra.id','ra.url as nombrearchivo','ra.recorte')
                ->where('ra.estado','comparar')
                ->where('ra.ocr','SI')
                ->get();
        }else
        {
            $id = Auth::user()->id;
            $datos = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ra.user_id')
                ->select('u.username as nombreusuario',
                        'ta.nombre as tipoarchivo',
                        'e.extension',
                        'ra.id','ra.url as nombrearchivo','ra.recorte')
                ->where('ra.user_id',$id)
                ->where('ra.estado','comparar')
                ->where('ra.ocr','SI')
                ->get();
        }
                
        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario']) 

             ->make(true);
        }else{
            abort('404');
        }
    }

    public function store(Request $request){
        if($request->archivo != "undefined")
        {
            $file = $request->archivo;
            $nameFile = $file->getClientOriginalName();
            $nameFile = str_replace(' ', '', $nameFile);
            $extension = $file->getClientOriginalExtension();
            $path = public_path() . '/archivocomparar/OCR/';
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $file->move($path,$nameFile);
            
            $exte = DB::table('extension')->where('extension',strtoupper($extension))->get();

            $RepositorioArchivo = new RepositorioArchivo();
            $RepositorioArchivo->url = $nameFile;
            $RepositorioArchivo->md5_file = md5_file($path . '/'. $nameFile);
            $RepositorioArchivo->estado = 'comparar';
            $RepositorioArchivo->ocr = 'SI';
            $RepositorioArchivo->extension_id = $exte[0]->id;
            $RepositorioArchivo->user_id = Auth::user()->id;
            $RepositorioArchivo->save();
            return response()->json('ok');
        }
    }

    //ALMACENA LAS COLAS (JOB)OCR
    public function leerDocumentoOCR(Request $request)
    { 
        
        for($i=0; $i<sizeof($request->datosComparar); $i++){
            $cadVec = explode("-", $request->datosComparar[$i]);
           //dd($cadVec);
            $datos = ['id'=> $cadVec[0], 'extension'=>$cadVec[1]];
            ProcessCompararOcr::dispatch($datos);
        }
        //dd($datos);
            //return response()->json('Jobs');        
        //}

        return response()->json('ok');

    }

    public function getArchivoOcr($id)
    {
        $datos = RepositorioArchivo::find($id);

        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->where('e.id',$datos->extension_id)
                ->get();

        $path = public_path() . '/archivocomparar/OCR/'. $datos->url;
        $headers = array(
                  'Content-Type: application/pdf',
                );
        
        $nameFile = $datos->url;

        return response()->download($path, $nameFile, [], 'inline');

    }

    public function eliminarArchivoOcr($criterio)
    {
        try {
            $datos = RepositorioArchivo::find($criterio);
            $datos->delete();
            $path = public_path() . '/archivocomparar/OCR/'. $datos->url;
            if (file_exists($path))
                unlink($path);
            return response()->json(['mensaje'=>'']);
        } catch(\Exception $exception){
            return response()->json(['mensaje'=>'Registro en Uso no puede ser eliminado']);
        }
    }

    public function recortarImagen(Request $request){
       // dd($request->all());
        $datos = RepositorioArchivo::find($request->idRec);
        $datos->recorte = 'SI';
        $datos->save();
        $url = public_path().'/archivocomparar/OCR/'.$request->ubicacion;
        list($ancho, $alto) = getimagesize($url);

        $x =  (int)$request->size['x'] ;
        $y =  (int)$request->size['y'] ;
        $h =  (int)$request->size['h'] ;
        $w =  (int)$request->size['w'] ;

        if(strtolower($request->extension) == 'jpeg' || strtolower($request->extension) == 'jpg')
            $img_r = imagecreatefromjpeg($url);
        if(strtolower($request->extension) == 'png')
            $img_r = imagecreatefrompng($url);
        $dst_r = ImageCreateTrueColor($w, $h);
 
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);
        // header('Content-type: image/jpeg');
        // imagejpeg($dst_r);
         //imagecopyresampled($dst_r, $img_r, 0, 0, $_GET['x'], $_GET['y'], $_GET['w'], $_GET['h'], $_GET['w'],$_GET['h']);
        $path = public_path() . '/archivocomparar/OCR/RECORTE/';
        if (!file_exists($path)) {
             mkdir($path, 0777, true);
        }
        imagejpeg($dst_r,$path.'Recorte-'.$request->ubicacion);

        return response()->json(['url'=>'Recorte-'.$request->ubicacion,'ancho'=>$ancho]);
        //return response(imagejpeg($dst_r), 200)->header('Content-type', 'image/jpeg');
        // //$file->move($path,'Recorte-'.$request->ubicacion);
        // $img = new imaging;
        // $img->set_img($file);
        // $img->move($path,'Recorte-'.$request->ubicacion);
    }
}
