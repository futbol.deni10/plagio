<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AlesZatloukal\GoogleSearchApi\GoogleSearchApi;
use DataTables;
use Smalot\PdfParser\Parser;
class GoogleSearchController extends Controller
{

    public function indexGoogleSearch($criterio)
    {
        //dd(base64_decode($criterio));
        $criterio = base64_decode($criterio);
        $cadVec = explode("-", $criterio);
        $vecData = [];
        $cad = '';
        if($criterio != '')
        {            
            $partes_ruta = pathinfo(public_path().'/proyectos2/'.$cadVec[1]);
            if(strtoupper($partes_ruta['extension']) != 'PDF')
            {
                $pos = strpos( $cadVec[0],'.');
                $urlDoc = substr($cadVec[0],0,$pos) . '.pdf';

                $path = public_path().'/proyectos2/'.$cadVec[3].'-convertidos'.'/'.$cadVec[2].'/'.$urlDoc;
                //dd($path);
            }else{
                $path = public_path().'/proyectos2/'.$cadVec[1];
            }
            //dd($partes_ruta);
            $PDFParser = new Parser();
            $pdf = $PDFParser->parseFile($path);
            $pages  = $pdf->getPages();

            for($p1=0; $p1<sizeof($pages); $p1++){
                $aux = $pages[$p1]->getTextArray();
                for($a=0; $a<sizeof($aux); $a++)
                {
                    $vecData [] = $aux[$a];
                }
            } 
            $cant = sizeof($vecData);

            for($v=10; $v<($cant/2)+(($cant/2)/2/2); $v++)
            {
                $cad .= $vecData[$v].' ';
            }
            $criterio = $cad;
        }
        return view('admin.googlesearch.index')->with('criterio',$criterio);
    } 

    public function test($criterio)
    {
        return view('admin.googlesearch.index')->with('criterio',$criterio);
    }

    public function gerResultados(Request $request)
    {
        //https://www.googleapis.com/customsearch/v1?key=AIzaSyAX1Eubq7LIbAAZ4SfUhmiYclxaRnBE1BU&cx=3864ba8520f9aa8ac&q=Guitarra
        $googleSearch = new GoogleSearchApi();
        $parametros = array(
            'start' => 1, // start from the 10th results,
            'num' => 5 // number of results to get, 10 is maximum and also default value
        );
        $results = $googleSearch->getResults($request->contenido);
        //$results = $googleSearch->getResults($request->contenido,$parametros);
        //dd($results);
        //$rawResults = $googleSearch->getRawResults();
        //return response()->json(['resultados'=>$results]);
        if(request()->ajax()){
            return Datatables::of($results)
            ->rawColumns( ['id','link','titulo','htmlSnippet'])
            ->make(true);
        }else{
            abort('404');
        }
    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
