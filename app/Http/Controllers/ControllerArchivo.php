<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use App\Extension;

use Illuminate\Support\Facades\DB;
use DataTables;

use Illuminate\Support\Facades\Auth;
class ControllerArchivo extends Controller
{

    public function index()
    {
        $tparchivo = Archivo::orderBy('nombre','ASC')->pluck('nombre','id');
        $tparchivo [''] = ' ';
        return view('admin.gestorarchivos.tipoarchivo-index')
                ->with('tparchivo',$tparchivo);
    }

    public function listar_tparchivos()
    {
        $datos = DB::table('tiposarchivo as ta')
                ->join('users as u','u.id','ta.user_id')
                ->select('u.username as nombreusuario','ta.id','ta.nombre')
                ->get();

        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','nombre','nombreusuario'])
             ->make(true);
        }else{
            abort('404');
        }
    }
    
    public function listar_extension()
    {
        $datos = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ta.user_id')
                ->select('u.username as nombreusuario','ta.nombre as tipoarchivo',
                        'e.extension','e.id','e.estado')
                ->get();

        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario'])
             ->make(true);
        }else{
            abort('404');
        }
    }

    public function bajaExtension(Request $request)
    {
        $datos = Extension::find($request->id);
        $datos->estado = "inactivo";
        $datos->save();

        return response()->json($datos);
    }

    public function activarExtension(Request $request)
    {
        $datos = Extension::find($request->id);
        $datos->estado = "activo";
        $datos->save();

        return response()->json($datos);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        if($request->ajax())
        {
            $dato = new Archivo($request->all());
            $dato->user_id = Auth::user()->id;
            $dato->save();

            return response()->json($dato);
        }    
    }

    public function storeExtension(Request $request)
    {
        if($request->ajax())
        {
            $dato = new Extension($request->all());
            $dato->estado = 'activo';
            $dato->user_id = Auth::user()->id;
            $dato->save();
            return response()->json($dato);
        }    
    }

    public function show($id)
    {
        //
    }
 
    public function edit($id)
    {
        $dato = Archivo::find($id);
        return $dato;
    }

    public function editExtension($id)
    {
        $dato = Extension::find($id);
        return $dato;
    }

    public function getArchivo(){
        return Archivo::orderBy('nombre','ASC')->get();
    }

    public function update(Request $request, $id)
    {
        
        $dato = Archivo::find($id);
        $dato->nombre = $request->nombre;
        $dato->user_id = Auth::user()->id;
        $dato->save();

        return response()->json($dato);
    }

    public function updateExtension(Request $request, $id)
    {
        
        $dato = Extension::find($id);
        $dato->extension = $request->extension;
        $dato->tiposarchivo_id = $request->tiposarchivo_id;
        $dato->user_id = Auth::user()->id;
        $dato->save();

        return response()->json($dato);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
