<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Asignacion;
//use App\GestionVersiones;
use App\Proyecto;
use App\ArchivosProyecto;
 
use ZipArchive;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Jobs\ProcessVersion;

use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
class GestionVersionController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        //$role = Role::where('user_id',$user->id)->get();
        //dd($role);
        if($user->hasRole('Administrador'))
        {
            $datos = Asignacion::from('asignaciones as a')
                    ->join('proyectos as p','p.id','a.proyecto_id')
                    //->select('p.nombre','p.id')->get();
                    ->pluck('p.nombre','p.id');
        }else {
            
            $datos = Asignacion::from('asignaciones as a')
                    ->join('proyectos as p','p.id','a.proyecto_id')                    
                    ->where('a.user_id',$user->id)
                    ->pluck('p.nombre','p.id');
        }
        //dd($datos);
        // $datos = Asignacion::from('asignaciones as a')
        //         ->join('proyectos as p','p.id','a.proyecto_id')
        //         ->where('p.baja','NO')
        //         ->pluck('p.nombre','p.id');

        return view('admin.gestionversiones.index')->with('datos',$datos);
    }

    public function listar_Versiones($criterio)
    {
        $dato =  DB::table('archivosproyecto as a')
        ->join('users as u','u.id','=','a.user_id')
        ->select('a.*',
                    'u.username')
        ->where('a.proyecto_id',$criterio)
        ->get();
        //dd($dato);
        if(request()->ajax()){
        return Datatables::of($dato)
            ->rawColumns( ['id','version','username','created_at','notas'])
            ->editColumn('created_at', function ($dato) {
                $dt = Carbon::create($dato->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
            ->editColumn('version', function ($dato) {
                return 'Version '.$dato->version;
            })
            ->make(true);
        }else{
            abort('404');
        }
    }

    public function getVersion($criterio){
        $dato = GestionVersiones::find($criterio);
        return $dato;
    }

    public function create()
    {
        //
    }
 
    //protected $contenido;
    public function store(Request $request)
    {     
        $archivo = $request->archivo;
        $destinationPath = public_path();
        //$nombre = 'archivo.'.$archivo->getClientOriginalExtension();
        $nombre = $archivo->getClientOriginalName();
        //dd($archivo->getClientOriginalExtension());
        $archivo->move($destinationPath, $nombre);
        if($request->verificar == 'true')
        {
            $proyecto = Proyecto::find($request->proyecto_id);

            $mpdf = new \Mpdf\Mpdf();
            $porciones = explode(",", $request->criterio);
            foreach ($porciones as $key => $c) {            
                $mpdf->WriteHTML($c);
            }
            //$nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
            $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/version'.$request->vers.'/';
            if (!file_exists($route)) {
                mkdir($route, 0777, true);
            } 
            $pos = strpos( $nombre,'.');
            $urlDoc = substr($nombre,0,$pos) . '.pdf';
            
            $route = public_path() .'/proyectos2/'.$proyecto->nombre.'-convertidos/version'.$request->vers.'/'.$urlDoc;
            $mpdf->Output($route,'F');
        } 
        ProcessVersion::dispatch($request->all(), Auth::user()->id,$nombre,$archivo->getClientOriginalExtension(),$request->vers);
        return response()->json('ok');
    }

    // public function process_compare($dir1, $dir2, $only_check_has){
    //     $this->compare_file_folder($dir1,  $dir1, $dir2, $only_check_has);
    // }
 
    // public function compare_file_folder($dir1,  $base_dir1, $base_dir2, $only_check_has=0){
    //     if (is_dir($dir1)) {
    //         $handle = dir($dir1);
    //         if ($dh = opendir($dir1)) {
    //             while ($entry = $handle->read()) {
    //                 if (($entry != ".") && ($entry != "..")  && ($entry != ".svn")){
    //                     $new = $dir1."/".$entry;
    //                     //echo 'compare:  ' . $new . "\n";
    //                     $other = preg_replace('#^'. $base_dir1 .'#' ,  $base_dir2, $new);
    //                     //dd($other,$new);
    //                     if(is_dir($new)) {
    //                                                 // Comparar
    //                         if (!is_dir($other)) {
    //                             //echo '!!not found direction:  '. $other. '  (' . $new .")<br>";
    //                             $del = public_path() .'/proyectos2/';
    //                             $del = str_replace("\\","/",$del,$i);

    //                             $this->contenido [] = ['tipo' => 'directorio', 
    //                                             'mensaje' => 'No se encuentra el directorio',
    //                                             'comparado' => str_replace($del,'',$other)];
    //                             //dd("oek");
    //                         }
    //                         $this->compare_file_folder($new, $base_dir1,$base_dir2,  $only_check_has) ;
    //                         //dd("oke");
    //                                         } else {// Si 1 es un archivo, entonces 2 también debería ser un archivo
    //                         if (!is_file($other)) {
    //                             //echo '!!not found file:       '. $other. '  ('.$new   .")<br>";
    //                             $del = public_path() .'/proyectos2/';
    //                             $del = str_replace("\\","/",$del,$i);
    //                             $this->contenido [] = ['tipo' => 'archivo', 
    //                                             'mensaje' => 'No se encuentra el archivo',
    //                                             'comparado' => str_replace($del,'',$other)];
    //                         }elseif ($only_check_has ==0 && ( md5_file($other) != md5_file($new) )  ){
    //                             //echo '!!file md5 error:       '. $other. '  ('.$new   .")\n";
    //                         }
    //                     }
    //                 }
    //             }
    //             closedir($dh);
    //         }

    //     }
    //     //return serialize($contenido);
    // }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        return ArchivosProyecto::find($id);
    }

    public function getCambios($criterio)
    {
        $ArchivosProyecto = ArchivosProyecto::find($criterio);
        //dd(!$ArchivosProyecto->resultadocomprar);
        if(!$ArchivosProyecto->resultadocomprar)
        {
            $datos = [];
            return response()->json(['dato'=>$datos]);
        }
        else
            $datos = unserialize($ArchivosProyecto->resultadocomprar);
        //dd($datos);
        //$a = Arr::sort($datos);
        //dd($a);
        $sorted = array_values(Arr::sort($datos, function ($value) {
            return $value['tipo'];
        }));
        return response()->json(['dato' => $sorted,'notas'=>$ArchivosProyecto->notas]);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function eliminarVersion(Request $request)
    {
        $GestionVersiones = GestionVersiones::find($request->criterio);
        $GestionVersiones->delete();
        $mensaje = "La version fue eliminada!";

        return response()->json($mensaje);
    }

    public function getVersion2($criterio){
        $dato = ArchivosProyecto::where('archivo',$criterio)->count();
        return $dato+1;
    }
}
