<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Extension;
use App\RepositorioArchivo;

use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;
use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
class ControllerRepositorioArchivo extends Controller
{

    public function index()
    {
        $extension = Extension::orderBy('extension','ASC')
                        ->where('estado','activo')
                        ->pluck('extension','id');
        $extension [''] = ' ';
        return view('admin.repositorioarchivo.index')
                ->with('extension',$extension);
    }

    public function getExtensiones()
    {
        $extension = DB::table('extension')->where('estado','activo')->get();
        return $extension;
    }

    public function getArchivo($id)
    {
        $datos = RepositorioArchivo::find($id);


        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->where('e.id',$datos->extension_id)
                ->get();

        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/' . $tipo[0]->extension .'/'. $datos->url;
        $headers = array(
                  'Content-Type: application/pdf',
                );
        
        $nameFile = $datos->url;

        return response()->download($path, $nameFile, [], 'inline');

    } 

    public function listar_repositorio()
    {
        
        $datos = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ta.user_id')
                ->select('u.username as nombreusuario',
                        'ta.nombre as tipoarchivo',
                        'e.extension',
                        'ra.id','ra.url as nombrearchivo')
                ->where('ra.estado','archivo')
                ->get();

        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario'])
             ->make(true);
        }else{
            abort('404');
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $extension = strtoupper($request->archivo->getClientOriginalExtension());

        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->select('e.id','e.extension','ta.nombre')
                ->where('e.extension',$extension)
                ->get();

        $cantidad = DB::table('repositorio_archivo')
                    ->where('estado','archivo')
                    ->where('extension_id',$tipo[0]->id)->count();

        if($extension == 'XLSX')
        {
            $cadena = '';
            $spreadsheet = IOFactory::load($request->archivo);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            //dd($sheetData);
            $indice = 65;
            $mayor = 0; 
            for($i=1; $i<=count($sheetData)-1; $i++)
            {
                if($mayor < count($sheetData[$i]))
                    $mayor = count($sheetData[$i]);
            }
            //echo $mayor;
            for($i=1; $i<=count($sheetData); $i++)
            {
                for($j=0; $j<$mayor; $j++)
                {
                    if($sheetData[$i][chr($indice)] != null)
                    {
                        $cadena .= '<p>'. $sheetData[$i][chr($indice)] .'</p><br> ,' ;
                        $indice++;
                    }
                }
                $indice = 65;
            }

            $mpdf = new \Mpdf\Mpdf();
            $porciones = explode(",", $cadena);
            foreach ($porciones as $key => $c) {            
                $mpdf->WriteHTML($c);
            }
            $nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
            $route = public_path() .'/archivos/'.$tipo[0]->nombre.'/convertidos/'. $extension;
            if (!file_exists($route)) {
                mkdir($route, 0777, true);
            }
            $route = public_path() .'/archivos/'.$tipo[0]->nombre.'/convertidos/'. $extension.'/'.$nameFile2;
            $mpdf->Output($route,'F');
        }

        $file = $request->archivo;
        $nameFile = 'Archivo'.($cantidad+1).'.'.$file->getClientOriginalExtension();
        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'. $extension;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $file->move($path,$nameFile);
        
        $md5_file = md5_file($path . '/'. $nameFile);

        if($request->verificar == 'true')
        {
            $mpdf = new \Mpdf\Mpdf();
            $porciones = explode(",", $request->criterio);
            foreach ($porciones as $key => $c) {            
                $mpdf->WriteHTML($c);
            }
            $nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
            $route = public_path() .'/archivos/'.$tipo[0]->nombre.'/convertidos/'. $tipo[0]->extension;
            if (!file_exists($route)) {
                mkdir($route, 0777, true);
            }
            $route = public_path() .'/archivos/'.$tipo[0]->nombre.'/convertidos/'. $tipo[0]->extension.'/'.$nameFile2;
            $mpdf->Output($route,'F');
        }

        $dato = new RepositorioArchivo();
        $dato->url = $nameFile;
        $dato->md5_file = $md5_file;
        $dato->estado = $request->estado;
        $dato->extension_id = $tipo[0]->id;
        $dato->user_id = Auth::user()->id;
        $dato->save();

        return response()->json($dato);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dato = RepositorioArchivo::find($id);
        return $dato;
    }

    public function actualizarrepositorio(Request $request)
    {
        $datos = RepositorioArchivo::find($request->id);
        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->where('e.id',$datos->extension_id)
                ->get();

        if($request->archivo != "undefined")
        {
            $this->eliminarArchivo($request->id);
            $cantidad = DB::table('repositorio_archivo')->where('extension_id',$request->extension_id)->count();
            $file = $request->archivo;
            $nameFile = 'Archivo'.($cantidad+1).'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/archivos/'. $tipo[0]->nombre;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $file->move($path,$nameFile);
            $datos->url = $nameFile;
            $md5_file = md5_file($file);
            $datos->md5_file = $md5_file;
        }
        $datos->save();

        return response()->json($datos);
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $dato = RepositorioArchivo::findOrFail($id);
        $this->eliminarArchivo($id);
        $dato->delete();

        return response()->json($dato);
    }

    public function eliminarArchivo($criterio)
    {
        $datos = RepositorioArchivo::find($criterio);
        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->select('e.id','e.extension','ta.nombre')
                ->where('e.id',$datos->extension_id)
                ->get();

        if($datos->extension != 'PDF')
        {            
            $pos = strpos( $datos->url,'.');
            $urlDoc = substr($datos->url,0,$pos) . '.pdf';
        }else
            $urlDoc = $datos->url;

        $route = public_path() .'/archivos/'.$tipo[0]->nombre.'/convertidos/'. $tipo[0]->extension.'/'.$urlDoc;
        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'. $tipo[0]->extension .'/'. $datos->url;

        if (file_exists($path))
            unlink($path);

        if (file_exists($route))
            unlink($route);
    }
}
