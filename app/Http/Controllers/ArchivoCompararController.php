<?php
namespace Alimranahmed\LaraOCR\Controllers;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\RepositorioArchivo;
use App\Extension;

use DataTables;
use Illuminate\Support\Facades\Auth;

use Smalot\PdfParser\Parser;

use Illuminate\Support\Facades\DB;
use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Imagick;
use App\Jobs\ProcessComparar;

use Carbon\Carbon;
use App\ResultadoComparar;

use Response;
use App\Jobs\SendEmailJob;
use App\Mail\SendEmailTest;
use Mail;

//use Alimranahmed\LaraOCR\Services\OcrAbstract;
use OCR;
use Smalot\PdfParser\XObject\Image;
class ArchivoCompararController extends Controller
{

    public function index(){ 
        // // //$ocr = app()->make(OcrAbstract::class);
        // // $parseador = new Parser();
        // // $imagePath = public_path().'/1.pdf';
        // // $documento = $parseador->parseFile($imagePath);
        // // $images = array_filter($documento->getObjectsByType('XObject'), fn($o) => $o InstanceOf Image );
        // // foreach ( $images as $i => $image ) {
        // //     //dd($image->getContent());
        // //     file_put_contents ($i.'.jpg',$image->getContent());
        // //      Storage::disk('images_OCR')->put('IM.jpg',$image->getContent());
        // //      $text =  OCR::scan(public_path().'/IM.jpg');
        // //         dd($text);
        // // }
        // // // foreach ($imagenes as $imagen) {
        // // //     //dd(base64_encode($imagen->getContent()));
        // // //     //file_put_contents( 'image_'.$i, $image->getContent());
        // // //     $img = $this->getB64Image(base64_encode($imagen->getContent()));
        // // //     //dd($img);
        // // //     Storage::disk('images_OCR')->put('IM.jpg',$imagen->getContent());
        // // //     //dd($imagen->getContent());
        //     $text =  OCR::scan(public_path().'/11.png');
        //     dd($text);
        // // // }
        // // // //$imagePath = str_replace("/","\\",$imagePath,$i);
        // // // //$imagePath = str_replace("\\","/",$imagePath,$i);
        // // // //dd($imagePath);
        // // $text =  OCR::scan($imagePath);
        // // // //$text = $ocr->scan($imagePath);
        
        $extension = Extension::orderBy('extension','ASC')
                        ->where('estado','activo')
                        ->pluck('extension','id');
        $extension [''] = ' ';
        
        $datos = ['email'=>Auth::user()->email,'mensaje'=>'La comparacion de la imagen fue finalizada','url'=>'resultadoIndex'];
        //SendEmailJob::dispatch($datos);
        //SendEmailJob::dispatch($datos);
        
        return view('admin.comparararchivo.index')
                ->with('extension',$extension);
    }

    public function resultadoIndex()
    {
        $porcentaje = [''=>'',
                     '0'=>'0%',
                    '5'=>'5%',
                    '10'=>'10%',
                    '20'=>'20%',
                    '30'=>'30%',
                    '40'=>'40%',
                    '50'=>'50%',
                    '60'=>'60%',
                    '70'=>'70%',
                    '80'=>'80%',
                    '90'=>'90%',
                    '100'=>'100%'];
        $cantidad = [''=>'',
                    '5'=>5,
                    '10'=>10,
                    '20'=>20];
        return view('admin.comparararchivo.ver_index')
                ->with('cantidad',$cantidad)
                ->with('porcentaje',$porcentaje);
    }

    public function listar_comparar()
    {
        $check = Auth::user()->hasRole('Administrador');
        if($check)
        {
            $datos = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ra.user_id')
                ->select('u.username as nombreusuario',
                        'ta.nombre as tipoarchivo',
                        'e.extension',
                        'ra.id','ra.url as nombrearchivo')
                ->where('ra.estado','comparar')
                ->where('ra.ocr','NO')
                ->get();
        }else
        {
            $id = Auth::user()->id;
            $datos = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->join('users as u','u.id','ra.user_id')
                ->select('u.username as nombreusuario',
                        'ta.nombre as tipoarchivo',
                        'e.extension',
                        'ra.id','ra.url as nombrearchivo')
                ->where('ra.user_id',$id)
                ->where('ra.estado','comparar')
                ->where('ra.ocr','NO')
                ->get();
        }
                
        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario']) 

             ->make(true);
        }else{
            abort('404');
        }
    }

    public function listar_resultados()
    {
                $datos = DB::table('resultados_comparar as rc')
                ->join('repositorio_archivo as ra','ra.id','rc.archivosproyecto_id')
                //->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                // ->join('users as u','u.id','ta.user_id')
                ->select(
                        //'ta.nombre as tipoarchivo',
                        // 'e.extension',
                        'rc.created_at',
                        'rc.id','ra.url as nombrearchivo')
                ->where('ra.user_id',Auth::user()->id)
                ->get();

        if(request()->ajax()){
            return Datatables::of($datos)
             ->rawColumns( ['id','tipoarchivo','extension','nombreusuario'])
                         ->editColumn('created_at', function ($datos) {
                $dt = Carbon::create($datos->created_at)->locale('es');
                $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                return $fecha;
            })
             ->make(true);
        }else{
            abort('404');
        }
    }

    //ALMACENA LAS COLAS (JOB)
    public function leerDocumento(Request $request)
    { 
        
        for($i=0; $i<sizeof($request->datosComparar); $i++){
            $cadVec = explode("-", $request->datosComparar[$i]);
           //dd($cadVec);
            $datos = ['id'=> $cadVec[0], 'extension'=>$cadVec[1]];
            ProcessComparar::dispatch($datos);
        }
        //dd($datos);
            //return response()->json('Jobs');        
        //}

        return response()->json('ok');

    }

    public function getB64Image($base64_image){        
         $image_service_str = substr($base64_image, strpos($base64_image, ",")+1);       
         $image = base64_decode($image_service_str);   
         return $image; 
    }

    public function getB64Extension($base64_image, $full=null){  
        preg_match("/^data:image\/(.*);base64/i",$base64_image, $img_extension);   
        return ($full) ?  $img_extension[0] : $img_extension[1];  
    }

    public function getResultados(Request $request)
    {
            $datos = ResultadoComparar::find($request->id);
            //dd($datos);
            $tipo = DB::table('resultados_comparar as rc')
                    ->join('repositorio_archivo as ra','ra.id','rc.archivosproyecto_id')
                    ->join('extension as e','e.id','ra.extension_id')
                    ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                    ->select('ta.nombre','rc.id','ra.ocr')
                    ->where('rc.id',$datos->id)->get();
            if($tipo[0]->ocr == 'SI'){
                $id = $tipo[0]->id;
                $tipo = [];
                $tipo [] = ['nombre'=>'DOCUMENTOS','id'=>$id];
            }
            
            return response()->json(['unoPor' => unserialize($datos->a_comparados),
                                    'tipo'=>$tipo[0],
                                    'cantidad'=>$request->cantidad,
                                    'porcentaje'=>$request->porcentaje]);
        
    }



    public function create()
    {
        //
    }

    public function convertirPDF(Request $request)
    {
        $mpdf = new \Mpdf\Mpdf();
        //dd($request->criterio);
        foreach ($request->criterio as $key => $c) {            
            $mpdf->WriteHTML($c);
        }

        $route = public_path().'/DOCUMENTOS/convertidos/';

        $mpdf->Output('filename.pdf','F');

        return response()->json("ok");
    }



    public function store(Request $request)
    { 
        $extension = strtoupper($request->archivo->getClientOriginalExtension());
        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->select('ta.nombre','e.id','e.extension')
                ->where('e.extension',$extension)
                ->get();

        $cantidad = DB::table('repositorio_archivo')
                ->where('estado','comparar')
                ->where('extension_id',$tipo[0]->id)->count();


        if($extension == 'XLSX' || $extension == 'XLS' )
        {
            $cadena = '';
            $spreadsheet = IOFactory::load($request->archivo);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            //dd($sheetData);
            $indice = 65;
            $mayor = 0;
            for($i=1; $i<=count($sheetData)-1; $i++)
            {
                if($mayor < count($sheetData[$i]))
                    $mayor = count($sheetData[$i]);
            }
            //echo $mayor;
            for($i=1; $i<=count($sheetData); $i++)
            {
                for($j=0; $j<$mayor; $j++)
                {
                    if($sheetData[$i][chr($indice)] != null)
                    {
                        $cadena .= '<p>'. $sheetData[$i][chr($indice)] .'</p><br> ,' ;
                        $indice++;
                    }
                }
                $indice = 65;
            }

            $mpdf = new \Mpdf\Mpdf();
            $porciones = explode(",", $cadena);
            foreach ($porciones as $key => $c) {            
                $mpdf->WriteHTML($c);
            }
            $nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
            $route = public_path() .'/archivocomparar/'.$tipo[0]->nombre.'/convertidos/'. $extension;
            if (!file_exists($route)) {
                mkdir($route, 0777, true);
            }
            $route = public_path() .'/archivocomparar/'.$tipo[0]->nombre.'/convertidos/'. $extension.'/'.$nameFile2;
            $mpdf->Output($route,'F');
        }

        if($request->archivo != "undefined")
        {
            $file = $request->archivo;
            $nameFile = 'Archivo'.($cantidad+1).'.'.$file->getClientOriginalExtension();
            $path = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/'. $extension;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            } 
            $file->move($path,$nameFile);
            
            $md5_file = md5_file($path . '/'. $nameFile);
            
            if($request->verificar == 'true')
            {
                $mpdf = new \Mpdf\Mpdf();
                $porciones = explode(",", $request->criterio);
                foreach ($porciones as $key => $c) {            
                    $mpdf->WriteHTML($c);
                }
                $nameFile2 = 'Archivo'.($cantidad+1).'.pdf';
                $route = public_path() .'/archivocomparar/'.$tipo[0]->nombre.'/convertidos/'. $tipo[0]->extension;
                if (!file_exists($route)) {
                    mkdir($route, 0777, true);
                }
                $route = public_path() .'/archivocomparar/'.$tipo[0]->nombre.'/convertidos/'. $tipo[0]->extension.'/'.$nameFile2;
                $mpdf->Output($route,'F');
            }
        }

        $dato = new RepositorioArchivo();
        $dato->url = $nameFile;
        $dato->md5_file = $md5_file;
        $dato->estado = $request->estado;
        $dato->extension_id = $tipo[0]->id;
        $dato->user_id = Auth::user()->id;
        $dato->save();

        return response()->json($dato);
    }

    public function getArchivo2($id)
    {
        $datos = RepositorioArchivo::find($id);

        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->where('e.id',$datos->extension_id)
                ->get();

        $path = public_path() . '/archivocomparar/'. $tipo[0]->nombre . '/'. $tipo[0]->extension .'/'. $datos->url;
        $headers = array(
                  'Content-Type: application/pdf',
                );
        
        $nameFile = $datos->url;

        return response()->download($path, $nameFile, [], 'inline');

    } 

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {
        $dato = RepositorioArchivo::findOrFail($id);
        $this->eliminarArchivo($id);
        $dato->delete();

        return response()->json($dato);


    }

    public function eliminarArchivo($criterio)
    {
        $datos = RepositorioArchivo::find($criterio);
        $tipo = DB::table('extension as e')
                ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                ->where('e.id',$datos->extension_id)
                ->get();

        $path = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/'. $datos->url;
        if (file_exists($path))
            unlink($path);
    }

    public function getComparacion($nameFileRes,$idFile){
        $nameFileRes = base64_decode($nameFileRes);
        $idFile = base64_decode($idFile);

        $datoFileRes = DB::table('repositorio_archivo as ra')
                ->join('extension as e','e.id','ra.extension_id')
                ->select('e.extension',
                        'ra.url','ra.id')
                ->where('ra.url',$nameFileRes)
                ->where('ra.estado','archivo')
                ->get();

        $datoFile = DB::table('resultados_comparar as rc')
                ->join('repositorio_archivo as ra','ra.id','rc.archivosproyecto_id')
                ->join('extension as e','e.id','ra.extension_id')
                ->select('e.extension',
                        'ra.url','ra.id')
                ->where('rc.id',$idFile)
                ->get();
        
        //dd($datoFile,$datoFileRes);

        if($datoFileRes[0]->extension != 'PDF'){
            $pathinfo = pathinfo(public_path().'/archivos/DOCUMENTOS/'.$datoFileRes[0]->extension.'/'.$datoFileRes[0]->url);
            $pathFileRes = public_path().'/archivos/DOCUMENTOS/convertidos/'.$datoFileRes[0]->extension.'/'.$pathinfo['filename'].'.pdf';
        }else{
            $pathFileRes = public_path().'/archivos/DOCUMENTOS/'.$datoFileRes[0]->extension.'/'.$datoFileRes[0]->url;
        }

        if($datoFile[0]->extension != 'PDF'){
            $pathinfo = pathinfo($pathFile = public_path().'/archivocomparar/DOCUMENTOS/'.$datoFile[0]->extension.'/'.$datoFile[0]->url);
            $pathFile = public_path().'/archivocomparar/DOCUMENTOS/convertidos/'.$datoFile[0]->extension.'/'.$pathinfo['filename'].'.pdf';
        }else{
            $pathFile = public_path().'/archivocomparar/DOCUMENTOS/'.$datoFile[0]->extension.'/'.$datoFile[0]->url;
        }

        $PDFParser = new Parser();
        $pdf = $PDFParser->parseFile($pathFileRes);

        $pages  = $pdf->getPages();
        
        $vecDataRes = [];
        for($p1=0; $p1<sizeof($pages); $p1++){
            $aux = $pages[$p1]->getTextArray();
            //dd($aux);
            for($a=0; $a<sizeof($aux); $a++)
            {
                $vecDataRes [] = $aux[$a];
            }
        }

        $PDFParser = new Parser();
        $pdf = $PDFParser->parseFile($pathFile);
        //$text = $pdf->getText();
        //dd($text);
        $pages  = $pdf->getPages();
        
        $vecData = [];
        for($p1=0; $p1<sizeof($pages); $p1++){
            $aux = $pages[$p1]->getTextArray();
            //dd($aux);
            for($a=0; $a<sizeof($aux); $a++)
            {
                $vecData [] = $aux[$a];
            }
        }
        //dd($vecData);
        $resultado = '';
        
        for($i=0; $i<sizeof($vecDataRes); $i++){
            $clave = array_search($vecDataRes[$i], $vecData);
            if(false !== $clave)
                $resultado .= '<h4 style="color:red;">'.$vecDataRes[$i].'</h4>'.' ';
            else
                $resultado .= '<h4 style="color:black;">'.$vecDataRes[$i].'</h4>'.' ';
        }

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($resultado);
        $mpdf->Output('Resultado.PDF','I');
    }
}
