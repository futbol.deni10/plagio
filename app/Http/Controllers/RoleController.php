<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Role;
// use App\Permission;

use Illuminate\Support\Facades\DB;
use DataTables;

use Spatie\Permission\Models\Role;

use Spatie\Permission\Models\Permission;
class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index()
    {   
        $permission = Permission::get();
        return view('admin.role.index')
                ->with('permission',$permission);
    }

    public function list_roles()
    {
        $dato = DB::table('roles')->get();
        if(request()->ajax()){
            return Datatables::of($dato)
                ->rawColumns(['id','name'])
                ->make(true);
            }else{
                abort('404');
        }

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if($request->ajax()){
            // $Role = new Role($request->all());
            // $Role->save();
            // $permission = new Permission();
            // if($request->add == "on" )
            //     $permission->add = "1";
            // else
            //     $permission->add = "0";
            // if($request->edit == "on")
            //     $permission->edit = "1";    
            // else
            //     $permission->edit = "0";
            // if($request->remove == "on")
            //     $permission->remove = "1";    
            // else
            //     $permission->remove = "0";
            // $permission->role_id = $Role->id;
            // $permission->save();
            $role = Role::create(['name' => $request->input('name'),'descripcion' => $request->input('descripcion')]);
            $role->syncPermissions($request->input('permission'));
            return response()->json($role);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //$dato = Role::find($id);
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            //->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->get();
        return response()->json(['role'=>$role,'permission'=>$permission,'rolePermissions'=>$rolePermissions]);
    }

    public function update(Request $request, $id)
    {
        
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->descripcion = $request->input('descripcion');
        $role->save();
        $role->syncPermissions($request->input('permission'));

        return response()->json($role);
    }

    public function actualizarRoles(Request $request, $id)
    {
        //dd($request->input('permission'));
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->descripcion = $request->input('descripcion');
        $role->save();
        $role->syncPermissions($request->input('permission'));

        return response()->json($role);
    }


    public function destroy($id)
    {
        //
    }
}
