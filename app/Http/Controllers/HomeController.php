<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;
use AlesZatloukal\GoogleSearchApi\GoogleSearchApi;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {          
        //Artisan::call('queue:work');
        $googleSearch = new GoogleSearchApi();
        //dd($googleSearch);
        $results = $googleSearch->getResults('juegos');
        //https://www.googleapis.com/customsearch/v1?key=AIzaSyAX1Eubq7LIbAAZ4SfUhmiYclxaRnBE1BU&cx=3864ba8520f9aa8ac&q=guitarra
        //dd($results);
        return view('home');
    }

}
