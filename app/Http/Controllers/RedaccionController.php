<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\Redaccion;
use Brian2694\Toastr\Facades\Toastr;

//use App\Events\NuevaOrdenEvent;
use Pusher\Laravel\Facades\Pusher;
use Illuminate\Support\Facades\Event;

use App\Jobs\SendEmailJob;
class RedaccionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Redaccion-list|Redaccion-create|Redaccion-edit|Redaccion-delete', ['only' => ['index','store']]);
        $this->middleware('permission:Redaccion-create', ['only' => ['create','store']]);
        $this->middleware('permission:Redaccion-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:Redaccion-delete', ['only' => ['destroy']]);
    }

    public function index(){
        $Users = User::orderBy('username','ASC')
                ->where('username','<>','admin')
                ->pluck('username','id');

        return view('admin.redaccion.index')
                ->with('users',$Users);
    }

    public function indexEditor($criterio)
    {
        
        $u = User::orderBy('username','ASC')
                ->where('username','<>','admin')
                ->pluck('username','id');
        $Redaccion = Redaccion::find($criterio);
        $noredaccion = '';
        if($Redaccion)
        {            
            $users = unserialize($Redaccion->asignados);
            
            $check= in_array(strval(Auth::user()->id), $users);
            //dd($check);
            if($check)
            {   if($Redaccion->creadopor == Auth::user()->id)
                    return view('admin.redaccion.ver')->with('who','CREADOR')->with('criterio',$Redaccion)->with('users',$u);
                else
                    return view('admin.redaccion.ver')->with('who','INVITADO')->with('criterio',$Redaccion)->with('users',$u);
            }else{
                $noredaccion = 'No tienes permiso para participar en la redaccion';
                Toastr::error($noredaccion);
                return redirect()->route('home');
            }
        }else
        {
            $noredaccion = 'La url de redaccion no existe';
            Toastr::error($noredaccion);
            return redirect()->route('home');
        }

    }
    
    public function listar_redacciones()
    {
        $datosRedaccion = [];

            $dato = DB::table('redaccion as r')->get();
            $asignados = [];
            for($i=0; $i<sizeof($dato); $i++){
                $ver = unserialize($dato[$i]->asignados);
                if(in_array(Auth::user()->id, $ver)){
                    $asignados [] = $dato[$i]->id;
                }
            }
            
            $datosRedaccion = [];
            for($d=0; $d<sizeof($asignados); $d++)
            {          

                $datos =  DB::table('redaccion as r')
                ->join('users as u','u.id','=','r.creadopor')
                ->select('r.*',
                        'u.username')
                ->where('r.id',$asignados[$d])
                ->get();

                for($f=0; $f<sizeof($datos); $f++){
                    $datosRedaccion [] = [
                                        "id" => $datos[$f]->id,
                                        "nombre" => $datos[$f]->nombre,
                                        "contenido" => $datos[$f]->contenido,
                                        "asignados" => $datos[$f]->asignados,
                                        "creadopor" => $datos[$f]->creadopor,
                                        "username" => $datos[$f]->username,
                                        "created_at" => $datos[$f]->created_at,
                                        "updated_at" => $datos[$f]->updated_at,
                                    ]; 
                }
            }     


        if(request()->ajax()){
            return Datatables::of($datosRedaccion)
             ->rawColumns( ['id','username','nombre','created_at'])
             ->editColumn('created_at', function ($datosRedaccion) {
                    $dt = Carbon::create($datosRedaccion['created_at'])->locale('es');
                    $fecha = $dt->day.' '.$dt->monthName.' '.$dt->year;
                    return $fecha;
                })
             ->make(true);
            }else{
                abort('404');
        }
    }

    public function create()
    {


    }

    public function store(Request $request)
    {   if($request->user_id){
            $cad = $request->user_id;
            array_push($cad,strval(Auth::user()->id));

        }
        else{
            $cad = [];
            array_push($cad,strval(Auth::user()->id));
        }
        


        if($request->ajax())
        {
            $Redaccion = new Redaccion();
            $Redaccion->nombre = $request->nombre;
            $Redaccion->asignados = serialize($cad);
            $Redaccion->creadopor = Auth::user()->id;
            $Redaccion->save();
            return response()->json($Redaccion);
        }
    }

    public function storeUsuario(Request $request){
        //dd($request->all());
        $Redaccion = Redaccion::find($request->criterio);
        $cad = unserialize($Redaccion->asignados);
        for($i=0; $i<sizeof($request->users); $i++){
            $check = in_array(strval($request->users[$i]), $cad);
            if(!$check)
            {
                array_push($cad,strval($request->users[$i]));
                $user = User::find($request->users[$i]);
                $user2 = User::find($Redaccion->creadopor);
                $mensaje = 'Fue invitado a participar en la redaccion, creado por el Usuario: '.$user2->username;
                $datos = ['email'=>$user->email,'mensaje'=>$mensaje,'url'=>\URL::to('/').'/veredaccion/'.$Redaccion->id,'user'=>$user];
                SendEmailJob::dispatch($datos);
            }

        }
        $Redaccion->asignados = serialize($cad);
        $Redaccion->save();
        return response()->json($Redaccion);
    }

    public function guardarTexto (Request $request){
        $Redaccion = Redaccion::find($request->id);
        $Redaccion->contenido = $request->contenido;
        $Redaccion->save();
        //event(new \App\Events\NuevaOrdenEvent($request->contenido));
        return response()->json($Redaccion);
    }

    public function getRedaccion($id){
        return Redaccion::find($id);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function eliminarRedaccion($criterio)
    {
        try {
            $datos = Redaccion::find($criterio);
            $datos->delete();
            return response()->json(['mensaje'=>'']);
        } catch(\Exception $exception){
            return response()->json(['mensaje'=>'Registro en Uso no puede ser eliminado']);
        }
    }
 
    public function destroy($id)
    {
        //
    }
}
