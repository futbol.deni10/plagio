<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\http\Requests\PersonaRequest;
use App\Persona;
use App\Ciudad;
use App\User;

use Illuminate\Support\Facades\DB;
use DataTables;
//use Brian2694\Toastr\Facades\Toastr;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class PersonaController extends Controller
{
 
    function __construct()
    {
        $this->middleware('permission:persona-list|persona-create|persona-edit|persona-delete', ['only' => ['index','store']]);
        $this->middleware('permission:persona-create', ['only' => ['create','store']]);
        $this->middleware('permission:persona-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:persona-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $users = User::orderBy('id','ASC')
                ->where('baja','NO')
                ->where('disponible','SI')
                //->where('username','<>','admin')
                ->get();    
        $ciudades = Ciudad::orderBy('id','ASC')->get();
        return view('admin.persona.index')
                ->with('ciudades',$ciudades)
                ->with('users',$users);
    }

    public function get_allciudades()
    {
        $ciudades =  DB::table('ciudades')
        ->orderBy('id','ASC')
        ->get();
        return response()->json($ciudades);
    }      

    public function get_usuariosdisponibles()
    {
        $users =  DB::table('users')
        ->orderBy('id','ASC')
        ->where('baja','NO')
        ->where('disponible','SI')
        //->where('username','<>','admin')
        ->get(); 
        return response()->json($users);
    }    

    public function listar_personas()
    {
        $persona =  DB::table('personas as p')
        ->join('ciudades as c','c.id','=','p.ciudad_id')
        ->join('users as u','u.id','=','p.user_id')
        ->select('p.*',
            DB::raw("CONCAT(p.nombre,' ',p.apellido) AS personal_nombre"),
                'c.nombre as pais','u.username','u.baja')
        ->orderBy('p.id')
        ->get();
        //dd($personal);
        if(request()->ajax()){
        return Datatables::of($persona)
         ->rawColumns( ['id','nombre','ci','celular','cargo','baja'])
         ->editColumn('baja', function ($persona) {
                    if ($persona->baja == 'NO')
                        $m = 'Activo';
                    if ($persona->baja == 'SI')
                        $m = 'Inactivo';
                    return $m;
            })
         ->make(true);
        }else{
            abort('404');
        }
    }

    public function create()
    {
        //
    }


    public function store(PersonaRequest $request)
    { 
        if ($request->ajax())
        {
            $Persona = new Persona($request->all());
            $Persona->save();
            $user = User::find($request->user_id);
            $user->disponible = "NO";
            $user->save();

            return response()->json($Persona);
        } 
    } 


    public function show($id)
    {
        $Personal = Persona::find($id);
        return response()->json($Personal);
    }

    public function get_allUsuarios()
    {
        $users =  DB::table('users')
            ->orderBy('id','ASC')
            ->where('baja','NO')
            // ->where('username','<>','admin')
            ->get();
        return response()->json($users);
    }

    public function edit($id)
    {
        $Personal = DB::table('personas as p')
        ->join('ciudades as c','c.id','=','p.ciudad_id')
        ->select('p.*',
                'c.id as pais_id')
        ->where('p.id',$id)
        ->get();
        
        return response()->json($Personal);
    }


    public function actualizarPersonal(Request $request, $id) 
    {
        $Personal = Persona::FindOrFail($id);
        $Personal->nombre = $request->nombre;
        $Personal->apellido = $request->apellido;
        $Personal->ci = $request->ci;
        $Personal->telefono = $request->telefono;
        $Personal->direccion = $request->direccion;
        $Personal->ciudad_id = $request->ciudad_id;
        $ex_user = $Personal->user_id;
        $Personal->user_id = $request->user_id;
        $Personal->save();

        if($ex_user != $request->user_id)
        {
            $user = User::find($request->user_id);
            $user->disponible = "NO";
            $user->save();

            $user = User::find($ex_user);
            $user->disponible = "SI";
            $user->save();
        }
        return response()->json($Personal);             
    }

    

    public function checkDisponible($id,$id_personal)
    {
        $a = DB::table('persona')->where('user_id',$id)->count();
        $usuario = DB::table('persona')->where([
                    ['id', '=', $id_persona],
                    ['user_id', '=', $id]
                ])->count();
        $contador = 0;
            
        if ($a - $usuario == 1)
        {
            $disponible = false;
        }else{
            $disponible = true; 
        }          

        return response()->json([$disponible]); 

    }
    public function destroy($id)
    {
        //
    }

} 