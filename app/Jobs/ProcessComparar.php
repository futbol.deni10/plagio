<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\ResultadoComparar;
use Illuminate\Support\Facades\Auth;

use Smalot\PdfParser\Parser;

use App\User;

use Illuminate\Support\Facades\DB;
use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Imagick;
use Illuminate\Support\Facades\Storage;
use App\Jobs\SendEmailJob;
class ProcessComparar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $datosRequest;

    public function __construct($request)
    {
        $this->datosRequest = $request; 
    }

                    
    public function handle()
    {
        //$resultado = array();

        $unoPor = array();
        $resImagenes = array();
        $comparar = DB::table('repositorio_archivo as r')
                    ->join('extension as e','e.id','r.extension_id')
                    ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                    ->select('r.*','e.extension',
                            'ta.nombre as tipo_archivo','ta.id as tiposarchivo_id')
                    ->where('r.id',$this->datosRequest['id'])
                    ->get();


        
        if($comparar[0]->tipo_archivo == "IMAGENES")
        {    
            $ext = DB::table('extension')
                    ->where('estado','activo')
                    ->where('tiposarchivo_id',$comparar[0]->tiposarchivo_id)
                    ->get();
            $urlAchivoComparar = '/archivocomparar/IMAGENES/'.$comparar[0]->extension.'/'.$comparar[0]->url;
            for ($i=0; $i<sizeof($ext); $i++) {
                $idExt = $ext[$i]->id;
                //dd($idExt);
                $repositorio = DB::table('repositorio_archivo as ra')
                            ->join('extension as e','e.id','ra.extension_id')
                            ->select('ra.*','e.extension')
                            ->where('ra.estado','archivo')
                            ->where('ra.extension_id',$idExt)
                            ->get();
                //dd($repositorio);
                //recorrido de todos los archivos con una extension X
                for($j=0; $j<count($repositorio); $j++){
                    $urlArchivoRepositorio = '/archivos/IMAGENES/'.$repositorio[0]->extension.'/'.$repositorio[$j]->url;
                    //dd($urlArchivoRepositorio);
                    $image1 = new Imagick(public_path().$urlArchivoRepositorio);
                    $image2 = new Imagick(public_path().$urlAchivoComparar);
                    $image1->setCompression(Imagick::COMPRESSION_JPEG); 
                    $image1->setImageCompressionQuality(90);
                    $image2->setCompression(Imagick::COMPRESSION_JPEG); 
                    $image2->setImageCompressionQuality(90);

                    $imagick1Size = $image1->getImageGeometry();
                    $imagick2Size = $image2->getImageGeometry();
                    $maxWidth = max($imagick1Size['width'], $imagick2Size['width']);
                    $maxHeight = max($imagick1Size['height'], $imagick2Size['height']);
                    $image1->extentImage($maxWidth, $maxHeight, 0, 0);
                    $image2->extentImage($maxWidth, $maxHeight, 0, 0);



                    $result = $image1->compareImages($image2, Imagick::METRIC_MEANSQUAREERROR);
                    //dd($result);
                    //$porcentaje = round(($result[1]*100), 2);
                    $porcentaje =  round((double) substr($result[1], 0, 6) * 100, 2);
                    $porcentaje =  strrev(strval($porcentaje));
                    $porcentaje = floatval($porcentaje);
                    if($porcentaje == 0)
                        $porcentaje = 100;
                    $result[0]->setCompression(Imagick::COMPRESSION_JPEG);
                    $result[0]->setImageCompressionQuality(90);
                    $result[0]->setImageFormat("jpeg");
                    $result[0]->setResolution(150, 150);
                    $img_name = time().'_'.$repositorio[$j]->url;
                    //dd($img_name);
                    Storage::disk('images_base64')->put($img_name,$result[0]);

                    $resImagenes [] = [
                                'id' => $repositorio[$j]->id,
                                'urlRepositorio'=> $repositorio[$j]->url,
                                'extension' => $repositorio[0]->extension,
                                'porcentaje'=>$porcentaje,
                                'urlComparar'=>$urlAchivoComparar,
                                'urlResultado'=> $img_name
                                ];
 
                    //dd($resImagenes);
                }
            }           
            $ResultadoComparar = new ResultadoComparar();
            $ResultadoComparar->a_comparados =  serialize($resImagenes);
            $ResultadoComparar->archivosproyecto_id = $this->datosRequest['id'];
            $ResultadoComparar->save();

            $idusuario = $comparar[0]->user_id;
            $user = User::find($idusuario);
            
            $datos = ['email'=>$user->email,'mensaje'=>'La comparacion de la imagen fue finalizada <br> Esperamos que los resultados sean lo que tu esperabas, recuerda que la ultima
palabra la tienes tu','url'=>\URL::to('/').'/resultadoIndex','user'=>$user];
            SendEmailJob::dispatch($datos);

        }else 
        {
            $ext = DB::table('extension')
                ->where('estado','activo')
                ->where('tiposarchivo_id',$comparar[0]->tiposarchivo_id)
                ->get(); 

            if($comparar[0]->extension != 'PDF')
            {            
                $pos = strpos( $comparar[0]->url,'.');
                $urlDoc2 = substr($comparar[0]->url,0,$pos) . '.pdf';
            }else
                $urlDoc2 = $comparar[0]->url;
                //dd($urlDoc);
            for ($i=0; $i<sizeof($ext); $i++) {
                $idExt = $ext[$i]->id;
                $repositorio = DB::table('repositorio_archivo as ra')
                                ->join('extension as e','e.id','ra.extension_id')
                                ->select('ra.*','e.extension')
                                ->where('ra.estado','archivo')
                                ->where('ra.extension_id',$idExt)
                                ->get();
                //dd($repositorio);
                for($j=0; $j<count($repositorio); $j++)
                {   
                    //dd($repositorio[$j]->extension_id);
                    if($repositorio[$j]->md5_file == $comparar[0]->md5_file){
                        $resultado [] = ['url'=> $urlDoc2,'id' => $comparar[0]->id];
                    }

                    $tipo = DB::table('extension as e')
                            ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                            ->where('e.id',$repositorio[$j]->extension_id)
                            ->get();
                    //dd($tipo);
                    if($repositorio[$j]->extension != 'PDF')
                    {
                        $pos = strpos( $repositorio[$j]->url,'.');
                        $urlDoc = substr($repositorio[$j]->url,0,$pos) . '.pdf';

                        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/convertidos/'.$tipo[0]->extension.'/'. $urlDoc;

                    }else {
                        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'.$tipo[0]->extension.'/'. $repositorio[$j]->url;
                    }
                    //dd($path);
                    $PDFParser = new Parser();
                    $pdf = $PDFParser->parseFile($path);
                    $pages  = $pdf->getPages();

                    $cad1 = "";
                    for($p1=0; $p1<sizeof($pages); $p1++){
                        $cad1 .= implode($pages[$p1]->getTextArray());
                    }

                    $cad1 = implode(" ",array_unique(explode(" ", $cad1)));

                    if($comparar[0]->extension != 'PDF')
                    {
                        $path2 = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/convertidos/'. $comparar[0]->extension .'/'. $urlDoc2;
                    }else {                    
                        $path2 = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/'. $comparar[0]->extension .'/'. $urlDoc2;    
                    }
                    //dd($path2);
                    $PDFParser2 = new Parser();
                    $pdf = $PDFParser2->parseFile($path2);

                    $pages  = $pdf->getPages();
                    
                    $cad2 = "";
                    for($p1=0; $p1<sizeof($pages); $p1++){
                        $cad2 .= implode($pages[$p1]->getTextArray());
                    } 
                    //dd($cad2);
                    $cad2 = implode(" ",array_unique(explode(" ", $cad2)));
                    similar_text($cad1, $cad2, $porcentaje);

                    $unoPor [] = ['url'=> $repositorio[$j]->url,'id' => $repositorio[$j]->id,'porcentaje'=>$porcentaje];          
                }
            }
            $ResultadoComparar = new ResultadoComparar();
            $ResultadoComparar->a_comparados =  serialize($unoPor);
            $ResultadoComparar->archivosproyecto_id = $this->datosRequest['id'];
            $ResultadoComparar->save();

            $idusuario = $comparar[0]->user_id;
            $user = User::find($idusuario);

            $datos = ['email'=>$user->email,'mensaje'=>'La comparacion de la imagen fue finalizada <br> Esperamos que los resultados sean lo que tu esperabas, recuerda que la ultima
palabra la tienes tu','url'=>\URL::to('/').'/resultadoIndex','user'=>$user];
            SendEmailJob::dispatch($datos);
        }
        
    } 
}
