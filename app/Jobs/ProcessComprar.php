<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\ResultadoComparar;
class ProcessComprar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datosRequest;

    public function __construct(Request $request)
    {
        $this->datosRequest = $request;
    }

    public function handle()
    {
        $resultado = array();
        $unoPor = array();
        $comparar = DB::table('repositorio_archivo as r')
                    ->join('extension as e','e.id','r.extension_id')
                    ->select('r.*','e.extension')
                    ->where('r.id',$datosRequest->id)
                    ->get();
        $ext = DB::table('extension')->where('estado','activo')->get();

        if($comparar[0]->extension != 'PDF')
        {            
            $pos = strpos( $comparar[0]->url,'.');
            $urlDoc = substr($comparar[0]->url,0,$pos) . '.pdf';
        }else
            $urlDoc = $comparar[0]->url;

        for ($i=0; $i<sizeof($ext); $i++) {
            $idExt = $ext[$i]->id;
            $repositorio = DB::table('repositorio_archivo as ra')
                            ->join('extension as e','e.id','ra.extension_id')
                            ->select('ra.*','e.extension')
                            ->where('ra.estado','archivo')
                            ->where('ra.extension_id',$idExt)
                            ->get();
            //dd($repositorio);
            for($j=0; $j<count($repositorio); $j++)
            {   
                //dd($repositorio[$j]->extension_id);
                if($repositorio[$j]->md5_file == $comparar[0]->md5_file){
                    $resultado [] = ['url'=> $urlDoc,'id' => $comparar[0]->id];
                }

                $tipo = DB::table('extension as e')
                        ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                        ->where('e.id',$repositorio[$j]->extension_id)
                        ->get();
                //dd($tipo);
                if($repositorio[$j]->extension != 'PDF')
                {
                    $pos = strpos( $repositorio[$j]->url,'.');
                    $urlDoc = substr($repositorio[$j]->url,0,$pos) . '.pdf';

                    $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/convertidos/'.$tipo[0]->extension.'/'. $urlDoc;

                }else {
                    $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'.$tipo[0]->extension.'/'. $repositorio[$j]->url;
                }
                //dd($path);
                $PDFParser = new Parser();
                $pdf = $PDFParser->parseFile($path);
                $pages  = $pdf->getPages();

                $cad1 = "";
                for($p1=0; $p1<sizeof($pages); $p1++){
                    $cad1 .= implode($pages[$p1]->getTextArray());
                }

                $cad1 = implode(" ",array_unique(explode(" ", $cad1)));

                if($comparar[0]->extension != 'PDF')
                {
                    $path2 = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/convertidos/'. $comparar[0]->extension .'/'. $urlDoc;
                }else {                    
                    $path2 = public_path() . '/archivocomparar/'. $tipo[0]->nombre .'/'. $comparar[0]->extension .'/'. $urlDoc;    
                }
                //dd($path2);
                $PDFParser2 = new Parser();
                $pdf = $PDFParser2->parseFile($path2);

                $pages  = $pdf->getPages();
                
                $cad2 = "";
                for($p1=0; $p1<sizeof($pages); $p1++){
                    $cad2 .= implode($pages[$p1]->getTextArray());
                } 
                //dd($cad2);
                $cad2 = implode(" ",array_unique(explode(" ", $cad2)));
                similar_text($cad1, $cad2, $porcentaje);

                $unoPor [] = ['url'=> $repositorio[$j]->url,'id' => $repositorio[$j]->id,'porcentaje'=>$porcentaje];
                
                $ResultadoComparar = new ResultadoComparar();
                $ResultadoComparar->a_comparados =  serialize($unoPor);
                $ResultadoComparar->archivosproyecto_id = $datosRequest->id;
                $ResultadoComparar->save();
            }
        }
    } 
}
