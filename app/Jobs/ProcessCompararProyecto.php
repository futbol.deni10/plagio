<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\ResComProyecto;
use Illuminate\Support\Facades\Auth;

use Smalot\PdfParser\Parser;

use App\User;

use Illuminate\Support\Facades\DB;
use \Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Imagick;
use Illuminate\Support\Facades\Storage;
use App\Jobs\SendEmailJob;
class ProcessCompararProyecto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datosRequest;

    public function __construct($request)
    {
        $this->datosRequest = $request;
    }
 
    public function handle()
    {

        $unoPor = array();
        $resImagenes = array();

        $tipoarchivo = DB::table('tiposarchivo as t')
                        ->join('extension as e','e.tiposarchivo_id','t.id')
                        ->where('e.extension',strtoupper($this->datosRequest['extension']))
                        ->select('t.*')
                        ->get();
        
        if($tipoarchivo[0]->nombre == 'IMAGENES')
        {
            $ext = DB::table('extension')
                ->where('estado','activo')
                ->where('tiposarchivo_id',$tipoarchivo[0]->id)
                ->get(); 
            $urlAchivoComparar ='/proyectos2/'.$this->datosRequest['url'];

            for ($i=0; $i<sizeof($ext); $i++) {
                $idExt = $ext[$i]->id;
                $repositorio = DB::table('repositorio_archivo as ra')
                            ->join('extension as e','e.id','ra.extension_id')
                            ->select('ra.*','e.extension')
                            ->where('ra.estado','archivo')
                            ->where('ra.extension_id',$idExt)
                            ->get();

                for($j=0; $j<count($repositorio); $j++){
                    $urlArchivoRepositorio = '/archivos/IMAGENES/'.$repositorio[0]->extension.'/'.$repositorio[$j]->url;
                    //dd($urlAchivoComparar);
                    $image1 = new Imagick(public_path().$urlArchivoRepositorio);
                    $image2 = new Imagick(public_path().$urlAchivoComparar);
                    $image1->setCompression(Imagick::COMPRESSION_JPEG); 
                    $image1->setImageCompressionQuality(90);
                    $image2->setCompression(Imagick::COMPRESSION_JPEG); 
                    $image2->setImageCompressionQuality(90);

                    $imagick1Size = $image1->getImageGeometry();
                    $imagick2Size = $image2->getImageGeometry();
                    $maxWidth = max($imagick1Size['width'], $imagick2Size['width']);
                    $maxHeight = max($imagick1Size['height'], $imagick2Size['height']);
                    $image1->extentImage($maxWidth, $maxHeight, 0, 0);
                    $image2->extentImage($maxWidth, $maxHeight, 0, 0);



                    $result = $image1->compareImages($image2, Imagick::METRIC_MEANSQUAREERROR);
                    //dd($result);
                    //$porcentaje = round(($result[1]*100), 2);
                    $porcentaje =  round((double) substr($result[1], 0, 6) * 100, 2);
                    $porcentaje =  strrev(strval($porcentaje));
                    $porcentaje = floatval($porcentaje);
                    if($porcentaje == 0)
                        $porcentaje = 100;
                    $result[0]->setCompression(Imagick::COMPRESSION_JPEG);
                    $result[0]->setImageCompressionQuality(90);
                    $result[0]->setImageFormat("jpeg");
                    $result[0]->setResolution(150, 150);
                    $img_name = time().'_'.$repositorio[$j]->url;
                    //dd($img_name);
                    Storage::disk('images_base64')->put($img_name,$result[0]);

                    $resImagenes [] = [
                                'id' => $repositorio[$j]->id,
                                'urlRepositorio'=> $repositorio[$j]->url,
                                'extension' => $repositorio[0]->extension,
                                'porcentaje'=>$porcentaje,
                                'urlComparar'=>$urlAchivoComparar,
                                'urlResultado'=> $img_name
                                ];
                }
            }
                $d = DB::table('proyectos')->where('nombre',$this->datosRequest['nombreproyecto'])->get();

                $ResultadoComparar = new ResComProyecto();
                $ResultadoComparar->a_comparados =  serialize($resImagenes);
                $ResultadoComparar->proyectos_id  = $d[0]->id;
                $ResultadoComparar->archivo  = $this->datosRequest['nombre'].$this->datosRequest['extension'];
                $ResultadoComparar->extension  = strtoupper($this->datosRequest['extension']);
                $ResultadoComparar->save();
          
                $user = User::find($this->datosRequest['user_id']);

                $datos = ['email'=>$user->email,'mensaje'=>'La comparacion de la imagen fue finalizada <br> Esperamos que los resultados sean lo que tu esperabas, recuerda que la ultima
    palabra la tienes tu','url'=>\URL::to('/').'/resultadoIndex','user'=>$user];
                SendEmailJob::dispatch($datos);

        }else{
            $ext = DB::table('extension')
                ->where('estado','activo')
                ->where('tiposarchivo_id',$tipoarchivo[0]->id)
                ->get(); 
           // dd($ext);
            if(strtoupper($this->datosRequest['extension']) == 'PDF')
            {            
                $urlDoc2 =  public_path().'/proyectos2/'.$this->datosRequest['url'];
            }else
                $urlDoc2 = public_path().'/proyectos2/'.$this->datosRequest['nombreproyecto'].'-convertidos/'.$this->datosRequest['version'].'/'.$this->datosRequest['nombre'].'.pdf';
            //dd($urlDoc2);
            for ($i=1; $i<sizeof($ext); $i++) {
                $idExt = $ext[$i]->id;
                $repositorio = DB::table('repositorio_archivo as ra')
                                ->join('extension as e','e.id','ra.extension_id')
                                ->select('ra.*','e.extension')
                                ->where('ra.estado','archivo')
                                ->where('ra.extension_id',$idExt)
                                ->get();
                //dd($repositorio);
                for($j=0; $j<count($repositorio); $j++)
                {   
                    $tipo = DB::table('extension as e')
                            ->join('tiposarchivo as ta','ta.id','e.tiposarchivo_id')
                            ->where('e.id',$repositorio[$j]->extension_id)
                            ->get();
                    //dd($tipo);
                    if($repositorio[$j]->extension != 'PDF')
                    {
                        $pos = strpos( $repositorio[$j]->url,'.');
                        $urlDoc = substr($repositorio[$j]->url,0,$pos) . '.pdf';

                        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/convertidos/'.$tipo[0]->extension.'/'. $urlDoc;

                    }else {
                        $path = public_path() . '/archivos/'. $tipo[0]->nombre .'/'.$tipo[0]->extension.'/'. $repositorio[$j]->url;
                    }

                    $PDFParser = new Parser();
                    $pdf = $PDFParser->parseFile($path);
                    $pages  = $pdf->getPages();

                    $cad1 = "";
                    for($p1=0; $p1<sizeof($pages); $p1++){
                        $cad1 .= implode($pages[$p1]->getTextArray());
                    }

                    $cad1 = implode(" ",array_unique(explode(" ", $cad1)));
                    //dd($cad1);
                    $PDFParser2 = new Parser();
                    //dd($urlDoc2);
                    $pdf = $PDFParser2->parseFile($urlDoc2);
                    $pages  = $pdf->getPages();
                    
                    $cad2 = "";
                    for($p1=0; $p1<sizeof($pages); $p1++){
                        $cad2 .= implode($pages[$p1]->getTextArray());
                    } 
                   // dd($cad2);
                    $cad2 = implode(" ",array_unique(explode(" ", $cad2)));
                    similar_text($cad1, $cad2, $porcentaje);

                    $unoPor [] = ['url'=> $repositorio[$j]->url,'id' => $repositorio[$j]->id,'porcentaje'=>$porcentaje]; 
                }
            }

            $d = DB::table('proyectos')->where('nombre',$this->datosRequest['nombreproyecto'])->get();

            $ResultadoComparar = new ResComProyecto();
            $ResultadoComparar->a_comparados =  serialize($unoPor);
            $ResultadoComparar->proyectos_id  = $d[0]->id;
            $ResultadoComparar->archivo  = $this->datosRequest['nombre'].$this->datosRequest['extension'];
            $ResultadoComparar->extension  = strtoupper($this->datosRequest['extension']);
            $ResultadoComparar->save();


            
            $user = User::find($this->datosRequest['user_id']);

            $datos = ['email'=>$user->email,'mensaje'=>'La comparacion de la imagen fue finalizada <br> Esperamos que los resultados sean lo que tu esperabas, recuerda que la ultima
palabra la tienes tu','url'=>\URL::to('/').'/resultadoIndex','user'=>$user];
            SendEmailJob::dispatch($datos);
        }
    }
}
