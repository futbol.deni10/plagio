<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivosProyecto extends Model
{
	protected $table = "archivosproyecto";

    protected $fillable = [
        'archivo','user_id','user_id','version','notas','resultadocomprar'
    ];
}
