<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    protected $table = "version";

    protected $fillable = [
        'texto', 'gestion_versiones_id','user_id'
    ];
}
