<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultadoComparar extends Model
{
	protected $table = "resultados_comparar";

    protected $fillable = [
        'a_comparados','archivosproyecto_id'
    ];
}
