<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GestionVersiones extends Model
{
    protected $table = "gestion_versiones";

    protected $fillable = [
        'nombre','contenido','proyecto_id','user_id'
    ];
}
