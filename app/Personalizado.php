<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personalizado extends Model
{
	protected $table = "personalizados";

    protected $fillable = [
    	'pcoded_navbar',
    	'navbar_logo',
    	'pcoded_header',
    	'active_item_theme',
    	'pcoded_navigatio_lavel',
    	'nav_type',
    	'vertical_effect',
    	'item_border_style',
    	'dropdown_icon',
    	'subitem_icon',
    	'user_id'
    ];
}
